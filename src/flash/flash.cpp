#include "../../popsign.h"
#include "../flash/flash.h"
#include "../board/board.h"
#include "../led_matrix/display_page.h"

extern int display_step;
extern uint8_t modeNum;

// 페이지 Enable List --> 페이지 사용가능하는지 가지고 있는 버퍼 (모든 페이지 접근 하는데에 시간이 오래걸려 해당 버퍼 사용)
extern uint8_t pageEnableList[PAGE_TOTAL_COUNT];

void deleteFile(fs::FS &fs, const char * path){
    // Serial.printf("Deleting file: %s\r\n", path);
    if(fs.remove(path)){
        Serial.println("- file deleted");
    } else {
        Serial.println("- delete failed");
    }
}

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    // Serial.printf("Listing directory: %s\r\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("- failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println(" - not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){ 
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("\tSIZE: ");
            Serial.println(file.size());
        }
            file = root.openNextFile();
    }
} 

void writeFile(fs::FS &fs, const char * path, uint8_t* buffer, size_t size){
    //  Serial.printf("Writing file: %s\r\n", path);
    //  페이지1~5인경우 + 와이파이 모드 인 경우 플래시 지우고 다시 읽기

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("- failed to open file for writing");
        return;
    }
    file.write(buffer, size);
    file.close();

}

int readFile(fs::FS &fs, const char * path, uint8_t * buffer){
    // Serial.printf("Reading file: %s\r\n", path);
    int fileIndex = 0;

    File file = fs.open(path);
    if(!file || file.isDirectory()){
        Serial.println("- failed to open file for reading");
        return -1;
    }

    // Serial.println("- read from file:");
    while (file.available()){
        buffer[fileIndex++] = file.read();
    }
    file.close();
    return fileIndex;
}

void flashFormat() {
    LittleFS.format();
}


/**************************************************************************/
/** 
 * @brief   - 모든 페이지 Flash 삭제 및 wifi mode일경우 페이지 reload
*/
/**************************************************************************/
void allPageDelete() {
    /*  page1 ~ PAGE_TOTAL_COUNT 삭제  */
    for (int i = 0 ; i < PAGE_TOTAL_COUNT; i++) {
        if (pageEnableList[i] == FLASH_PAGE_ENABLE_VALUE) {
            char pageStr[15];
            sprintf(pageStr, "/page%d.txt", i + 1); //현재 0페이지로 들어오게 되어있어서 0 + 1 연산
            deleteFile(LittleFS, pageStr);
        }
        pageEnableList[i] = FLASH_PAGE_DISENABLE_VALUE;  //  페이지 Enable List 저장
    }
    writeFile(LittleFS, FLASH_PAGE_ENABLE_LIST, pageEnableList, PAGE_TOTAL_COUNT);  //    페이지 Enable List 모두 0으로 초기화
}