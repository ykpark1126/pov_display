#include <time.h>
#include "../led_matrix/ledpannel.h"
#include "../utils/localtime.h"

const char* ntpServer = "kr.pool.ntp.org";
 
//3200초는 1시간, 우리나라는 GMT+9 이므로 3600초x9 = 32400 해 줌
 
//섬머타임 적용 시 3600 (시간을 1시간 빠르게 함)
//우리나라는 시행 안하므로 0
const int   daylightOffset_sec = 0; 

extern struct tm g_timeinfo;

/**
 * @brief Get the localtime object
 * 
 */
void get_localtime()
{
  if(!getLocalTime(&g_timeinfo)){
    // Serial.println("local time fail");
  } else {
    // Serial.println("local time success");
  }
}

/**
 * @brief Time Gap 지정
 * 
 * @param _time_sec_offset Time Gap
 */
void time_gap_setup(int _time_sec_offset) 
{
    configTime(_time_sec_offset, daylightOffset_sec, ntpServer);
}

void printLocalTime(void* matrix, LedPannel* ledpannel) {
  time_t t;
  static time_t last_t = 0;
  static time_t last_min = 0;
  struct tm *tm;
  static const char* const wd[7] = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};

  t = time(NULL);
  if (last_t == t) return; // draw each second
  last_t = t;
  tm = localtime(&t);

  if (last_min == tm->tm_min) {
    ((MATRIX*)matrix)->fillRect(47,3,14,13,0);
    ((MATRIX*)matrix)->setTextColor(((MATRIX*)matrix)->Color(15, 15, 15));
    ((MATRIX*)matrix)->setCursor(48, 4);
    ((MATRIX*)matrix)->printf("%02d", tm->tm_sec);
  } else {
    ((MATRIX*)matrix)->fillScreen(0);
    ((MATRIX*)matrix)->setTextColor(((MATRIX*)matrix)->Color(15, 15, 15));
    ledpannel->setupFont(3);
    ((MATRIX*)matrix)->setCursor(0, 2);
    ((MATRIX*)matrix)->printf("%02d:%02d:", tm->tm_hour, tm->tm_min);
    ledpannel->setupFont(1);
    ((MATRIX*)matrix)->setCursor(48, 4);
    ((MATRIX*)matrix)->setTextColor(((MATRIX*)matrix)->Color(15, 15, 15));
    ((MATRIX*)matrix)->printf("%02d", tm->tm_sec);

    ((MATRIX*)matrix)->setCursor(-1, 18);
    ((MATRIX*)matrix)->setTextColor(((MATRIX*)matrix)->Color(12, 12, 4));
    ((MATRIX*)matrix)->printf("%02d/%02d/%s", tm->tm_mon+1, tm->tm_mday, wd[tm->tm_wday]);
    // ((MATRIX*)matrix)->setCursor(54, 18);
    // ((MATRIX*)matrix)->printf("일\n");
    last_min = tm->tm_min;
  }
};

/**
 * @brief Set the time object 
 * 매개변수로 전달 받은 시간으로 Time Set
 * 
 */
void set_local_time(uint16_t s_yy, uint8_t s_MM, uint8_t s_dd, uint8_t s_hh, uint8_t s_mm,uint8_t s_ss) {
  time_gap_setup(BT_TIME_GAP);

  struct tm tm;
  tm.tm_year = s_yy - 1900;
  tm.tm_mon = s_MM - 1;
  tm.tm_mday = s_dd;
  tm.tm_hour = s_hh;
  tm.tm_min = s_mm;
  tm.tm_sec = s_ss;
  time_t t = mktime(&tm);
  printf("Setting time: %s", asctime(&tm));
  struct timeval now = { .tv_sec = t };
  settimeofday(&now, NULL);
}