#include <Arduino.h>
#include "../board/board.h"
#include "../utils/etc.h"

int busy_flg = 0;

boolean delayExt(unsigned long cur_time, unsigned long* pre_time, uint16_t inteval){

  if (cur_time - *pre_time >= inteval ){  //  If time has passed more than inteval = Return true
    *pre_time = cur_time;
    return true;
  } else 
    return false;
}

boolean delayUsExt(unsigned long cur_time, unsigned long* pre_time, unsigned long inteval){

  if (cur_time - *pre_time >= inteval ){  //  If time has passed more than inteval = Return true
    *pre_time = cur_time;
    return true;
  } else 
    return false;
}


int get_critical_status(void)
{
  return busy_flg;
}

void critical_section_begin(void)
{
  busy_flg = BUSY_WAITS;
}

void critical_section_end(void)
{
  busy_flg = NON_BUSY;
}