#ifndef _LOCALTIME_
#define _LOCALTIME_

#define GMT

const long  gmtOffset_sec = 32400 - 3600; 

#define BT_TIME_GAP         0    // 블루투스로 시간 받아오는 경우 GAP 차이 없어 0 값으로 Set

#define SERVER_TIME_GAP_KOREA 32400    //  서버에서 시간 받아오는 경우 32400 GAP Set

class LedPannel; // ledpannel.h 두 헤더 파일 서로 참조 해서 미리 클래스 타입 선언 

void get_localtime();
void time_gap_setup(int _time_sec_offset);
void printLocalTime(void* matrix, LedPannel* ledpannel);
void set_local_time(uint16_t s_yy, uint8_t s_MM, uint8_t s_dd, uint8_t s_hh, uint8_t s_mm,uint8_t s_ss);

#endif