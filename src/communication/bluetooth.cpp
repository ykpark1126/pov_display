#include <BluetoothSerial.h>
#include "bluetooth.h"
#include "../board/config.h"

extern String bt_name;

void Bluetooth::setup()
{
  pSerialBT->begin(bt_name); //Bluetooth device name
}

int Bluetooth::check()
{
	return pSerialBT->available();
}

void Bluetooth::disconnect()
{
  pSerialBT->disconnect();
  // SerialBT->flush();
  pSerialBT->end(); 
}

void Bluetooth::end()
{
  pSerialBT->end(); 
}
