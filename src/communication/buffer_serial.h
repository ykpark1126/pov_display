#ifndef _BUFFER_SERIAL_
#define _BUFFER_SERIAL_

#include <BluetoothSerial.h>
#include "../led_matrix/ledpannel.h"
#include "../communication/command.h"


#define MAX_SERIAL_BUF 2048
#define PACKET_SIZE 2048
#define STX 0x02
#define ETX 0x03
//#define ACK 0x06
#define NCK 0x15

/*	패킷 사이즈	*/
#define SIZE_STX				1
#define SIZE_LEN1				2
#define SIZE_LEN2				2
#define SIZE_CMD                1
#define SIZE_CRC                2
#define SIZE_ETX                1
#define SIZE_FIELD              5
#define SIZE_PAGE               1
#define SIZE_SET_DRAW_FIELD     SIZE_FIELD + SIZE_PAGE + SIZE_LEN1
#define SIZE_PKT_DATA			SIZE_STX + SIZE_LEN1 + SIZE_LEN2 + SIZE_CMD + SIZE_CRC + SIZE_ETX;
#define SIZE_ACTION                    3
#define SIZE_EFFECT                    9
#define SIZE_OPTION  	               1
#define SIZE_PICTURE_SIZE  	           4	// Width 2bytes + Hegiht 2bytes

/*	패킷 인덱스	*/
#define IDX_STX					0
#define IDX_LEN					IDX_STX + SIZE_STX
#define IDX_CMD					IDX_LEN + SIZE_LEN1 + SIZE_LEN2
#define IDX_CRC					IDX_CMD + SIZE_CMD
#define IDX_ETX					IDX_CRC + SIZE_CRC

#define TRANSMIT_BLUETOOTH	1
#define TRANSMIT_UART_GPIO 	0


//통신 상태 Value
#define TRANSMIT_STATUS_DISENABLE_VALUE	1	//	Non-Communication Value (통신 불가능 상태)
#define TRANSMIT_STATUS_ENABLE_VALUE		0   //  Communication enabled status (통신 가능 상태)

class BufferSerial{
private:
	uint8_t  buf[MAX_SERIAL_BUF];
	uint16_t head;
	uint16_t tail;
	char packet[PACKET_SIZE];
	int transmit_status_vaule = TRANSMIT_STATUS_ENABLE_VALUE;	//	통신 상태 값

	BluetoothSerial* pSerialBT;
	
	void reicve_bt_packet(boolean * ret);
	void reicve_uart_packet(boolean * ret);
	void reicve_wifi_packet(boolean * ret);
	void reicve_lora_packet(boolean * ret);

	void send_bt_packet(char* sendPacket, uint16_t sendDataLen);
	void send_uart_packet(char* sendPacket, uint16_t sendDataLen);
	void send_lora_packet(char* sendPacket, uint16_t sendDataLen);
	
	boolean recv_callback();
	uint8_t serialType = TRANSMIT_BLUETOOTH;
public:
	BufferSerial(BluetoothSerial* btSerial){
		this->pSerialBT = btSerial;
  		memset(buf, 0, sizeof(buf));
		head = 0;
		tail = 0;
	}
	bool rcvComplete = true;
	void process(void* matrix, LedPannel* ledpannel);
	void cmd_process(void* matrix, LedPannel* ledpannel, char cmd, char* data, int len);
	void transmit_data(char cmd, char* data, uint32_t len);
	void transmit_data(char cmd);

	void bluetooth_end();
	void set_serial_type(uint8_t typeNum);
	int Rcv_MelsecMaster(void* matrix, LedPannel* ledpannel);
	int get_transmit_critical_status(void);	//	통신 상태 Getter
	void critical_transmit_section_begin(void);	//	통신 불가능 상태 진입 (불가능 상태로 전환)
	void critical_transmit_section_end(void);	//	통신 불가능 상태 종료 (가능 상태로 전환)
};

#endif
