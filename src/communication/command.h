#ifndef _COMMAND_
#define _COMMAND_

/*  BLUETOOTH DRAW DATA   */
#define SET_DRAW                    0x11
#define SET_PICTURE_START           0x14
#define SET_PICTURE                 0x15
#define SET_PICTURE_END             0x16
#define SET_PICTURE_ONCE_TIME       0x65
#define SET_TEXT                    0x21
#define SET_SCR_CLEAR               0x31
#define SET_SCR_COLOR               0x41
#define SET_COLOR_TEXT              0x51
#define SET_FIRM_UPDATE             0x61
#define SET_PERFORM_MODE            0x71
#define SET_EXAM                    0x81

/*  NOTIFY BOARD INFO FOR APP   */
#define GET_FIRM_UPDATE             0x62
#define GET_BOARD_INFO              0x91
#define GET_BATERY_INFO             0x98
#define SET_BOARD_CONFIG_SETTING    0x99    //  board Config Setting (모델, Pixel Width Hieght Size, Flash Memory)
#define SET_INIT_BOARD              0xA0
#define GET_PAGE_SETTING_INFO       0xA8


/*  BEIGNG OUTPUT PIXEL SEND TO APP   */
#define GET_DISPLAY_PAGE_PIXEL      0x9B
#define GET_DISPLAY_PAGE_COUNT      0xA2
#define GET_DISPLAY_PAGE_PIXEL_START    0x19
#define GET_DISPLAY_PAGE_PIXEL_END  0x18

/*  FLASH WRITE   */
#define SAVE_TEXT                   0xA1
#define SAVE_COLOR_TEXT             0xA3
#define SAVE_PICTURE                0xA5

/*  FLASH Delete   */
#define DELETE_ALL_PAGE             0xA6


/*  ACTION TYPE   */
#define CMD_ACT_DEFALUT             0x00
#define CMD_ACT_LEFT_RIGHT          0x01
#define CMD_ACT_RIGHT_LEFT          0x02
#define CMD_ACT_TOP_DOWN            0x03
#define CMD_ACT_DOWN_TOP            0x04
#define CMD_ACT_BLINK               0x05
#define CMD_ACT_PAGE_CHANGE         0x06

/*  PICTURE COMMUNICATE    */
#define PICTURE_RECIVE_NACK         0x12
#define PICTURE_RECIVE_ACK          0x13

/*  BOARD SETTING    */
#define SET_START_MODE              0x92
#define SAVE_LED_LEVEL              0x93
#define SET_LED_LEVEL               0x95
#define SET_USE_MODE                0x94
#define SET_LOGO_TIME               0x9D
#define SET_PAGE_TIME               0x9E
#define SET_PAGE_CHANGE_MODE        0xA4
#define SET_LOGO_ENABLE             0x9F
#define SET_LOGO_REPEAT             0x9C

/*  ROTATION    */
#define SET_ROTATION_LANDSCAPE        0x96
#define SET_ROTATION_PORTRAIT         0x97

/*  ETC  */
#define SET_BT_NAME                 0x22

/*  TRANSMIT MODE CHANGE   */
#define SET_TRANSMIT_MODE_CHANGE            0x17    //  Uart <-> Bluetooth
#define SET_MODE_CHANGE                     0x20    //  모든 모드 변경
#define SET_MODE_CHANGE_BT_NON_DISCONNECT   0x27    //  블루투스 연결 끊지 않고 블루투스 모드로 변경

/*  PAGE TIME SWICH INFO   */
#define SET_PAGE_TIMER_SWITCH           0xA7
#define SET_PAGE_TIMER_SWITCH_ENABLE    0xA9
#define GET_PAGE_TIMER_SWITCH_ENABLE    0xAA


#define TRANSMIT_MODE_BLUETOOTH     0x01
#define TRANSMIT_MODE_UART          0x02

/* Set Matrix Function (Clock, . . .) */ 
#define SET_MATRIX_FUNCTION         0x42

/*  TRANSMIT STATUS CODE    */
#define TS_CONTINUE 0x03
#define TS_STOP 0x04


/* 패킷 수신 후 Next Packet 요청 CMD    */
#define SET_NEXT_PACKET              0xBC

/** LED GAMMA TABLE ENABLE VALUE*/
#define SET_LED_GAMMA_TABLE_ENABLE      0x30


/*  BLUETOOTH 업데이트 바이너리 파일 가져오기 COMMAND */
#define BT_UPDATE_START_HEADER_INFO         0x52
#define BT_UPDATE_ONGOING                   0x53
#define BT_UPDATE_END                       0x54
#define BT_UPDATE_CANCEL                    0x55

/*  Random Page 출력    */
#define SET_RANDOE_PAGE_PRINT               0x32

/*  페이지 간격 시간 설정    */
#define SET_PAGE_INTERVAL_TIME              0x33

/*  무게 인식 최소 값 설정   */
#define SET_WEIGHT_RECOGNITION_MIN_VALUE    0x34

/*  Lora Sub Mode Type    */
#define SET_LORA_SUB_MODE_TYPE              0x35

/*  게이지 모드 막대 Count    */
#define SET_GAGE_MODE_GAGE_COUNT            0x36

#endif
