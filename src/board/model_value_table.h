#ifndef _MODEL_VALUE_TABLE_
#define _MODEL_VALUE_TABLE_

/*  보드 타입 종류 (Basic or YouFuni or ENT_12832) */
#define MASTER              0
#define YOUFUNI             1
#define ENT_12832           2
#define VOICE_MODEL         3
#define GLASS_LED           4
#define NUMBER_TABLE        5
#define SHOU_POV_DISPLAY    6
#define FLOOR_LEDSIGN       7


/*  CPU 모듈 종류 (WROOM-32D or WROVER-E) */
#define WROOM_32D   0
#define WROVER_E    1
#define WROVER_E_8M 2

/** Model Number
 * 최 상위    :   쓰레기 값(1 Fix) ==> 최 상위 자릿수 '0'일 경우 '0'값 손실되는 점에 의해 적용
 * 상위 2~4   :   모델 정보  ( 모델 테이블 표 참조)
 * 상위 4~7   :   넓이 정보 
 * 상위 8~11  :   높이 정보 
 * 상위 12~13 :   메모리 정보
*/                                          
#define MASTER_0064_0032_16M                1000064003216
#define MASTER_0057_0016_16M                1000057001616
#define MASTER_0046_0016_16M                1000046001616
#define MASTER_0128_0032_16M                1000128003216
#define MASTER_0192_0032_16M                1000192003216
#define MASTER_0128_0064_16M                1000128006416
#define SHOU_0100_0040_16M                  1060100004016

#endif
