#ifndef _CONFIG_
#define _CONFIG_


#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include "../board/model_value_table.h"

#define PANEL_CHAIN 1      // Total number of panels chained one to another

/* Model Table Value 값 */
#define MODEL_VALUE      SHOU_0100_0040_16M

/**************************************************************************/
/**
 *  @brief      - MODEL_VALUE 값에 따른 아래 정보 파싱
    @details    - MODEL_INFO : 모델 넘버 값
                - IDE_MATRIX_WIDTH : 디스플레이 넓이
                - IDE_MATRIX_HEIGHT : 디스플레이 높이
                - FLASH_MOMORY_SIZE : CPU 메모리 크기
*/
/**************************************************************************/
#define MODEL_INFO      ((MODEL_VALUE % 1000000000000) / 10000000000)
#define IDE_MATRIX_WIDTH    ((MODEL_VALUE % 10000000000)/ 1000000)  //  I
#define IDE_MATRIX_HEIGHT   ((MODEL_VALUE % 1000000)/ 100)                             
#define FLASH_MOMORY_SIZE (MODEL_VALUE % 100)  

/**
 * @brief 보드 모델 CONFIG
 */
struct Board_Config {   
  uint8_t model_info = MODEL_INFO;  //  모델 정보
  uint16_t matrix_width = IDE_MATRIX_WIDTH; //  Matrix Width 
  uint16_t matrix_height = IDE_MATRIX_HEIGHT; //  Matrix Height
  uint8_t flash_size = FLASH_MOMORY_SIZE;  //  Flash Size
  // 생성자
  Board_Config(uint8_t _model_info, uint16_t _matrix_width, uint16_t _matrix_height, uint8_t _flash_size) {
    model_info = _model_info;
    matrix_width = _matrix_width;
    matrix_height = _matrix_height;
    flash_size = _flash_size;
  } 
}; 


/**************************************************************************/
/** 
 * @brief   - 모델 정보에 따라 매트릭스 타입 정의
 * @details - 투명 LED =  GlassLedMatrix
 *            그 외 LED = P3RGB64x32MatrixPanel       
*/
/**************************************************************************/
#if MODEL_INFO == SHOU_POV_DISPLAY
  typedef  Adafruit_NeoMatrix MATRIX;
#elif MODEL_INFO == GLASS_LED
  typedef  GlassLedMatrix MATRIX;
#else
  typedef  MatrixPanel_I2S_DMA MATRIX;
#endif

/*  펌웨어 버전 */
#define FIRM_VERSION "0.0.1"

/*  비트별로 단위 나눔  */
#define FIRM_VERSION_MSB_DIGIT    0
#define FIRM_VERSION_MID_DIGIT    0
#define FIRM_VERSION_LSB_DIGIT    1

#endif
