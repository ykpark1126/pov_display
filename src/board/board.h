#ifndef _BOARD_
#define _BOARD_
#include <BluetoothSerial.h>
#include "../communication/bluetooth.h"

/* PIN */
#define WAKEUP_PIN 34     // the number of the wakeup pin
#define WAKEUP_TIME 200
#define MODE_PIN 13 
#define HALL_SENSOR_PIN 36


/*
  적외선 송신 센서 관련
*/

#define IR_TX_PIN 4

/* BATTERY DEFINE */
#define BATTERY_PIN 35 
#define BATTERY_WARING_VALUE 3200 
#define BATTERY_WARING_COUNT 10 
#define BATTERY_SUTDOWN_COUNT 30

#define POPSIGN_BATTERY_MODE 0
#define POPSIGN_NON_BATTERY_MODE 1

/* LED BRIGHT MODE */
/* 1 : Led Bright Level High, 0 : Led Bright Level Low */
#define LED_BRIGHT_MODE 0

/* LED BRIGHT DIV LEVEL */
#define LED_DIV_LEVEL 10

#define BG_COL_DIV 10

#define MAX_BRIGHT_LEVEL 31

#define DEFAULT_LED_LEVEL_BATTERY    3
#define DEFAULT_LED_LEVEL_PERMANENT 3 


/* 
BOARD MODE COUNT
*/
#define MODE_COUNT 5

/* BOARD MODE */
#define MODE_UART 5
#define MODE_BLUETOOTH 0
#define MODE_DISPLAY 1
#define MODE_CLOCK 4

/* LittleFS FILE SETTING.TXT BYTE INDEX INFO */
#define SETTING_IDX_MODE                        0
#define SETTING_IDX_BRIGHT                      1
#define SETTING_IDX_DP_CHECK                    2
#define SETTING_IDX_LR_CHECK                    3
#define SETTING_IDX_WI_CHECK                    4
#define SETTING_IDX_CK_CHECK                    5
#define SETTING_IDX_VERSION_FIRST_DIGIT         6
#define SETTING_IDX_VERSION_SECOND_DIGIT        7
#define SETTING_IDX_VERSION_THIRD_DIGIT         8
#define SETTING_IDX_LOGO_TIME_FIRST_DIGIT       9
#define SETTING_IDX_LOGO_TIME_SECOND_DIGIT      10
#define SETTING_IDX_LOGO_TIME_THIRD_DIGIT       11
#define SETTING_IDX_LOGO_TIME_FOUTH_DIGIT       12
#define SETTING_IDX_PAGE_TIME_FIRST_DIGIT       13
#define SETTING_IDX_PAGE_TIME_SECOND_DIGIT      14
#define SETTING_IDX_PAGE_TIME_THIRD_DIGIT       15
#define SETTING_IDX_PAGE_TIME_FOUTH_DIGIT       16
#define SETTING_IDX_LOGO_ENABLE_BIT             17
#define SETTING_IDX_PAGE_CHANGE_WAY             18

/* Board Setting Info Data Count*/
#define SETTING_DATA_COUNT 19

/* Preference FILE BOARD KEY BYTE INDEX INFO */
#define BOARD_INFO_IDX_ROTATION                 0   //  로테이션 값 (0 == 가로 or 1 == 세로)
#define BOARD_INFO_IDX_LOGO_REPEAT_ON_OFF       1   //  로고 반복 OnOff
#define BOARD_INFO_IDX_GAMMA_ENABLE             2   //  Led Gamma Table 적용 enable value

/* Board Info Preference Data Total Count*/
#define BOARD_INFO_PREFS_IDX_TOTAL_COUNT 10

/* Preference 보드 CONFIG */
#define BOARD_INFO_IDX_MODEL_VALUE                  0   //  보드 모델 값
#define BOARD_INFO_IDX_MATRIX_WIDTH_TOP_8BIT        1   //  넓이 상위 8비트(2bytes)
#define BOARD_INFO_IDX_MATRIX_WIDTH_LOW_8BIT        2   //  넓이 하위 8비트(2bytes)
#define BOARD_INFO_IDX_MATRIX_HEIGHT_TOP_8BIT       3   //  높이 상위 8비트(2bytes)
#define BOARD_INFO_IDX_MATRIX_HEIGHT_LOW_8BIT       4   //  높이 상위 8비트(2bytes)
#define BOARD_INFO_IDX_FLASH_MEMORY_SIZE            5   //  플래시 크기

/* Preference Random Content Page Idx */
#define NVS_RANDOM_CONTENT_PAGE_TYPE_IDX    0        //  랜덤 컨텐츠 타입 Index
#define NVS_RANDOM_CONTENT_PAGE_GAGE_COUNT_IDX   1   //  게이지 카운트 Index
#define NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FIRST_DIGIT_IDX   2   //  게이지 카운트 Index
#define NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_SECOND_DIGIT_IDX   3   //  게이지 카운트 Index
#define NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_THIRD_DIGIT_IDX   4   //  게이지 카운트 Index
#define NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FOUTH_DIGIT_IDX   5   //  게이지 카운트 Index

#define RANDOME_CONTENT_NVS_LENGTH   6   //  Random Content NVS Flash Length

/* BOLTAGE */
#define REGISTER_DIST 5 // Registance Distribution

/* LOGO_BIT_MASK */
#define BLUETOOTH_MODE_LOGO_ENABLE_BIT_MASK     0b00000001
#define DISPLAY_MODE_LOGO_ENABLE_BIT_MASK       0b00000010
#define CLOCK_MODE_LOGO_ENABLE_BIT_MASK         0b00000100
#define LORA_MODE_LOGO_ENABLE_BIT_MASK          0b00001000
#define WIFI_MODE_LOGO_ENABLE_BIT_MASK          0b00010000

#define LOGO_ENABLE_BIT_MASK_DEFAULT            0b00011111
/* LOGO REPEAT FLAG */
#define MODE_LOGO_REPEAT        1
#define MODE_LOGO_NON_REPEAT    0

/* 로테이션 모드 값 (가로 or 세로 ) */
#define ROTATION_VALUE_LANDSCAPE    0   //  가로 로테이션
#define ROTATION_VALUE_PORTRAIT     1   //  세로 로테이션

/* UART GPIO PORT */
#define RX 18
#define TX 19

/* Preferences Name */
#define PREFS_NAME "board"

/* Preferences Section Key */
#define PREFS_KEY_LOGO_PAGE  "logo"
#define PREFS_KEY_BOARD_CONFIG_PAGE "board_config"
#define PREFS_KEY_BOARD_INFO_PAGE "board"
#define PREFS_KEY_RANDOM_CONTENT_INFO_PAGE "random_content" //  랜덤 페이지 & 게이지 출력 설정 값 nvs

/* 인터럽트 타이머 관련 */
#define ITTR_LED_TIMER_SETTING_MODE             40
#define ITTR_LED_TIMER_128_32_SETTING_MODE      80    
#define ITTR_LED_TIMER_STABILITY_MODE_UPDATE    100
#define ITTR_LED_TIMER_DISPLAY_MODE             30


/* 근접 센서 I2C 채널 관련 */
#define SENS1_I2C_SDA 25
#define SENS1_I2C_SCL 33
#define SENS2_I2C_SDA 27
#define SENS2_I2C_SCL 26
#define SENS3_I2C_SDA 12
#define SENS3_I2C_SCL 14
#define SENS4_I2C_SDA 15
#define SENS4_I2C_SCL 13

/* 근접 센서 인터럽트 핀 */
#define PROXI_SENS1 39
#define PROXI_SENS2 34
#define PROXI_SENS3 35
#define PROXI_SENS4 32


#define IR_MOTOR_STOP   0xA5035A // Stop
#define IR_MOTOR_START  0xA5025A // Start

void power_switch_check(void* matrix, Bluetooth* blue);
void get_battery_amount(void* matrix, Bluetooth* blue, LedPannel* ledPannel);
void getSettingInfo(uint8_t* modeNum, float* ledDivLev, uint8_t* modeCount, uint8_t isModeChecked[], uint8_t useModeLen, bool* checkUpdate, uint8_t* startModeNum, int* logoTime, int* pageTime);
void getMatrixSizeRange(int16_t widthRange, int16_t heightRange, int * width, int * height, uint8_t rotationVal);
void board_config_setting(char* data, BufferSerial* pBufferSerial);  //  board Config Setting (모델, Pixel Width Hieght Size, Flash Memory)
void set_led_gamma_enable(void* matrix, char* data);
void set_lora_sub_mode_type(char* data);
void set_lora_gage_mode_gage_count(char* data);
void set_wegiht_recognition_min_value(char* data);
void all_prox_sens_init(void);
bool prox_init(uint8_t ch);
void hall_sensor_init(void);
void ir_init(void);
void ir_motor_control(bool onoff);

#endif
