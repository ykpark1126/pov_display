#include <Arduino.h>
#include <FS.h>
#include <LittleFS.h>
#include <Preferences.h>
#include "../board/board.h"
#include "..//utils/etc.h"
#include "..//led_matrix/ledpannel.h"
#include "..//flash/flash.h"
#include "..//board/config.h"
#include "../communication/buffer_serial.h"
#include "../communication/command.h"
#include <VCNL3036.h>
#include <Wire.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>

//  Board Config Info 
extern Board_Config g_board_config;

// power timer check
unsigned long pre_time_power_check = 0;
//  Power WakeUp Conut
int wakeUpCount = 0;
//  Power Sleep Count
int sleepCount = 0;
//  Battery Read Value
int adcValue = 0; 
//  Board 정보 배터리 Or 상시 전원
extern int8_t isBatteryModeInfo;
// 로고 이용 
extern uint8_t logoEnable;
// 페이지 전환 모드 (수동 or 자동) 
extern bool autoNextPageMode;
// 블루투스 이름 
extern String bt_name;

// 페이지 Enable List --> 페이지 사용가능하는지 가지고 있는 버퍼 (모든 페이지 접근 하는데에 시간이 오래걸려 해당 버퍼 사용)
extern uint8_t pageEnableList[PAGE_TOTAL_COUNT];  


/*  PAGE TIME SWICH INFO   */
extern bool timeSwitchIsEnable[PAGE_TOTAL_COUNT];
extern int8_t timeSwitchStartHour[PAGE_TOTAL_COUNT], timeSwitchStartMinute[PAGE_TOTAL_COUNT];
extern int8_t timeSwitchFromHour[PAGE_TOTAL_COUNT], timeSwitchFromMinute[PAGE_TOTAL_COUNT];

/* Preferences Flash */
extern Preferences prefs;  
extern uint8_t prefsBoardInfoArray[BOARD_INFO_PREFS_IDX_TOTAL_COUNT];

//  Lora Sub mode (페이지 출력, 게이지 출력)
extern int g_lora_sub_mode_type;
//  Gage Mode 관련 변수 - 게이지 갯수
extern int g_gage_count;
//  무게 인식 최소 값
extern int g_weight_recognitition;

void power_switch_check(void* matrix, Bluetooth* blue)
{
  if (digitalRead(WAKEUP_PIN) == 0) 
  {
    if(delayExt(millis(), &pre_time_power_check, 10)) 
    {
      ++wakeUpCount;
      if (wakeUpCount >= WAKEUP_TIME) {
        wakeUpCount = 0;
        Serial.println("going to sleep ");
        blue->disconnect();
        esp_deep_sleep_start();
      }
    }
  }
}

void get_battery_amount(void* matrix, Bluetooth* blue, LedPannel* ledPannel) {
  bool previousMatrixStatus = getMatrixBeginStatus();
 
  adcValue = analogRead(BATTERY_PIN); 
  // Serial.print("adcValue : ");  Serial.println(adcValue * REGISTER_DIST);

  if (adcValue * REGISTER_DIST <= BATTERY_WARING_VALUE) {
    sleepCount++;
    ((MATRIX*)matrix)->setTextSize(1);

    if(sleepCount >= BATTERY_SUTDOWN_COUNT) {
      Serial.println("going to sleep ");
      ledPannel->set_matrix_begin();
      ((MATRIX*)matrix)->fillScreen(0);
      ((MATRIX*)matrix)->setCursor(0,10);
      ((MATRIX*)matrix)->println("shut down...");
      delay(5000);
      blue->disconnect();
      ledPannel->set_matrix_end();
      esp_deep_sleep_start();
    }
    else if (sleepCount >= BATTERY_WARING_COUNT && sleepCount % 10 == 0) { //10 == 0
      // Serial.print("Battery : ");  Serial.println(adcValue * REGISTER_DIST);
      // Serial.print("sleepCount : ");  Serial.println(sleepCount);

      // Serial.println("Low Battery. . . ");
      ledPannel->set_matrix_begin();
      ((MATRIX*)matrix)->fillScreen(0);
      for(int i = 0; i < 10; i++) {
        ((MATRIX*)matrix)->setCursor(0,10);
        ((MATRIX*)matrix)->print("Low Battery...");
        delay(500);
        fillScreenEx(0, 0, 0, (MATRIX*)matrix);
        delay(100);
      }
      fillScreenEx(0, 0, 0, (MATRIX*)matrix);

      if (previousMatrixStatus == false) { // 이전 matrix가 Stop 상태였다면 matrix end
        ledPannel->set_matrix_end();
        Serial.println("previousMatrixStatus == false. . . ");
      }      
    }
  } else {
    sleepCount = 0;
  }
}

void getSettingInfo(uint8_t* modeNum, float* ledDivLev, uint8_t* modeCount, uint8_t isModeChecked[], uint8_t useModeLen, bool* checkUpdate, uint8_t* startModeNum, int* logoTime, int* displayPageTime) {
  uint8_t buffer[SETTING_DATA_COUNT];
  int i = 0;
  int readBufferIndex = 0;
  bool startModeSyncOk = false; //시작 모드가 사용 모드 리스트에 있는지 체크.
  uint8_t verInfo[3] = {0};
  File file;

  /*  블루투스 이름 LittleFS /bt_name.txt 에서 읽어 옴  */
  char* btNameBuffer;
  int btFileSize = 0;
  int btFileIdx = 0;
  file = LittleFS.open("/bt_name.txt");
  btFileSize = file.size();
  if (btFileSize != 0)  {
    btNameBuffer = (char*)malloc(btFileSize);
    while(file.available()){
      btNameBuffer[btFileIdx++] = file.read();
    }
    bt_name.clear();
    bt_name = btNameBuffer;
    free(btNameBuffer);
  }
  file.close();


  // 기본 보드 Setting Flash Get ( Mode, Led Bright Info Flash Read (StartModeNum, UseMode, CheckUpadate Value, . . .))
  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  if (!(readBufferIndex  == SETTING_DATA_COUNT)) {  //  플래시에 모드 정보 값이 저장되어 있지 않을 경우 예외 처리 (기본 모드로 동작)
    uint8_t brightLevel;
    *modeCount = 2;

    isModeChecked[0] = MODE_BLUETOOTH;
    isModeChecked[1] = MODE_DISPLAY;
    #if MODEL_INFO == LORA_MODEL  //   로라 모델인 경우 defalut 로라 모드 추가
      isModeChecked[2] = MODE_LORA;
      *modeCount = 3;
    #endif

    if (isBatteryModeInfo == POPSIGN_BATTERY_MODE) {
      *ledDivLev = getLedDivValue(DEFAULT_LED_LEVEL_BATTERY);  //  if case : 배터리 전원 모델 , 밝기 단계 3, 밝기 레벨 8
      brightLevel = DEFAULT_LED_LEVEL_BATTERY;
    }
    else {
      *ledDivLev = getLedDivValue(DEFAULT_LED_LEVEL_PERMANENT); // else case :  상시 전원 모델 , 밝기 단계 4, 밝기 레벨 10
      brightLevel = DEFAULT_LED_LEVEL_PERMANENT;
    }

    /*  Mode Defalut Setting  */
    buffer[SETTING_IDX_MODE] = MODE_BLUETOOTH;   
    buffer[SETTING_IDX_BRIGHT] = brightLevel;
    buffer[SETTING_IDX_DP_CHECK] = isModeChecked[MODE_DISPLAY];
    buffer[SETTING_IDX_LR_CHECK] = 0;
    buffer[SETTING_IDX_WI_CHECK] = 0;
    buffer[SETTING_IDX_CK_CHECK] = isModeChecked[MODE_CLOCK];
    buffer[SETTING_IDX_VERSION_FIRST_DIGIT] = FIRM_VERSION_MSB_DIGIT;
    buffer[SETTING_IDX_VERSION_SECOND_DIGIT] = FIRM_VERSION_MID_DIGIT;
    buffer[SETTING_IDX_VERSION_THIRD_DIGIT] = FIRM_VERSION_LSB_DIGIT;
    buffer[SETTING_IDX_LOGO_TIME_FIRST_DIGIT] = 0;
    buffer[SETTING_IDX_LOGO_TIME_SECOND_DIGIT] = 0;
    buffer[SETTING_IDX_LOGO_TIME_THIRD_DIGIT] = ((2000 & 0x0000ff00) >> 8);
    buffer[SETTING_IDX_LOGO_TIME_FOUTH_DIGIT] = (2000 & 0x000000ff);
    buffer[SETTING_IDX_PAGE_TIME_FIRST_DIGIT] = 0;
    buffer[SETTING_IDX_PAGE_TIME_SECOND_DIGIT] = 0;
    buffer[SETTING_IDX_PAGE_TIME_THIRD_DIGIT] = ((5000 & 0x0000ff00) >> 8);
    buffer[SETTING_IDX_PAGE_TIME_FOUTH_DIGIT] = (5000 & 0x000000ff);
    
    buffer[SETTING_IDX_LOGO_ENABLE_BIT] = LOGO_ENABLE_BIT_MASK_DEFAULT;
    buffer[SETTING_IDX_PAGE_CHANGE_WAY] = DISPLAY_AUTO_PAGE_CHANGE;

    writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
    return;
  } 

  *modeNum = buffer[SETTING_IDX_MODE];   //  시작 모드 설정
  *startModeNum = *modeNum;
  *ledDivLev = getLedDivValue(buffer[SETTING_IDX_BRIGHT]);

  *logoTime = 0;
  *logoTime =  *logoTime | buffer[SETTING_IDX_LOGO_TIME_FIRST_DIGIT] << 24;
  *logoTime =  *logoTime | buffer[SETTING_IDX_LOGO_TIME_SECOND_DIGIT] << 16;
  *logoTime =  *logoTime | buffer[SETTING_IDX_LOGO_TIME_THIRD_DIGIT] << 8;
  *logoTime =  *logoTime | buffer[SETTING_IDX_LOGO_TIME_FOUTH_DIGIT];

  *displayPageTime = 0;
  *displayPageTime =  *displayPageTime | buffer[SETTING_IDX_PAGE_TIME_FIRST_DIGIT] << 24;
  *displayPageTime =  *displayPageTime | buffer[SETTING_IDX_PAGE_TIME_SECOND_DIGIT] << 16;
  *displayPageTime =  *displayPageTime | buffer[SETTING_IDX_PAGE_TIME_THIRD_DIGIT] << 8;
  *displayPageTime =  *displayPageTime | buffer[SETTING_IDX_PAGE_TIME_FOUTH_DIGIT];

  logoEnable = buffer[SETTING_IDX_LOGO_ENABLE_BIT];
  autoNextPageMode = buffer[SETTING_IDX_PAGE_CHANGE_WAY];
  /*
   * 비트 연산하지 않고 byte to int 대입 하는 방법
  */  
  // 1 - int p* = *(int*)(buffer + SETTING_IDX_LOGO_TIME_FIRST_DIGIT);
  //     *logoTime = *p;
  // 2 - memcpy(logoTime, buffer + SETTING_IDX_LOGO_TIME_FIRST_DIGIT, 4)

  i = SETTING_IDX_DP_CHECK; 
  for (int cnt = 1; cnt < useModeLen; cnt++) {
    if (buffer[i++]) {
      isModeChecked[*modeCount] = cnt; 
      *modeCount += 1;
    }
  }

  for (int cnt = 0; cnt < MODE_COUNT; cnt++) {  //  isModeChecked 배열에서 시작 모드가 들어있는 인자를 받아옴.
    if (isModeChecked[cnt] == *modeNum) {
      *modeNum = (uint8_t)cnt;
      startModeSyncOk = true;
      break;
    }
  }
  if (startModeSyncOk == false)  *modeNum = 0; // 시작 모드가 사용 모드 리스트에 없다면 블루투스 모드를 시작모드로 설정

  verInfo[0] = buffer[SETTING_IDX_VERSION_FIRST_DIGIT]; 
  verInfo[1] = buffer[SETTING_IDX_VERSION_SECOND_DIGIT]; 
  verInfo[2] = buffer[SETTING_IDX_VERSION_THIRD_DIGIT];

  if (verInfo[0] != FIRM_VERSION_MSB_DIGIT || verInfo[1] != FIRM_VERSION_MID_DIGIT || verInfo[2] != FIRM_VERSION_LSB_DIGIT) {  //  checkUpdate 값이 50이라면 최초 Update된 상태. 1회 Update Ok 메세지 뛰우고 다시 플래시에 checkUpdate 다른 값 Write
    buffer[SETTING_IDX_VERSION_FIRST_DIGIT] = FIRM_VERSION_MSB_DIGIT;
    buffer[SETTING_IDX_VERSION_SECOND_DIGIT] = FIRM_VERSION_MID_DIGIT;
    buffer[SETTING_IDX_VERSION_THIRD_DIGIT] = FIRM_VERSION_LSB_DIGIT;
    writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
    *checkUpdate = true;
  } 

  //  페이지 Timer Enable List Flash Get
  uint8_t pageTimerSwitchBuffer[PAGE_TOTAL_COUNT * 5] = {0};
  readBufferIndex = 0;
  readBufferIndex = readFile(LittleFS, "/pageTimer.txt", pageTimerSwitchBuffer); 
  if (readBufferIndex != FILE_NOT_EXIST) {
    for (int i = 0; i < PAGE_TOTAL_COUNT; i++) {
      timeSwitchIsEnable[i] = pageTimerSwitchBuffer[i * 5 + 0];
      timeSwitchStartHour[i] = pageTimerSwitchBuffer[i * 5 + 1];
      timeSwitchStartMinute[i] = pageTimerSwitchBuffer[i * 5 + 2];
      timeSwitchFromHour[i] = pageTimerSwitchBuffer[i * 5 + 3];
      timeSwitchFromMinute[i] = pageTimerSwitchBuffer[i * 5 + 4];
    }
  }

  //  페이지 Enable List Flash Get
  readBufferIndex = readFile(LittleFS, FLASH_PAGE_ENABLE_LIST, pageEnableList);
}

/**************************************************************************/
/**
    @brief    - 필요한 매트릭스 사이즈 반환 함수 -
    @details  매트릭스에 출력되는 사이즈에 따라 범위에 맞는 매트릭스 사이즈를 반환 해준다.
              최소 매트릭스 값 rotation 64x32 - width 64 height 32 , 
                    "         rotation 32x64 - width 32 height 64  

    @param    widthRange    매트릭스에 필요한 width (width값의 범위)
    @param    heightRange   매트릭스에 필요한 height (height값의 범위)
    @param    width         width 값 전달하기 위한 포인터 변수 (call by ref)
    @param    height        height 값 전달하기 위한 포인터 변수 (call by ref) 
    @param    rotationVal   현재 매트릭스 로테이션 값
*/
/**************************************************************************/

void getMatrixSizeRange(int16_t widthRange, int16_t heightRange, int * width, int * height, uint8_t rotationVal) {
  int16_t widthValue, heightValue;

  if (widthRange == 0 && heightRange == 0) return;  // width, height 크기 0 일시 return

  if (rotationVal == ROTATION_VALUE_LANDSCAPE) {
    widthValue = (int16_t) (widthRange / RANGE_VALUE_16);
    *width = (widthValue + 1) * RANGE_VALUE_16;
    heightValue = (int16_t) (heightRange / RANGE_VALUE_16);
    *height = (heightValue + 1) * RANGE_VALUE_16;

  } else if (rotationVal == ROTATION_VALUE_PORTRAIT) {
    widthValue = (int16_t) (heightRange / RANGE_VALUE_16);
    *width = (widthValue + 1) * RANGE_VALUE_16;
    heightValue = (int16_t) (widthRange / RANGE_VALUE_16);
    *height = (heightValue + 1) * RANGE_VALUE_16;
  }

  if (*width < g_board_config.matrix_width)  *width = g_board_config.matrix_width;    //  최소 Matrix 값 검사   
  if (*height < g_board_config.matrix_height) *height = g_board_config.matrix_height; //  최소 Matrix 값 검사  
}

/**
 * @brief Board Config Setting (모델, Pixel Width Hieght Size, Flash Memory)
 * @param pBufferSerial 통신 프로세싱 객체 (BT, UART등등...)
 * @param data 6Bytes PayLoad (0- 모델정보, 1~2 매트릭스 넓이, 3~4 매트릭스 높이, 5 플래시 사이즈)
 */
void board_config_setting(char* data, BufferSerial* pBufferSerial) {
  int _idx = 0;
  uint8_t _model_info;  //  모델 정보
  uint16_t _matrix_width = 0, _matrix_height = 0; //  Matrix Width, Height 
  uint8_t _flash_size;  //  Flash Size

  _model_info = data[_idx++];
  _matrix_width = (data[_idx++] << 8);
  _matrix_width = _matrix_width | data[_idx++];
  _matrix_height = (data[_idx++] << 8);
  _matrix_height = _matrix_height | data[_idx++];
  _flash_size = data[_idx++];

  // Serial.printf("_model_info : %d \n", _model_info);
  // Serial.printf("_matrix_width : %d \n", _matrix_width);
  // Serial.printf("_matrix_height : %d \n", _matrix_height);
  // Serial.printf("_flash_size : %d \n", _flash_size);

  g_board_config.model_info = _model_info;
  g_board_config.matrix_width = _matrix_width;
  g_board_config.matrix_height = _matrix_height;
  g_board_config.flash_size = _flash_size;

  prefs.begin(PREFS_NAME);
  prefs.putBytes(PREFS_KEY_BOARD_CONFIG_PAGE, (uint8_t*) data, _idx);  //  _idx is count
  prefs.end();

  pBufferSerial->transmit_data(SET_BOARD_CONFIG_SETTING); //  SET_BOARD_CONFIG_SETTING 응답

  ESP.restart();
}
  
/**
 * @brief Set the led gamma enable object
 * 
 * @param matrix matrix object
 * @param data enable value
 */
void set_led_gamma_enable(void* matrix, char* data) {
  // Serial.printf("set_led_gamma_enable \n");

  prefsBoardInfoArray[BOARD_INFO_IDX_GAMMA_ENABLE] = data[0];
  // Serial.printf("prefsBoardInfoArray[BOARD_INFO_IDX_GAMMA_ENABLE] :  %d \n", prefsBoardInfoArray[BOARD_INFO_IDX_GAMMA_ENABLE]);
  //((MATRIX*)matrix)->gamma_enable =  prefsBoardInfoArray[BOARD_INFO_IDX_GAMMA_ENABLE];

  prefs.begin(PREFS_NAME);
  prefs.putBytes(PREFS_KEY_BOARD_INFO_PAGE, prefsBoardInfoArray, BOARD_INFO_PREFS_IDX_TOTAL_COUNT);
  prefs.end();  
}

/**
 * @brief Set the lora sub mode type object
 *       로라 서브 모드 타입 Flash 저장 함수
 * @param data - 0 == page print mode, 1 == gage print mode
 */
void set_lora_sub_mode_type(char* data) {
  Serial.printf("set_lora_sub_mode_type \n");
  g_lora_sub_mode_type = data[0];

  uint8_t _buf[RANDOME_CONTENT_NVS_LENGTH];
  _buf[NVS_RANDOM_CONTENT_PAGE_TYPE_IDX] = g_lora_sub_mode_type;
  _buf[NVS_RANDOM_CONTENT_PAGE_GAGE_COUNT_IDX] = g_gage_count;
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FIRST_DIGIT_IDX] =  (uint8_t)((g_weight_recognitition & 0xff000000) >> 24);
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_SECOND_DIGIT_IDX] = (uint8_t)((g_weight_recognitition & 0x00ff0000) >> 16);
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_THIRD_DIGIT_IDX] =  (uint8_t)((g_weight_recognitition & 0x0000ff00) >> 8);
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FOUTH_DIGIT_IDX] =  (uint8_t)((g_weight_recognitition & 0x000000ff));

  prefs.begin(PREFS_NAME);
  prefs.putBytes(PREFS_KEY_RANDOM_CONTENT_INFO_PAGE, (uint8_t*) _buf, RANDOME_CONTENT_NVS_LENGTH);  //  _idx is count
  prefs.end();
}

/**
 * @brief Set the lora sub mode type object
 *        로라 게이지 출력 모드 막대 갯수 Flash 저장 함수
 * @param data - 막대 Count (0~255)
 */
void set_lora_gage_mode_gage_count(char* data) {
  Serial.printf("set_lora_gage_mode_gage_count \n");
  g_gage_count = data[0];

  uint8_t _buf[RANDOME_CONTENT_NVS_LENGTH];
  _buf[NVS_RANDOM_CONTENT_PAGE_TYPE_IDX] = g_lora_sub_mode_type;
  _buf[NVS_RANDOM_CONTENT_PAGE_GAGE_COUNT_IDX] =  g_gage_count;
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FIRST_DIGIT_IDX] =  (uint8_t)((g_weight_recognitition & 0xff000000) >> 24);
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_SECOND_DIGIT_IDX] = (uint8_t)((g_weight_recognitition & 0x00ff0000) >> 16);
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_THIRD_DIGIT_IDX] =  (uint8_t)((g_weight_recognitition & 0x0000ff00) >> 8);
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FOUTH_DIGIT_IDX] =  (uint8_t)((g_weight_recognitition & 0x000000ff));
  
  prefs.begin(PREFS_NAME);
  prefs.putBytes(PREFS_KEY_RANDOM_CONTENT_INFO_PAGE, (uint8_t*) _buf, RANDOME_CONTENT_NVS_LENGTH);  //  _idx is count
  prefs.end();
}

/**
 * @brief Set the wegiht recognition min value object
 * 
 * @param data 
 */
void set_wegiht_recognition_min_value(char* data) {
  Serial.printf("set_wegiht_recognition_min_value \n");
  g_weight_recognitition = 0;
  g_weight_recognitition = g_weight_recognitition | (data[0] << 24);
  g_weight_recognitition = g_weight_recognitition | (data[1] << 16);
  g_weight_recognitition = g_weight_recognitition | (data[2] << 8);
  g_weight_recognitition = g_weight_recognitition | data[3];

  Serial.printf("g_weight_recognitition : %d \n", g_weight_recognitition);

  uint8_t _buf[RANDOME_CONTENT_NVS_LENGTH];
  _buf[NVS_RANDOM_CONTENT_PAGE_TYPE_IDX] = g_lora_sub_mode_type;
  _buf[NVS_RANDOM_CONTENT_PAGE_GAGE_COUNT_IDX] =  g_gage_count;
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FIRST_DIGIT_IDX] =  data[0];
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_SECOND_DIGIT_IDX] = data[1];
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_THIRD_DIGIT_IDX] =  data[2];
  _buf[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FOUTH_DIGIT_IDX] =  data[3];

  prefs.begin(PREFS_NAME);
  prefs.putBytes(PREFS_KEY_RANDOM_CONTENT_INFO_PAGE, (uint8_t*) _buf, RANDOME_CONTENT_NVS_LENGTH);  //  _idx is count
  prefs.end();
}


/** 
 * @brief - 근접 센서 체크
**/
void IRAM_ATTR check_proxi_sensor() 
{
  // stop motor
	ir_motor_control(false);
}


void all_prox_sens_init(void)
{	
  bool ret;
	ret =	prox_init(1);
	if (ret == false)
		Serial.printf("1 Sensor Fail\n");
	ret =	prox_init(2);
	if (ret == false)
		Serial.printf("2 Sensor Fail\n");
	ret =	prox_init(3);
	if (ret == false)
		Serial.printf("3 Sensor Fail\n");
	ret =	prox_init(4);
	if (ret == false)
		Serial.printf("4 Sensor Fail\n");

  pinMode(PROXI_SENS1, INPUT_PULLUP);
  pinMode(PROXI_SENS2, INPUT_PULLUP);
  pinMode(PROXI_SENS3, INPUT_PULLUP);
  pinMode(PROXI_SENS4, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(PROXI_SENS1), check_proxi_sensor, RISING);  
  attachInterrupt(digitalPinToInterrupt(PROXI_SENS2), check_proxi_sensor, RISING); 
  attachInterrupt(digitalPinToInterrupt(PROXI_SENS3), check_proxi_sensor, RISING); 
  attachInterrupt(digitalPinToInterrupt(PROXI_SENS4), check_proxi_sensor, RISING); 
}


bool prox_init(uint8_t ch)
{
	int sda, scl;
	if (ch == 1) {
		sda = SENS1_I2C_SDA;
		scl = SENS1_I2C_SCL;
	}
	else if (ch == 2) {
		sda = SENS2_I2C_SDA;
		scl = SENS2_I2C_SCL;
	}
	else if (ch == 3) {
		sda = SENS3_I2C_SDA;
		scl = SENS3_I2C_SCL;
	}
  else if (ch == 4) {
		sda = SENS4_I2C_SDA;
		scl = SENS4_I2C_SCL;
	}
	
	VCNL3036 sens;
	Wire.begin(sda, scl);
	if (sens.begin(true) == false)
		return false;
	else 
	{
		sens.setPSResolution(RESOL_12BIT);
		sens.setPSDuty(PS_DUTY_40);// 4.85ms
		sens.setPSIT(PS_IT_8T0);  // 8T
		sens.setINTMode(PS_INT_CLOSING); // interrupt by closing
		sens.enableINTProxMode();
		sens.setPSTHDL(500);
		sens.setPSTHDH(4000); // 위에 선택한 12bit(4096)의 값으로 센서의 감도를 나타내는 것으로 판단됨
		sens.setPSPers(PS_PERS_2); // 2번 이상 발생해야 인터럽트 발생
		Wire.end();
		return true;
	}
}

IRsend irsend(IR_TX_PIN);  // Set the GPIO to be used to sending the message.
void ir_init(void)
{
  pinMode(IR_TX_PIN, OUTPUT); // 1번을 출력으로 설정
  irsend.begin();
}

void hall_sensor_init(void)
{
  pinMode(HALL_SENSOR_PIN, INPUT_PULLUP);
}
 
/*
IR을 통해서 모터를 제어 한다.
*/
void ir_motor_control(bool onoff)
{
  if (onoff == true)
  {
    irsend.sendNEC(IR_MOTOR_START, 24);
    Serial.printf("%s\n", __FUNCTION__);
  }
  else {
    irsend.sendNEC(IR_MOTOR_STOP, 24);
  }
}