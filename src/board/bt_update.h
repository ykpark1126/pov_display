#ifndef _BT_UPDATE_
#define _BT_UPDATE_

// 다음 파티션 파일 요청
#define UPDATE_REQUEST_NEXT_PARTITION 0
// 모든 업데이트 바이너리 파티션 수신 완료 (업데이트 성공) 
#define UPDATE_REQUESET_UPDATE_SUCCESS 1
// 업데이트 실패 응답 값
#define UPDATE_REQUESET_UPDATE_FAIL 2

/*  Get Bt Update Binary File Communication Proceed */
void bt_update_start(char* data);
void bt_update_ongoing(char* data, int len); 
void bt_update_end(); 
void bt_update_cancel(); 

#endif



