#include <Arduino.h>
#include <LittleFS.h>
#include <Preferences.h>
#include <FS.h>
#include <Update.h>

#include "../../popsign.h"
#include "../board/bt_update.h"
#include "../communication/command.h"
#include "../communication/buffer_serial.h"

//  업데이트 바이너리 파일 크기
uint32_t g_update_file_size = 0;

//  1회에 보내는 최대 바이트 크기
uint16_t g_single_send_byte_size = 0;

//  업데이트 동안 받을 통신 총 횟 수
uint16_t g_transmit_count = 0;

/*  업데이트 파일 정상 수신 Check 리스트 */
uint8_t* g_packet_index_check = NULL;

// 업데이트 바이너리 파일 데이터
uint8_t* g_update_binary_data = NULL;

// 업데이트 파일 파티션
bool g_parition_last = false;

//블루투스 통신 패킷 프로토콜 처리 객체
extern BufferSerial bufferSerial;

//  LED 처리 관려 객체 매트릭스 
extern LedPannel ledpannel;

//  LED 출력 객체
extern MATRIX *matrix;


/**
 * @brief 업데이트 Start Header Packet Recive
 * @details - 파일 크기만큼 파티션으로 수신 
 *          - Cycle - start -> ongoing -> end
 *          - 받은 파일 순서대로 App01영역으로 Write 후 마지막 파일 수신 (g_parition_last) Update.end() 호출하여 업데이트 진행
 * @param data - [0] 파일 버전, [1~4] 파일 사이즈, [5~6] 1회 보내는 바이트 크기, 
 */
void bt_update_start(char* data) {
    PRINTF("bt_update_start \n");
    uint8_t _update_file_ver;   //  업데이트 파일 버전
    int _idx = 0;   //  패킷 인덱스

    ledpannel.print_str_matrix_basic(1, "업데이트\n진행 중", CMD_ACT_DEFALUT, 0xFFFF);

    _update_file_ver = data[_idx++];    //  파일 버전

    g_update_file_size = 0; //  파티션 파일 크기 (4Bytes)
    g_update_file_size = g_update_file_size | (data[_idx++] << 24);
    g_update_file_size = g_update_file_size | (data[_idx++] << 16);
    g_update_file_size = g_update_file_size | (data[_idx++] << 8);
    g_update_file_size =  g_update_file_size | data[_idx++];

    g_single_send_byte_size = 0;    //  한번에 보내는 BT Byte Length (2Bytes)
    g_single_send_byte_size = g_single_send_byte_size | (data[_idx++] << 8);
    g_single_send_byte_size = g_single_send_byte_size | data[_idx++];

    g_parition_last = data[_idx++]; //  마지막 파티션 유무 (1이 넘어오는 경우 해당 파티션 Write 후 업데이트 마무리, 아닌 경우 다음 파티션 파일 요청)

    g_transmit_count = (g_update_file_size / g_single_send_byte_size);  
    if (g_update_file_size % g_single_send_byte_size != 0)  g_transmit_count += 1; //  나머지 값이 있다면 + 1 (마지막 잔여 바이트 처리)

    if (g_packet_index_check != NULL) {
        free(g_packet_index_check);
    }
    g_packet_index_check = (uint8_t*) calloc (g_transmit_count, sizeof(uint8_t));

    if (g_update_binary_data != NULL) free (g_update_binary_data);
    else {  //  업데이트 파일 Null인 경우에만 Update Begin (여러번 Begin 방지)
        if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
            Update.printError(Serial);
        }
    }
    g_update_binary_data = (uint8_t*) malloc (sizeof(uint8_t) * g_update_file_size);  //  1 is cmd
    bufferSerial.transmit_data(BT_UPDATE_START_HEADER_INFO);
}

/**
 * @brief 업데이트 바이너리 파일 Data 수신
 * 
 * @param data - [0~1] 현재 보내는 패킷 Num, [2~] 바이너리 파일 데이터
 */
void bt_update_ongoing(char* data, int len) {
    // PRINTF("bt_update_ongoing \n");
    int _idx = 0;   //  패킷 인덱스
    uint16_t _transmitNum = 0;
    _transmitNum = _transmitNum | data[_idx++] << 8;
    _transmitNum = _transmitNum | data[_idx++];

    g_packet_index_check[_transmitNum] = RECIVE_ACK;

    while (_idx < len) {
        g_update_binary_data[(_transmitNum * g_single_send_byte_size) + (_idx + - 2)] = data[_idx];
        _idx++;
  }
}

/**
 * @brief 업데이트 바이너리 파일 전송 끝에 대한 처리
 * @details 모든 패킷 받은 경우 ACK응답 후 업데이트 처리, 못 받은 경우 NACK 응답
 */
void bt_update_end() {
    // PRINTF("bt_update_end \n");
    bufferSerial.critical_transmit_section_begin(); //  통신 불가능 상태로 진입

    uint8_t _nackList[g_transmit_count] = {0};
    int _nackCount = 0;

    for (int i = 0; i < g_transmit_count; i++) {
        if(g_packet_index_check[i] != RECIVE_ACK) {
        _nackList[_nackCount++] = (i >> 8); 
        _nackList[_nackCount++] = i; 
        }
    }

    if (_nackCount != 0) {  //못받은 패킷이 있다면 PICTURE_RECIVE_NACK
        bufferSerial.critical_transmit_section_end();       //  통신 가능 상태로 전환
        bufferSerial.transmit_data(BT_UPDATE_ONGOING, (char*) _nackList, (_nackCount));
    } 
    else {    //    모든 패킷 수신 한 경우
        uint8_t _buffer[1] = {UPDATE_REQUEST_NEXT_PARTITION};
        if (!g_parition_last) {   //  남은 파일 있는 경우 return
            if (Update.write(g_update_binary_data, g_update_file_size) != g_update_file_size) {
                Update.printError(Serial);
            } 
            bufferSerial.critical_transmit_section_end();       //  통신 가능 상태로 전환
            bufferSerial.transmit_data(BT_UPDATE_END, (char*) _buffer, 1);
            return;
        } else {    //  모든 파일 Get
            PRINTF("마지막 wirte \n");
            if (Update.write(g_update_binary_data, g_update_file_size) != g_update_file_size) {
                    Update.printError(Serial);
            }
            _buffer[0] = UPDATE_REQUESET_UPDATE_SUCCESS;
            bufferSerial.transmit_data(BT_UPDATE_END, (char*) _buffer, 1);

            //matrix->stopDMAoutput();
            delay(1000);

            if (Update.end(true)) { //true to set the size to the current progress
                PRINTF("Update Success: !! \n");
                ESP.restart();
            } else {
                PRINTF("Update Final Error \n");
                _buffer[0] = UPDATE_REQUESET_UPDATE_FAIL;
                bufferSerial.transmit_data(BT_UPDATE_END, (char*) _buffer, 1);
                Update.printError(Serial);
                ESP.restart();
            }
        }
    }
}

/**
 * @brief 업데이트 취소
 * @details 할당 되었던 자원 해제
 */
void bt_update_cancel() {
    ESP.restart();
}