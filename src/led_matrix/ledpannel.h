#ifndef _LEDPANNEL_
#define _LEDPANNEL_

#include <BluetoothSerial.h>
#include "../board/config.h"

/*
 *  - 매트릭스 관련 정의 -
 *    Matrix Width Define
 *    Matrix Height Define
 *  Matrix Status Vaule Define
 */
#define RANGE_VALUE_16   16

#define WIDTH_SIZE_32    32
#define WIDTH_SIZE_64    64
#define WIDTH_SIZE_128   128
#define WIDTH_SIZE_196   196
#define WIDTH_SIZE_256   256
#define WIDTH_SIZE_512   512

#define HEIGHT_SIZE_32   32
#define HEIGHT_SIZE_64   64
#define HEIGHT_SIZE_128  128
#define HEIGHT_SIZE_256  256
#define HEIGHT_SIZE_512  512

#define MATRIX_BEGIN       1
#define MATRIX_END         0

#define BT_SEND_MAX_COUNT 3000

/*
 *  - 폰트 관련 정의 -
 *      폰트 타입 
 *  텍스트 사이 위 아래 공백 
 */
#define FONT_NONE        0  //  Case Draw Pixel, Font = Null
#define FONT_ONE_SIZE    1  //  GodoM6pt8b 폰트 1 사이즈
#define FONT_TWO_SIZE    2  //  GodoM7pt8b 폰트 2 사이즈
#define FONT_THREE_SIZE  3  //  GodoM9pt8b 폰트 3 사이즈
#define FONT_FOUR_SIZE   4  //  GodoM13pt8b 폰트 4 사이즈
#define TEXT_SPACE_SIZE  2  //   텍스트 위 아래 공백

/*
 *  - 액션 관련 정의 -
 *  액션 정보 Define
 *  액션 기준 점 Define
 */
#define NON_SHIFT 0
#define VERTICAL_SHIFT 1
#define HORIZONTAL_SHIFT 2

#define STANDARD_POINT 0
#define TOTAL_X_Y_R_G_B_BITMAP_SIZE  10240    //  64X32 (WIDTH, HEIGHT) * 3(R, G ,B)

/*
 *  - 디스플레이 페이지 관련 정의 -
 *     단일 페이지 검사 Define
 *     PAGE CATEGORI
 *     페이지 카운트
 *     액션 디폴트 시간
 */
#define NON_SINGLE_PAGE         -1
#define DISPLAY_AUTO_PAGE_CHANGE        1
#define DISPLAY_MENUAL_PAGE_CHANGE      0

//  페이지 TOTAL COUNT
#define PAGE_TOTAL_COUNT 5

#define TTS_PAGE_TOTAL_COUNT 2

//  플래시 저장 Byte Buffer index
#define DISPLAY_CMD_IDX     0
#define DISPLAY_OPTION_IDX  1
#define DISPLAY_PAGE_IDX  2

#define DEFAULT_ACTION_TIME  100

//  로고 페이지 인덱스 (MAX PAGE COUNT + 1)
#define LOGO_PAGE_IDX  PAGE_TOTAL_COUNT

// 0~255 RGB Data to 0~31 전환하기 위한 Divide 값
#define COLOR_DIVIDE_VALUE  1



/*
 *  - 기타 값 관련 정의 -
 *    Error Return Value
 *  마지막 실행 커맨드 기록 플래그
 *     기타 값 Define
 */
#define EXCEPTION_NULLCHECK_NUM  -1

#define UPDEATE_CHECK_VALUE       50         //  업데이트 체크 값 Define


/*
 *  - Color RGB Value -
 */
#define COLOR_RGB_RED_VALUE                     0x001F0000
#define COLOR_RGB_GREEN_VALUE                   0x00001F00
#define COLOR_RGB_BLUE_VALUE                    0x001F1F1F

/*
 *  - Picture 관련 정의 -
 *  TRANSMIT_COUNT 
 */
#define TRANSMIT_COUNT 400               // 블루투스 한번에 전송하는 도트 갯수
#define TRANSMIT_MAX_COUNT 2000               // 블루투스 통신 Gif, TTS 한번에 전송하는 Packet Size

#define EFFECT_BORDER_BLINK_BIT_MASK      0b00000001
#define EFFECT_COLOR_WHEEL_BIT_MASK       0b00000010
#define EFFECT_RAIN_STAR_BIT_MASK         0b00000100

#define EFFECT_RAINBOW_BACKGROUND         0b00001000
#define EFFECT_BORDER_RGB                 0b00010000
#define EFFECT_CLEAR_CRAB                 0b00100000

//  기능 - 시계 출력 Bit Mask Value
#define FUNCTION_CLOCK_BIT_MASK           0b00000001

//  옵션 - TextToSpeech, Sensor Bit Mask Value
#define OPTION_TTS_BIT_MASK              0b00000001     //  옵션 - 보이스  Bit Mask Value
#define OPTION_SENSOR_BIT_MASK           0b00000010     //  옵션 - 센서  Bit Mask Value
#define OPTION_GIF_BACKGROUND            0b00000100     //  옵션 - GIF 배경  Bit Mask Value

// 효과 테두리 두께
#define EFFECT_BORDER_NO_LINE_COUNT       0
#define EFFECT_BORDER_ONE_LINE_COUNT      1
#define EFFECT_BORDER_TWO_LINE_COUNT      2

//  DRAW PIXEL 관련 Define
#define OVER_WRITE_DRAW_FLAG              0
#define OVER_WRITE_CLEAR_FLAG             1
#define MATRIX_CLEAR_COLOR_VALUE          0 //  매트릭스 지우기 컬러 값 (검은색)

class BufferSerial; // buffer_serail.h, ledpannel.h 두 헤더 파일 서로 참조 해서 미리 클래스 타입 선언 

struct Color_rgb {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

struct Color_text_group_info
{
    Color_rgb color_rgb;    // Red, Green, Blue 3Byte Color info
    uint8_t textSize;       // Color Text Group Range
};

struct Picture_Matrix_RGB{
  uint8_t red;
  uint8_t green;
  uint8_t blue;
 };

class LedPannel {
private:
    MATRIX* matrix;
    BufferSerial* pBufferSerial;

    /* 현재 진행중인 기능 담는 boolean 변수
     *  m_matrix_function_clock : 시계 출력
     */
    bool m_matrix_function_clock = false;

    /* 현재 진행중인 효과 기능 담는 boolean 변수
     *  m_effect_border_blink : 테두리 깜빡 효과
     *  m_effect_color_wheel : Text 색상 변경 효과
     *  m_effect_border_rgb : R G B 테두리 효과
     *  m_effect_clear_crab : 꽃게 지우기 효과
     */
    bool m_effect_border_blink = false;
    bool m_effect_color_wheel = false;
    bool m_effect_border_rgb = false;
    bool m_effect_clear_crab = false;

    // Effect Vaule
    uint8_t m_effectCmd;
    
    // Effect 관련 시간 값
    unsigned short m_effect_time = 0;
    unsigned short m_effect_rain_star_time = 0;
    bool m_is_border_blink = true;
    int m_color_border = 0;
    uint8_t m_effect_color_change_value;

    /* M Option Cmd Bit Value */
    uint8_t m_optionCmd;

public:
    /* 현재 진행중인 액션 정보 담는 boolean 변수
     *  m_action_flag : action 여부
     */
    bool m_action_flag;
    bool m_gif_action_Flag;
    
    /**
     *  m_effect_rain_star : 눈꽃 비 효과
     *  m_effect_bg_rainbow : 배경 레인보우 색 효과
     */
    bool m_effect_rain_star = false;
    bool m_effect_bg_rainbow = false;

    /*  액션 관련 변수 
     *  배경 색, 액션 Cmd 값, 액션 시간 . . .
     */
    int m_srcColor;
    uint8_t m_actionCmd;
    uint16_t m_actionTime;
    /*  Blink Flag  */
    bool m_isBlink = true;
    /*  Action 좌표 값  */
    int m_xPos = 0;
    int m_yPos = 0;

    /*  SetUp Matrix    */
    void setup(void* mat, BufferSerial* bufSerial);
    /*  SetUp Rotation Matrix    */
    void setupRotation();
    /*  SetUp Font    */
    void setupFont(uint8_t sel);
    /*  Action stop    */
    void set_action_off(bool isClear = true);
    /*  Get Width      */
    uint16_t get_disp_width(void);
    /*  Get Height      */
    uint16_t get_disp_height(void);

    /*  
     * Excute -- (액션, 효과, 기능. . .)
     */
    void command_excute(void);
    /*  Action Type process      */
    void action_excute(void);
    /*  Effect Type process      */
    void effect_excute(void);
    /* Function Type process      */
    bool function_excute(void);

    /*  BT Set Text, Color Text, Pixel, Picture Func */
    void set_text(char* data);
    void set_color_text(char* data);
    void set_draw(char* data); 

    void set_picture_start(char* data); 
    void set_picture(char* data); 
    void set_picture_end(); 
    void set_picture_once_time(char* data, int len); 

    /*  action 관련 함수    */
    void action_gif();

    /*  디스플레이 플래시 저장 함수  */
    void save_text(char* data, int len);
    void save_color_text(char* data, int len);
    void save_picture(char* data); 
    void save_picture_excute();
    void resetPage();

    /*  폰트 버퍼 저장 함수 */
    void init_strArr(String text);

    /*  디스플레이 페이지 관련 함수   */
    void multiPagePrint(uint8_t pageCount);
    void get_display_page_count(char* data);

    /*  디스플레이 출력 함수   */
    void print_text(char* buffer);
    void print_color_text(char* buffer);
    void print_picture(char* buffer);
    void print_pov(char* buffer);
    void play_pov(void);

    /*  매트릭스 On & Off    */
    void set_matrix_begin(void);
    void set_matrix_end(void);

    /*  세팅 및 기타 처리 함수  */
    void set_perform_mode(char* data);
    void set_firm_update(char* data);
    void get_board_info(char* data);
    void get_page_setting_info(char* data);
    void set_init_board(char* data);
    void setStartMode(char* data);
    void saveLedLevel(char* data);
    void setLedLevel(char* data);
    void setUseMode(char* data);
    void setLogoTime(char* data);
    void setPageTime(char* data);    
    void setPageChaneMode(char* data);
    void setLogoEnable(char* data);
    void setLogoRepeat(char* data);
    void get_battery_info(char* data);
    void setBtName(char* data, int len);
    void setPageTimerSwitch(char* data);
    void setPageTimerSwitchEnable(char* data);
    void getPageTimerSwitchEnable(char* data);

    /* Set Matrix Function (Clock, 한국 기상 정보, 세계 기상 정보) */ 
    void setMatrixFunction(char* data, int len);    //  Save Check Function (통신 받아서 출력)

    /*  Matrix Rotation 64x32 or 32x64  */
    void saveMatrixRotationLandscape(char* data);
    void saveMatrixRotationPortrait(char* data);

    /*  Matrix Clear, Canvas Clear Buffer   */
    void matrixClear();
    void mainMatrixPrint();

    /*  Matrix Bg Clear & fill   */
    void set_scr_clear(char* data);
    void set_scr_color(char* data);

    /*  Text Basic Print    */
    void print_str_matrix(int fontSize, String textBoxData);    //Matrix is When Start Begin And End Finishing
    void print_str_matrix_basic(int fontSize, String textBoxData, int8_t actionCmd, uint16_t fontColor);   //No Change Matrix status
    void getDisplayPagePixel(char* data);
    
    /*  effect  */
    void set_effect(uint8_t _effect_value);
    void init_effect_flag();
    void effect_border_blink();
    void effect_color_wheel();
    void effect_rain_star();
    void printStar(uint16_t x, uint16_t y, uint16_t color);
    void clearStar(uint16_t x, uint16_t y, uint16_t color);
    void printBGRainbowEffect();
    void effect_border_rgb();
    void effect_clear_crab();

    /*  function  */
    void init_function_flag();
    void print_function_clock();

    //  사용중인 LED에 덮어쓰지 않기 위한 draw pixel 
    void drawPixelColorCheck(int16_t x, int16_t y, uint16_t color, uint8_t flag);


    void pov_play(int* inx, int width, int height, unsigned long* pre_time, int* hall_flg); 
};

/*  Text Center Align Print    */
void print_center_align_text(int length, MATRIX* matrix, bool isPrint, int scrColor);
/*  Text Top Align Print    */
void print_top_align_text(int length, MATRIX* matrix, bool is_top_page, LedPannel* ledpannel);

/*  Matrix Bg Divide Level Fill draw   */
void fillScreenEx(uint8_t red, uint8_t green, uint8_t blue, MATRIX* matrix);

/*  Color Text Center Align Print (color group)   */
void print_center_align_color_text(int length, MATRIX* matrix, bool isPrint, int scrColor, Color_text_group_info* text_info);

/* 캔버스 크기 체크 후 메모리 할당 해제 함수*/
void canvasMemoryAllocate(uint16_t _width, uint16_t _height);

/*  Get Matrix Status   */
boolean getMatrixBeginStatus(void);

/*  Get RGB Color Div Level */
float getLedDivValue(uint16_t brightLevel);

/* color 255 range to 31 and divide led bright level */
uint8_t color_div(uint8_t _primary_color);

/* 로테이션 값에 따른 좌표 변환 */
void getRotationPos(uint16_t* xPos, uint16_t * yPos, uint8_t g_rotationValue);



#endif
