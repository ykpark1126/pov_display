#ifndef _BITMAP_ACTION_
#define _BITMAP_ACTION_

#include "../board/config.h"

#define START_ALIVE                    1   // 0부터 시작하므로 - 1 해주기 위하여 Defnie

/**
 *  Bitmap IMAGE X,Y Postion
 */
#define POS_BITMAP_START        0

void init_action();

void init_x_y_pos(int _x, int _y); //    x_y 좌표 세팅

/*  action play 함수    */
void action_bitmap(uint8_t cmd, uint16_t time);
// void action_colorText(uint8_t cmd, uint16_t time);
// void action_picture(uint8_t cmd);
/*  action setting 함수    */
void set_action();


#endif



