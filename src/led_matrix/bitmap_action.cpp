#include "../led_matrix/bitmap_action.h"
#include "../board/board.h"
#include "../board/config.h"
#include "../utils/etc.h"
#include "../communication/command.h"
#include "../led_matrix/ledpannel.h"
#include "../utils/localtime.h"
#include "../../popsign.h"


//  Board Config Info 
extern Board_Config g_board_config;

extern MATRIX *matrix;
extern GFXcanvas16* canvas16;     //  RGB Bitmap Canvas
extern LedPannel ledpannel;
extern BufferSerial bufferSerial;

/*  액션 출력 단계 (공동 사용 변수) */
uint8_t g_action_step = 0;

/*  시간 기록 함수 (delayExt 인자) */
extern unsigned long pre_time_action;

//  캔버스 넓이, 높이
extern int g_canvasWidth, g_canvasHeight;  

/* 폰트 관련 변수 */
extern int16_t font_total_width;  //  텍스트 길이 총 넓이
extern int16_t font_total_height; //  텍스트 길이 총 높이
extern int lineCnt; //  텍스트 개행 줄 카운트
extern int g_textSize;
extern uint16_t g_fontColor;

/*  디스플레이 모드 관련 변수  */
extern bool nextPageToken;
extern bool categoryActionFlag;

/*  현재 로테이션 값  */
extern uint8_t g_rotationValue;
extern int g_rotation_start_pos;

/*  ColorText 구조체  */
extern Color_text_group_info* color_text_group_info;

/*  Setting 정보  */
extern float ledDivLevel;
extern int displayPageTime;

/*  시간 저장되어있는 구조체   */
extern struct tm g_timeinfo;

/**************************************************************************/
/** 
 * @fn      - init_action()
 * @brief   - 액션 자원 초기화 (Step, time)
*/
/**************************************************************************/
void init_action() {
    g_action_step = 0;
    pre_time_action = millis();
}

/**************************************************************************/
/** 
 * @fn      - init_x_y_pos()
 * @brief   - 액션 x, y 좌표 매개변수 값으로 초기화
 * @param _x 액션 시작 할 x 좌표
 * @param _y 액션 시작 할 y 좌표
*/
/**************************************************************************/
void init_x_y_pos(int _x, int _y) {
    ledpannel.m_xPos = _x;
    ledpannel.m_yPos = _y;
}


extern bool pov_en;

/**
 * @brief 상하좌우 액션
 * 
 * @param cmd - 액션 커맨드
 * @param time - 액션 이동 주기
 */
void action_bitmap(uint8_t cmd, uint16_t time)
{
  pov_en = true;
}

void set_action()
{
  Serial.printf("%s, %d\n", __FUNCTION__, __LINE__);
    ledpannel.m_action_flag = true;
    matrix->fillScreen(0);
    if (ledpannel.m_actionCmd == CMD_ACT_PAGE_CHANGE) {
      print_top_align_text(lineCnt, matrix, true, &ledpannel);
      bufferSerial.transmit_data(SET_TEXT);
      ledpannel.m_isBlink = true;
      return;
    }

    if (ledpannel.m_actionCmd == CMD_ACT_BLINK) {
      categoryActionFlag = false;
      return;
    } else categoryActionFlag = true;

    if (g_rotationValue == ROTATION_VALUE_LANDSCAPE) {
      switch (ledpannel.m_actionCmd)
      {
      case CMD_ACT_RIGHT_LEFT:
        ledpannel.m_xPos = g_board_config.matrix_width;
        break;
      case CMD_ACT_LEFT_RIGHT:
        ledpannel.m_xPos = -g_canvasWidth;
        break;
      case CMD_ACT_TOP_DOWN:
        ledpannel.m_yPos = -g_canvasHeight;
        break;
      case CMD_ACT_DOWN_TOP:
        ledpannel.m_yPos = g_board_config.matrix_height;
        break;
      }
    } else if (g_rotationValue == ROTATION_VALUE_PORTRAIT) {
      switch (ledpannel.m_actionCmd)
      {
      case CMD_ACT_RIGHT_LEFT:
        ledpannel.m_yPos = g_board_config.matrix_height;
        break;
      case CMD_ACT_LEFT_RIGHT:
        ledpannel.m_yPos = -g_canvasHeight;
        break;
      case CMD_ACT_TOP_DOWN:
        ledpannel.m_xPos = g_board_config.matrix_width;
        break;
      case CMD_ACT_DOWN_TOP:
        ledpannel.m_xPos = -g_canvasWidth;
        break;
      }
    }
}
