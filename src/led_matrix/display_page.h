#ifndef _DISPLAY_PAGE_
#define _DISPLAY_PAGE_

// 페이지 간격 시간
#define DEFALUT_PAGE_INTERVAL_TIME 100
// 페이지 유지 시간
#define DEFALUT_PAGE_TIME 5000


bool nextPagePrint(void* matrix, LedPannel* ledpannel);
bool checkNextPageCondition(int _displayPageTime, LedPannel* ledpannel);
bool checkTimerPagePrint(void* matrix, LedPannel* ledpannel);
int init_multiPage();
void free_multiPage();
void play_display(void* matrix, LedPannel* ledpannel);

#endif