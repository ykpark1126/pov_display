#include <Arduino.h>
#include <SPI.h>
#include <LittleFS.h>
#include <Preferences.h>
#include <FS.h>
#include "../../popsign.h"
#include "../led_matrix/ledpannel.h"
#include "../led_matrix/play_gif.h"
#include "../led_matrix/led_effect.h"
#include "../led_matrix/bitmap_action.h"
#include "../communication/buffer_serial.h"
#include "../communication/command.h"
#include "../utils/etc.h"
#include "../utils/localtime.h"
#include "../flash/flash.h"
#include "../board/board.h"
#include "../board/config.h"
#include <Adafruit_NeoMatrix.h>


extern Board_Config g_board_config;

/* 캔버스 관련 객체 */
GFXcanvas16* canvas16 = NULL;     //  RGB Bitmap Canvas16

/*  현재 로테이션 값  */
extern uint8_t g_rotationValue;
extern int g_rotation_start_pos;

/*  시간 기록 함수 (delayExt 인자) */
unsigned long pre_time_action = 0;
unsigned long pre_time_effect_border = 0;
unsigned long pre_time_effect_rain_star = 0;
unsigned long pre_time_effect_color_wheel = 0;

/* 폰트 관련 변수 */
extern char **strArr; //  텍스트 저장 2차원 배열 (텍스트 개행 시 row 증가) 
extern int lineCnt; //  텍스트 개행 줄 카운트
extern int16_t font_total_width;  //  텍스트 길이 총 넓이
extern int16_t font_total_height; //  텍스트 길이 총 높이
int g_canvasWidth, g_canvasHeight;  //  텍스트 길이에 따른 넓이 & 높이 사이즈 
uint16_t g_fontColor;
int g_textSize = 0;

/*  디스플레이 모드 관련 변수  */
extern bool nextPageToken;
extern char* pageBuffer[PAGE_TOTAL_COUNT];
extern size_t pageSize[PAGE_TOTAL_COUNT];
extern uint8_t pageEnableList[PAGE_TOTAL_COUNT];  // 페이지 Enable List --> 페이지 사용가능하는지 가지고 있는 버퍼
extern int8_t g_pageCount;


extern bool categoryActionFlag;
extern bool autoNextPageMode;
bool g_isSaveFlag = false;  //  값에 따라 저장 or 미리보기

/*  Picture 관련 변수  */
uint8_t* packet_index_check = NULL;
//  Picture Flash 저장하기 위한 2차원 구조체 (플래시 저장 할 동안 저장)
Picture_Matrix_RGB **g_picture_matrix = NULL;


extern Adafruit_NeoPixel pixels1;

//sram 영역의 배열로 현재 출력 이미지에 대한 정보를 담고 있다. 
// 위의 g_picture_matrix를 대체할 예정이다.
extern Picture_Matrix_RGB rgbBuf[IDE_MATRIX_WIDTH][IDE_MATRIX_HEIGHT];

int transmitCount = 0;  //  총 송신 횟수 
uint8_t g_pageNum;
bool saveFlag = false;

/*  GIF 관련 변수 */
uint8_t* gif_data = NULL;;
int g_transmit_total_count = 0;
extern int g_gif_play_x_offset, g_gif_play_y_offset; //  Gif 출력 시작 좌표
extern bool mIsGifPlay;  //  gif play status (공유 변수 Flag)



/*  Matirx 상태 반환 변수  */
bool matrixStatus = false;

/*  Setting 정보  */
extern float ledDivLevel;
extern uint8_t modeNum;
extern uint8_t startModeNum;
extern uint8_t modeCount;
extern uint8_t useModeList[PAGE_TOTAL_COUNT];
extern int logoTime;
extern int displayPageTime;
extern uint8_t logoEnable;

extern int8_t isBatteryModeInfo;
String g_text = "";

extern String bt_name;

/*  PAGE TIME SWICH INFO   */
extern bool timeSwitchIsEnable[PAGE_TOTAL_COUNT];
extern int8_t timeSwitchStartHour[PAGE_TOTAL_COUNT], timeSwitchStartMinute[PAGE_TOTAL_COUNT];
extern int8_t timeSwitchFromHour[PAGE_TOTAL_COUNT], timeSwitchFromMinute[PAGE_TOTAL_COUNT];
extern bool isTimeSynch;

/*  시간 저장되어있는 구조체   */
extern struct tm g_timeinfo;

/* Preferences Flash */
extern Preferences prefs;  
extern uint8_t prefsBoardInfoArray[BOARD_INFO_PREFS_IDX_TOTAL_COUNT];

/*  effect star */
LedEffect* ledEffect = NULL;

/*  ColorText 구조체  */
Color_text_group_info* color_text_group_info = NULL;


/* BG Rainbow Effect 관련 Value 변수  */
const float radius1 =65.2, radius2 =92.0, radius3 =163.2, radius4 =176.8,
        centerx1=64.4, centerx2=46.4, centerx3= 93.6, centerx4= 16.4,
        centery1=34.8, centery2=26.0, centery3= 56.0, centery4=-11.6;
float       angle1  = 0.0, angle2  = 0.0, angle3  =  0.0, angle4  =  0.0;
long        hueShift= 0;

uint32_t prevTime = 0; // For frame-to-frame interval timing

// Input a value 0 to 24 to get a color value.
// The colours are a transition r - g - b - back to r.
/* Effect Border Rgb 관련 Value 변수  */
int borderAddPos = 0;

/*  Server FireBase   */
extern String serverRequestUrl;

/*  마지막 시간 저장 정적 변수   */
static int last_min = -1;


uint16_t Wheel(MATRIX* matrix, byte WheelPos) {
  if(WheelPos < 8) {
   return matrix->Color((uint8_t)ceil(((float)150 - WheelPos * 2* ledDivLevel)), (uint8_t)ceil(((float)WheelPos * 2* ledDivLevel)), (uint8_t)ceil(((float)0* ledDivLevel)));
  } else if(WheelPos < 16) {
   WheelPos -= 8;
   return matrix->Color((uint8_t)ceil(((float)0* ledDivLevel)), (uint8_t)ceil(((float)150 - WheelPos*2* ledDivLevel)), (uint8_t)ceil(((float)WheelPos * 2* ledDivLevel)));
  } else {
   WheelPos -= 16;
   return matrix->Color((uint8_t)ceil(((float)0* ledDivLevel)), (uint8_t)ceil(((float)150 - WheelPos*2* ledDivLevel)), (uint8_t)ceil(((float)7 - WheelPos * 2* ledDivLevel)));
  }
}

void set_screen_color(MATRIX* matrix, uint8_t r, uint8_t g, uint8_t b)
{
  matrix->fillScreen(matrix->Color(color_div(r), color_div(g), color_div(b)));
}

void set_text_color(MATRIX* matrix, uint8_t r, uint8_t g, uint8_t b)
{
  matrix->setTextColor(matrix->Color(color_div(r), color_div(g), color_div(b)));
}

void fillScreenEx(uint8_t red, uint8_t green, uint8_t blue, MATRIX* matrix) 
{
  matrix->fillScreen(matrix->Color(color_div(red), color_div(green), color_div(blue)));
  canvas16->fillScreen(0);
}

void set_draw_pixel(MATRIX* matrix, int16_t x, int16_t y, uint8_t r, uint8_t g, uint8_t b)
{
  canvas16->drawPixel(x, y, matrix->Color(color_div(r), color_div(g), color_div(b)));  //캔버스 버퍼에 저장
}

void set_draw_pixel_print(MATRIX* matrix, uint16_t x, uint16_t y, uint8_t r, uint8_t g, uint8_t b)
{
  getRotationPos(&x, &y, g_rotationValue);
  matrix->drawPixel(x, y, matrix->Color((uint8_t)ceil(((float)r* ledDivLevel)), (uint8_t)ceil(((float)g* ledDivLevel)), (uint8_t)ceil(((float)b* ledDivLevel)))); //매트릭스로 바로 출력
  matrix->show();
  // canvas16->drawPixel(x, y, matrix->Color((uint8_t)ceil(((float)r* ledDivLevel)), (uint8_t)ceil(((float)g* ledDivLevel)), (uint8_t)ceil(((float)b* ledDivLevel))));  //캔버스 버퍼에 저장 후 출력
  // matrix->drawRGBBitmap(0 , 0, canvas16->getBuffer(), g_board_config.matrix_width, g_board_config.matrix_height);
}

/**
 * @brief ~255 range color value ~31 전환 후 ledDivLevel으로 나누는 함수
 * 
 * @param _primary_color RGB Color 중 하나의 원색
 * @return uint8_t - 변경된 원색 값 
 */
uint8_t color_div(uint8_t _primary_color) { return (uint8_t)ceil((float) (_primary_color * ledDivLevel) / COLOR_DIVIDE_VALUE);}

/**
 * @brief Get the Rotation Pos object - 로테이션 값에 따른 좌표 변환
 * 
 * @param xPos 변환할 X 좌표 포인터
 * @param yPos 변환할 Y 좌표 포인터
 * @param rotationValue 로테이션 값
 */
void getRotationPos(uint16_t* xPos, uint16_t * yPos, uint8_t rotationValue) {

  int16_t t;

  switch(rotationValue) {
      case 1:
          t = *xPos;
          *xPos = g_board_config.matrix_width  - 1 - *yPos;
          *yPos = t;
          break;
      case 2:
          *xPos = g_board_config.matrix_width  - 1 - *xPos;
          *yPos = g_board_config.matrix_height - 1 - *yPos;
          break;
      case 3:
          t = *xPos;
          *xPos = *yPos;
          *yPos = g_board_config.matrix_height - 1 - t;
          break;
  }
}

void LedPannel::setup(void* mat, BufferSerial* bufSerial)
{
    g_canvasWidth = g_board_config.matrix_width;
    g_canvasHeight = g_board_config.matrix_height;

    this->matrix = (Adafruit_NeoMatrix*)mat;
    
    this->pBufferSerial = bufSerial;
    canvas16 = new GFXcanvas16(g_canvasWidth, g_canvasHeight);
}

void LedPannel::setupFont(uint8_t sel)
 {
  
 }
void LedPannel::setupRotation()
{
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) {
    g_rotation_start_pos = (g_board_config.matrix_width - g_canvasWidth);
    canvas16->setRotation(g_rotationValue);
    matrix->canvas_rotation = g_rotationValue;
  }
  ledEffect = new LedEffect(matrix);
}

uint16_t LedPannel::get_disp_width(void)
{
    return 64;
}

uint16_t LedPannel::get_disp_height(void)
{
    return 32;
}

void LedPannel::command_excute(void)
{
  // if(function_excute())  return;  //  function = 액션, 효과 동시 사용 하지 못하게 설정
  action_excute();
  effect_excute();
}

// int inx = 0;
// int pov_height = g_board_config.matrix_height;
// int pov_width = g_board_config.matrix_width;
// int row_add = inx * g_board_config.matrix_height * 3;	

// void LedPannel::pov_play(void)
// {
//   if (delayExt(millis(), &pre_time, 1)) { 
//   for (int16_t j = 0; j < pov_height; j++) {
//     pixels.setPixelColor(j, rgbBuf[inx][j].red, rgbBuf[inx][j].green, rgbBuf[inx][j].blue);
//   }
//   pixels.show();
//   //delay(20);
//   inx = inx + 1;	
//   if (inx >= pov_width)
//     inx = 0;
//   } else return;
//}


void LedPannel::pov_play(int* inx, int width, int height, unsigned long* pre_time, int* hall_flg)
{
  int led_inx = *inx;
  if (delayExt(millis(), pre_time, 1)) { 
    for (int16_t j = 0; j < height; j++) {
      pixels1.setPixelColor(j, rgbBuf[led_inx][j].red, rgbBuf[led_inx][j].green, rgbBuf[led_inx][j].blue);
    }
    pixels1.show();
    //delay(20);
    *inx = *inx + 1;	
    if (*inx >= width) // 화면 출력 완료 후 Inxdex 초기화
    {
      *inx = 0;
      *hall_flg = HIGH;
    }
  } else return;
}

void LedPannel::action_excute(void)
{
  if (m_action_flag == true) 
    action_bitmap(m_actionCmd, m_actionTime);
  
}

void LedPannel::effect_excute(void)
{
  if (m_effect_border_blink)  effect_border_blink();
  if (m_effect_color_wheel) effect_color_wheel();
  if (m_effect_rain_star) effect_rain_star();

  if (m_effect_bg_rainbow)  printBGRainbowEffect();
  if (m_effect_border_rgb) effect_border_rgb();
  if (m_effect_clear_crab) effect_clear_crab();
}

/**  function_excute
 * return : true --> 아래 excute 실행 안함. false --> 아래 excute 실행
 *         ex) 시계 : 하나의 기능만 사용가능하여 true 반환, TTS 
 */
bool LedPannel::function_excute(void)
{
  if (m_matrix_function_clock) {
    print_function_clock();
    return true;
  }  
  return false;
}

void LedPannel::set_action_off(bool isClear)
{
  m_action_flag = false;
  m_gif_action_Flag = false;

  init_effect_flag();
  init_function_flag();
  init_action();
  // if (g_board_config.model_info == VOICE_MODEL)
  //   audioTimerStop();

  m_xPos = 0, m_yPos = 0;
  while (mIsGifPlay) {  //  gif 출력 중인 경우 끝날떄까지 Wait
    delay(100);
  }

  if (isClear) {
    matrix->clear();
    matrix->show();
  }
}

void LedPannel::print_str_matrix(int fontSize, String textBoxData)
{
  int colorRgbFont;
  int colorRgbScr;
  uint8_t colorFont[3]; 
  uint8_t colorScr[3];
  
  memset(colorFont, 0, sizeof(colorFont));
  memset(colorScr, 0, sizeof(colorScr));

  colorFont[0] = 0;
  colorFont[1] = 150;
  colorFont[2] = 0;

  colorScr[0] = 0;
  colorScr[1] = 0;
  colorScr[2] = 0;

  //g_fontColor = matrix->Color((uint8_t)ceil(((float)colorFont[0]* ledDivLevel)), (uint8_t)ceil(((float)colorFont[1]* ledDivLevel)), (uint8_t)ceil(((float)colorFont[2]* ledDivLevel)));
  //colorRgbScr = matrix->Color((uint8_t)ceil(((float)colorScr[0]* ledDivLevel)), (uint8_t)ceil(((float)colorScr[1]* ledDivLevel)), (uint8_t)ceil(((float)colorScr[2]* ledDivLevel)));

  g_fontColor = matrix->Color(1, 2, 3);

  g_textSize = fontSize;
  set_action_off(); /* scrolling text off */
  setupFont(g_textSize);
  init_strArr(textBoxData);
  print_center_align_text(lineCnt, matrix, true, colorRgbScr);
  categoryActionFlag = false;
  delay(2000);
  set_screen_color(matrix, 0, 0, 0); 
}

void LedPannel::print_str_matrix_basic(int fontSize, String textBoxData, int8_t actionCmd, uint16_t fontColor)
{
  int colorRgbScr;
  int actionTime = 50;
  uint8_t colorFont[3]; 
  uint8_t colorScr[3];

  memset(colorFont, 0, sizeof(colorFont));
  memset(colorScr, 0, sizeof(colorScr));

  // colorFont[0] = 15;
  // colorFont[1] = 0;
  // colorFont[2] = 0;

  colorScr[0] = 0;
  colorScr[1] = 0;
  colorScr[2] = 0;

  // colorRgbFont = (int)(colorFont[0] << 16 | colorFont[1] << 8 | colorFont[2]);
  colorRgbScr = (int)(colorScr[0] << 16 | colorScr[1] << 8 | colorScr[2]);

  g_fontColor = fontColor;
  g_textSize = fontSize;
  set_action_off(); /* scrolling text off */
  setupFont(g_textSize);
  init_strArr(textBoxData);
  m_actionCmd = actionCmd;
  m_actionTime = DEFAULT_ACTION_TIME;

  if (m_actionCmd == CMD_ACT_DEFALUT) {
    print_center_align_text(lineCnt, matrix, true, colorRgbScr);
    categoryActionFlag = false;
  }
  else {
    print_center_align_text(lineCnt, matrix, false, colorRgbScr);
    set_action();        
  }
}

/* 
 *  Command : Set Text One - 컬러 텍스트 가운데 정렬 출력 함수  
 *  Packet Protrocol : PageNum (1Byte), Screen Color(3Bytes), Font Size(1Byte), Text Group Count(1Byte), String(N Bytes), Font Color(3Byte * Group Count)
 */
/*  Color Text Center Align Print (color group)   */

void print_center_align_color_text(int totalLineCnt, MATRIX* matrix, bool isPrint, int scrColor, Color_text_group_info* color_text_group_info)
{
  String str;
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  int16_t pointX = 0, pointY = 0;
  int curWidth = 0, curHeight = 0;
  int privousTextHeight;
  int total_index_count = 0;
  int index_group = 0;
  int16_t _w_size = g_board_config.matrix_width, _y_size = g_board_config.matrix_height;
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) { //  로테이션 값이 세로모드라면 M,H 값 치환
    _w_size = g_board_config.matrix_height;
    _y_size = g_board_config.matrix_width;
  }

  /*  Font Color -> Rgb / Div Level  */

  font_total_height = font_total_height + ((lineCnt - 1) * TEXT_SPACE_SIZE);
  getMatrixSizeRange(font_total_width, font_total_height, &curWidth, &curHeight, g_rotationValue);
  canvasMemoryAllocate(curWidth, curHeight);  //  캔버스 크기 할당


  canvas16->setRotation(g_rotationValue); 
  matrix->canvas_rotation = g_rotationValue;
  canvas16->fillScreen(scrColor);

  for(int textLineCnt = 0; textLineCnt < totalLineCnt; textLineCnt++){
    str = strArr[textLineCnt]; 
    int16_t x1, y1;
    uint16_t w1, h1; 

    matrix->getTextBounds(str , 0, 0, &x1, &y1, &w1, &h1);
    
    /* 
     *  Center Alignment Width, Height 
     */

    /* Width Center Alignment */
    if (w1 >= _w_size ) { // 문자열의 넓이가 g_curRotationWidth보다 크다면 좌측부터 출력
      pointX = 0; 
    }  
    else pointX = ((_w_size - w1 ) / 2) - 1; //  줄 마다 가운데 정렬

    /* Height Center Alignment */
    if (textLineCnt == 0) {
        if ((_y_size - font_total_height) / 2 > 0) {  //  가운데 정렬 했을 때 처음 문자가 매트릭스에 표시된다면. 
          pointY = ((_y_size - font_total_height) / 2); 
        }
    } else pointY += privousTextHeight + g_textSize; // 두 번째 라인의 문자는 첫 번째 줄의 문자 + 문자 높이 + 공백
    
    privousTextHeight = h1;
    canvas16->setCursor(pointX, pointY);    
    
    for(int index = 0; index < strlen(strArr[textLineCnt]); index++){
      if (total_index_count >= color_text_group_info[index_group].textSize) {
        index_group++;
      }
      canvas16->colorPrint(strArr[textLineCnt][index], matrix->Color((uint8_t)ceil(((float)color_text_group_info[index_group].color_rgb.red* ledDivLevel)), (uint8_t)ceil(((float)color_text_group_info[index_group].color_rgb.green* ledDivLevel)), (uint8_t)ceil(((float)color_text_group_info[index_group].color_rgb.blue* ledDivLevel))));
      total_index_count++;
    }  
    total_index_count++;  // 개행 문자에서도 인덱스 그룹 증가
  }
  
  if (isPrint == true) {
    if (g_rotationValue == ROTATION_VALUE_LANDSCAPE) matrix->drawRGBBitmap(0 , 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
    else if (g_rotationValue == ROTATION_VALUE_PORTRAIT)  matrix->drawRGBBitmap(g_board_config.matrix_width - g_canvasWidth, 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
  }
  matrix->show();
}

/* 
 *  godo6pt 폰트에 대한 가운데 정렬 텍스트 출력 함수   
 */

void print_center_align_text(int totalLineCnt, MATRIX* matrix, bool isPrint, int scrColor)
{
  String str;
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  int16_t pointX = 0, pointY = 0;
  int curWidth = 0, curHeight = 0;
  int privousTextHeight;
  int16_t _w_size = g_board_config.matrix_width, _y_size = g_board_config.matrix_height;
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) { //  로테이션 값이 세로모드라면 M,H 값 치환
    _w_size = g_board_config.matrix_height;
    _y_size = g_board_config.matrix_width;
  }
  // PRINTF("font_total_height : %d \n ", font_total_height);
  // PRINTF("pre font_total_width : %d \n ", font_total_width);

  font_total_height = font_total_height + ((lineCnt - 1) * TEXT_SPACE_SIZE);
  getMatrixSizeRange(font_total_width, font_total_height, &curWidth, &curHeight, g_rotationValue);
  canvasMemoryAllocate(curWidth, curHeight);
  matrix->canvas_rotation = g_rotationValue;
  canvas16->fillScreen(scrColor);

  for(int textLineCnt = 0; textLineCnt < totalLineCnt; textLineCnt++){
    str = strArr[textLineCnt]; 

    int16_t x1, y1;
    uint16_t w1, h1; 
    matrix->getTextBounds(str , 0, 0, &x1, &y1, &w1, &h1);
    /* 
     *  Center Alignment Width, Height 
     */

    /* Width Center Alignment */

    if (w1 >= _w_size ) { // 문자열의 넓이가 g_curRotationWidth보다 크다면 좌측부터 출력
      pointX = 0; 
    }  
    else pointX = ((_w_size - w1 ) / 2) - 1; //  줄 마다 가운데 정렬

    /* Height Center Alignment */
    if (textLineCnt == 0) {
        if ((_y_size - font_total_height) / 2 > 0) {  //  가운데 정렬 했을 때 처음 문자가 매트릭스에 표시된다면. 
          pointY = ((_y_size - font_total_height) / 2); 
        }
    } else pointY += privousTextHeight + g_textSize; // 두 번째 라인의 문자는 첫 번째 줄의 문자 + 문자 높이 + 공백
    
    privousTextHeight = h1;
    // PRINTF("font_total_height : %d \n", font_total_height);
    // PRINTF("%d 번째 라인의 w1 : %d, y1 = %d \n", textLineCnt, w1, h1);
    // PRINTF("%d 번째 라인의 point x : %d, point y = %d \n", textLineCnt, pointX ,pointY);
    canvas16->setCursor(pointX, pointY);
    canvas16->colorPrint(str, g_fontColor);
  }
  if (isPrint == true) { 
    if (g_rotationValue == ROTATION_VALUE_LANDSCAPE)  matrix->drawRGBBitmap(0, 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
    else if (g_rotationValue == ROTATION_VALUE_PORTRAIT) matrix->drawRGBBitmap(g_board_config.matrix_width - g_canvasWidth , 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
  }
  matrix->show();
}

/* 
 *  godo6pt 폰트에 대한 상단 정렬 텍스트 출력 함수   
 */

void print_top_align_text(int totalLineCnt, MATRIX* matrix, bool is_top_page, LedPannel* ledpannel)
{
  String str;

  int16_t pointX = 0, pointY = 0;
  int curWidth = 0, curHeight = 0;
  int privousTextHeight;
  // PRINTF("font_total_height : %d \n ", font_total_height);
  // PRINTF("pre font_total_width : %d \n ", font_total_width);
  int textLineCnt = 0;
  int printLineCnt = 0;
  int16_t _w_size = g_board_config.matrix_width, _y_size = g_board_config.matrix_height;
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) { //  로테이션 값이 세로모드라면 M,H 값 치환
    _w_size = g_board_config.matrix_height;
    _y_size = g_board_config.matrix_width;
  }

  printLineCnt = totalLineCnt;

  font_total_height = font_total_height + ((lineCnt - 1) * TEXT_SPACE_SIZE);
  getMatrixSizeRange(font_total_width, font_total_height, &curWidth, &curHeight, g_rotationValue);
  canvasMemoryAllocate(curWidth, curHeight);  //  캔버스 크기 할당

  /** 세로모드 페이지 변환 숫자 갯수
   *  폰트크기 1 : [6], [13] (Case-패널 카운트1, 2)
   *  폰트크기 2 : [5], [11] (Case-패널 카운트1, 2)
   */    

  int page_change_number_count = 0;

  
  canvas16->fillScreen(0);
  canvas16->setTextWrap(false); // Don't wrap at end of line - will do ourselves
  canvas16->setAttribute(UTF8_ENABLE , true);
  canvas16->setRotation(g_rotationValue); 
  matrix->canvas_rotation = g_rotationValue;

  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) { //  세로 모드 일경우 한 화면에 표시 안되는지 검사
    if (totalLineCnt >= page_change_number_count) {
      categoryActionFlag = true;
      ledpannel->m_action_flag = true;
    }
    else {
      categoryActionFlag = false;
      ledpannel->m_action_flag = false;
    }
    if (is_top_page) {
      textLineCnt = 0; 
      if (totalLineCnt >= page_change_number_count) printLineCnt = page_change_number_count - 1;
    } else {
      textLineCnt = page_change_number_count - 1;
    }
  }
  /** 가로모드 페이지 변환 스트링 길이 : 
   *  폰트크기 1 : [11] (한 화면에 표시되는 스트링 길이)
   *  폰트크기 2 : [9] (한 화면에 표시되는 스트링 길이)
   */
  page_change_number_count = 0;
  for(int cnt = 0; textLineCnt < printLineCnt; textLineCnt++) {
    String tempStr;
    tempStr = strArr[textLineCnt];
    int tempFromIndex = 0;
    if (g_rotationValue == ROTATION_VALUE_LANDSCAPE) {  //  가로 모드 일경우 한 화면에 표시 안되는지 검사
      int startIndex = 0, fromIndex = -1;

      while ((fromIndex = tempStr.indexOf(" ", fromIndex + 1)) >= 0) {  // get textOfLineCount
        if (tempFromIndex < fromIndex) tempFromIndex = fromIndex;
        if (g_textSize == FONT_ONE_SIZE) {
          if (fromIndex >= 11 * (g_board_config.matrix_width / 64)) {
            categoryActionFlag = true;
            ledpannel->m_action_flag = true;
            if (is_top_page) break;
            else startIndex = fromIndex;
          }
        } 
        else if (g_textSize == FONT_TWO_SIZE) {
          if (fromIndex >= 9 * (g_board_config.matrix_width / 64)) {
            categoryActionFlag = true;
            ledpannel->m_action_flag = true;
            if (is_top_page) break;
            else startIndex = fromIndex;
          }
        }
      }
      str = tempStr.substring(startIndex, fromIndex);
    } else  str = strArr[textLineCnt];

    int16_t x1, y1;
    uint16_t w1, h1; 

    matrix->getTextBounds(str , 0, 0, &x1, &y1, &w1, &h1);
    /* 
     *  Center Alignment Width, Height 
     */

    /* Width Center Alignment */

    if (w1 >= _w_size || g_rotationValue == ROTATION_VALUE_LANDSCAPE) { // 문자열의 넓이가 g_curRotationWidth보다 크다면 좌측부터 출력
      pointX = 1; 
    }  
    else pointX = ((_w_size - w1 ) / 2); //  줄 마다 가운데 정렬

    /* Height Center Alignment */
    if (cnt == 0) {
      pointY = 1;
      cnt++;
      if (g_rotationValue == ROTATION_VALUE_LANDSCAPE) {
        if ((_y_size - font_total_height) / 2 > 0) {  //  가운데 정렬 했을 때 처음 문자가 매트릭스에 표시된다면. 
          pointY = ((_y_size - font_total_height) / 2); 
        }
      }
    } else pointY += privousTextHeight + g_textSize; // 두 번째 라인의 문자는 첫 번째 줄의 문자 + 문자 높이 + 공백
    
    privousTextHeight = h1;
    // PRINTF("font_total_height : %d \n", font_total_height);
    // PRINTF("%d 번째 라인의 w1 : %d, y1 = %d \n", textLineCnt, w1, h1);
    // PRINTF("%d 번째 라인의 point x : %d, point y = %d \n", textLineCnt, pointX ,pointY);

    canvas16->setCursor(pointX, pointY);
    canvas16->colorPrint(str, g_fontColor);
    matrix->setCursor(pointX, pointY);    // very
    matrix->setTextColor(g_fontColor);
    matrix->print(str);

  }
  if (g_rotationValue == ROTATION_VALUE_LANDSCAPE)  matrix->drawRGBBitmap(0, 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
  else if (g_rotationValue == ROTATION_VALUE_PORTRAIT) matrix->drawRGBBitmap(g_board_config.matrix_width - g_canvasWidth , 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
}

void LedPannel::init_strArr(String text){
  if(strArr != NULL){
    for (int i = 0; i < lineCnt; i++) free(strArr[i]);
    free(strArr);
    strArr = NULL;
  }
  uint8_t textLen;
  textLen = text.length();
  matrix->setTextWrap(false); // Don't wrap at end of line - will do ourselves

  String str = "";
  g_text = text;
  int startIndex = 0, fromIndex = -1;
  lineCnt = 1;
  font_total_width = 0;
  font_total_height = 0;
  int16_t x1, y1;
  uint16_t w1, h1; 
  
  while ((fromIndex = text.indexOf("\n", fromIndex + 1)) >= 0){  // get textOfLineCount
        lineCnt++;
  }
  strArr = (char **)malloc(sizeof(char *) * lineCnt);

  for (int i = 0; i < lineCnt; i++) { // Create Dynamic Array by Input Text
    int strNullLen, strlen;
    if ((fromIndex = text.indexOf("\n", fromIndex + 1)) >= 0) { // isNextline Ok
      str = text.substring(startIndex, fromIndex);
      strNullLen = (int)fromIndex - startIndex + 1;
      strlen = strNullLen -1;
      
      matrix->getTextBounds(str , 0, 0, &x1, &y1, &w1, &h1);  
      if (fromIndex == 0) {
        h1 = 10;
        w1 = 0;
      }
      if(font_total_width < w1) font_total_width = w1 + 2;
      font_total_height += h1;
      strArr[i] = (char *)malloc(sizeof(char) * (strNullLen));
      strcpy(strArr[i], str.c_str() );
      strArr[i][strlen] = 0;
      startIndex = fromIndex + 1; 
    } 
    else { // isNextline No
      str = text.substring(startIndex);
      strNullLen = (int)str.length() + 1;
      strlen = strNullLen - 1;

      matrix->getTextBounds(str , 0, 0, &x1, &y1, &w1, &h1); 
      if(font_total_width < w1) font_total_width = w1 + 2;
      font_total_height += h1; 
      strArr[i] = (char *)malloc(sizeof(char) * (strNullLen));
      strcpy(strArr[i], str.c_str());
      strArr[i][strlen] = 0;
    }
  }
}

/**
 * @brief 디스플레이 모드 Page Flash Data 페이지 Set
 * 
 * @param pageCount - 출력 할 페이지
 */
void LedPannel::multiPagePrint(uint8_t pageCount)
{
  // PRINTF("multiPagePrint \n");
  File pageFile;    //  페이지 파일
  char pageStr[30];

  /*  Page Data Flash to psram Road Section  */
  if (pageEnableList[pageCount] == FLASH_PAGE_ENABLE_VALUE) { //  페이지 있는 경우 Data Road
    sprintf(pageStr, "/page%d.txt", pageCount + 1); //현재 0페이지로 들어오게 되어있어서 0 + 1 연산
    pageFile = LittleFS.open(pageStr);
    pageSize[pageCount] = pageFile.size();

    //  Page Flash Byte Read
    if(pageSize[pageCount]){ 

      if (pageBuffer[pageCount] != NULL) { 
        free(pageBuffer[pageCount]);
        pageBuffer[pageCount] = NULL;
      }

      pageBuffer[pageCount] = (char*)malloc(pageSize[pageCount]);
      readFile(LittleFS, pageStr, (uint8_t*) pageBuffer[pageCount]); 
    }
    pageFile.close();
  } else {  //  페이지 없는 경우 Return
      pageSize[pageCount] = 0;
      return;
  }
  Serial.printf("%s %d\n", __FUNCTION__, __LINE__);
  /*  Page Data Draw Section  */
  uint8_t cmd;
  nextPageToken = false;
  cmd = pageBuffer[pageCount][DISPLAY_CMD_IDX]; // DISPLAY_CMD_IDX = Cmd Index
  uint8_t _option = pageBuffer[pageCount][DISPLAY_OPTION_IDX];
  // PRINTF("cmd : %x \n", cmd);

  switch (cmd)
  {
    case SET_TEXT:   // 1번지 부터 시작 == CMD 제외한 값
      set_text(&pageBuffer[pageCount][1]); 
      Serial.printf("%s %d\n", __FUNCTION__, __LINE__);
      break;
    case SET_COLOR_TEXT:
      set_color_text(&pageBuffer[pageCount][1]);
      Serial.printf("%s %d\n", __FUNCTION__, __LINE__);
      break;
    case SET_PICTURE:
      Serial.printf("%s %d\n", __FUNCTION__, __LINE__);
      print_pov(&pageBuffer[pageCount][1]); 
      break;
    case SET_MATRIX_FUNCTION:
      setMatrixFunction(&pageBuffer[pageCount][1], 0); //  매개변수 형식 맞추기 위해 NULL 전달 (페이지 출력 시 두번 째 매개변수 필요하지 않음.)
      break;

  }
}

/**
 * @brief 페이지 카운트 Get 
 * 
 * @param data 
 */
void LedPannel::get_display_page_count(char* data)
{
  PRINTF("get_display_page_count \n");

  /*  페이지 정보 가져오기 부분 */
  File file;
  int pageTotalCount = 0;
  char pageStr[15];

  //  페이지 Enable List에서 데이터 있는 페이지 내용 검사
  for(int i = 0; i < PAGE_TOTAL_COUNT; i++){
    if (pageEnableList[i]){
      pageTotalCount++;
    }
  }
  if (pageTotalCount == 0) {
    pBufferSerial->transmit_data(GET_DISPLAY_PAGE_COUNT);
    return;
  } 

  //  페이지 Enable List의 데이터 존재하는 리스트만 전송
  uint8_t sendBuffer[pageTotalCount] = {0};
  int sendBufIdx = 0;
  for(int i = 0; i < PAGE_TOTAL_COUNT; i++){
    if (pageEnableList[i]) {
      PRINTF("sendBufIdx[%d] : %d \n", sendBufIdx, i);
      sendBuffer[sendBufIdx++] = i;
    } 
  }
  pBufferSerial->transmit_data(GET_DISPLAY_PAGE_COUNT, (char*) sendBuffer, pageTotalCount);
}

void LedPannel::print_picture(char* buffer) 
{
  set_action_off(); /* scrolling text off */

  uint8_t pageNum;
  uint8_t fontSize;
  int bufferIndex = 0;
  uint16_t _picture_width = 0, _picture_height = 0; //  App 에서 보내는 picture width, height 사이즈

  m_optionCmd = buffer[bufferIndex++];
  pageNum = buffer[bufferIndex++];
  m_actionCmd = buffer[bufferIndex++];
  m_actionTime = 0;
  m_actionTime = m_actionTime | buffer[bufferIndex++] << 8;
  m_actionTime = m_actionTime | buffer[bufferIndex++];

  m_effectCmd = buffer[bufferIndex++];
  m_effect_time = m_effect_time | buffer[bufferIndex++] << 8;
  m_effect_time = m_effect_time | buffer[bufferIndex++];
  m_color_border = (int)(buffer[bufferIndex++] << 16 | buffer[bufferIndex++] << 8 | buffer[bufferIndex++]);
  m_effect_color_change_value = (buffer[bufferIndex++] * 8);
  m_effect_rain_star_time = m_effect_rain_star_time | buffer[bufferIndex++] << 8;
  m_effect_rain_star_time = m_effect_rain_star_time | buffer[bufferIndex++];
  _picture_width = _picture_width | buffer[bufferIndex++] << 8;
  _picture_width = _picture_width | buffer[bufferIndex++];
  _picture_height = _picture_height | buffer[bufferIndex++] << 8; 
  _picture_height = _picture_height | buffer[bufferIndex++];

  canvasMemoryAllocate(_picture_width, _picture_height);

  int _rotation_w =  g_canvasWidth, _rotation_h = g_canvasHeight; //  로테이션 된 상태의 width, height
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) {
    _rotation_w = g_canvasHeight;
    _rotation_h = g_canvasWidth;
  }

  for (int _x = 0; _x < _rotation_w; _x++) {
    for (int _y = 0; _y < _rotation_h; _y++) {
      set_draw_pixel(matrix, _x, _y, buffer[bufferIndex++], buffer[bufferIndex++], buffer[bufferIndex++]);
    }
  }

  if (m_actionCmd == CMD_ACT_DEFALUT)
  {
    categoryActionFlag = false;
    matrix->drawRGBBitmap(0 , 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
    matrix->show();

  } else {
    set_action();
  } 
  set_effect(m_effectCmd);
}


void LedPannel::print_pov(char* buffer) 
{
  set_action_off(); /* scrolling text off */

  uint8_t pageNum;
  uint8_t fontSize;
  int bufferIndex = 0;
  uint16_t _picture_width = 0, _picture_height = 0; //  App 에서 보내는 picture width, height 사이즈

  m_optionCmd = buffer[bufferIndex++];
  pageNum = buffer[bufferIndex++];
  m_actionCmd = buffer[bufferIndex++];
  m_actionTime = 0;
  m_actionTime = m_actionTime | buffer[bufferIndex++] << 8;
  m_actionTime = m_actionTime | buffer[bufferIndex++];

  m_effectCmd = buffer[bufferIndex++];
  m_effect_time = m_effect_time | buffer[bufferIndex++] << 8;
  m_effect_time = m_effect_time | buffer[bufferIndex++];
  m_color_border = (int)(buffer[bufferIndex++] << 16 | buffer[bufferIndex++] << 8 | buffer[bufferIndex++]);
  m_effect_color_change_value = (buffer[bufferIndex++] * 8);
  m_effect_rain_star_time = m_effect_rain_star_time | buffer[bufferIndex++] << 8;
  m_effect_rain_star_time = m_effect_rain_star_time | buffer[bufferIndex++];
  _picture_width = _picture_width | buffer[bufferIndex++] << 8;
  _picture_width = _picture_width | buffer[bufferIndex++];
  _picture_height = _picture_height | buffer[bufferIndex++] << 8; 
  _picture_height = _picture_height | buffer[bufferIndex++];

  canvasMemoryAllocate(_picture_width, _picture_height);

  int _rotation_w =  g_canvasWidth, _rotation_h = g_canvasHeight; //  로테이션 된 상태의 width, height
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) {
    _rotation_w = g_canvasHeight;
    _rotation_h = g_canvasWidth;
  }

  // buffer데이터를 rgb buffer에 넣는 구조로 변경 필요
  for(int x=0; x < _rotation_w; x++)
  {  
    for(int y=0; y < _rotation_h; y++){
      rgbBuf[x][y].red = buffer[bufferIndex++];
      rgbBuf[x][y].green = buffer[bufferIndex++];
      rgbBuf[x][y].blue = buffer[bufferIndex++];
    }
  }
}

boolean getMatrixBeginStatus(void)
{
  return matrixStatus;
}

void LedPannel::set_matrix_begin(void)
{
  // if (getMatrixBeginStatus() == false) {
    matrixStatus = true;
//    delay(1000);
  // }
}

void LedPannel::set_matrix_end(void)
{
  // if (getMatrixBeginStatus() == true) {
    set_screen_color(matrix, 0, 0, 0);  
    matrixStatus = false;
    // delay(150);
  // }
}

void LedPannel::set_draw(char* data)
{
  int inx = 0;
  uint8_t pageNum = 0;
  byte pointX;
  byte pointY;
  byte colorDot[3];
  uint16_t dotLen = 0;
  char tempData[5];

  pageNum = data[inx++];
  dotLen = dotLen | (data[inx++] << 8);
  dotLen =  dotLen | data[inx++];

  // m_action_flag = false;
  // m_gif_action_Flag = false;

  // init_effect_flag();
  // init_function_flag();
  // init_action();
  // // if (g_board_config.model_info == VOICE_MODEL)
  // //   audioTimerStop();

  // m_xPos = 0, m_yPos = 0;
  // while (mIsGifPlay) {  //  gif 출력 중인 경우 끝날떄까지 Wait
  //   delay(100);
  // }

  for(int i=0; i<dotLen; i++)
  {
    pointX = data[inx++];
    pointY = data[inx++];
    colorDot[0] = data[inx++];
    colorDot[1] = data[inx++];
    colorDot[2] = data[inx++];
    
    set_draw_pixel(matrix, pointX, pointY, colorDot[0], colorDot[1], colorDot[2]);
    set_draw_pixel_print(matrix, pointX, pointY, colorDot[0], colorDot[1], colorDot[2]);
  }
}

/**
 * @brief Bitmap Image Drawing Protocol 
 * @details 로테이션 Canvas는 Width, Height 반전 하지 않음 -> drawPixel 시에 값 변환해서 적용
 *          g_picture_matrix 로테이션 값에 따라 버퍼 변환
 * @param data picture header packet
 */
void LedPannel::set_picture_start(char* data) {
  uint16_t sendCount = 0;
  char buffer[1] = {0};
  char colorBorder[3]; 
  int inx = 0;
  uint16_t _picture_width = 0, _picture_height = 0; //  App 에서 보내는 picture width, height 사이즈

  canvas16->fillScreen(0);
  set_action_off(); /* scrolling text off */

  m_optionCmd = data[inx++];
  sendCount = sendCount | (data[inx++] << 8);
  sendCount =  sendCount | data[inx++];
  m_actionCmd = data[inx++];

  m_actionTime = 0;
  m_actionTime = m_actionTime | (data[inx++] << 8);
  m_actionTime = m_actionTime | data[inx++];
  g_pageNum = data[inx++] + 1;

  m_effectCmd = data[inx++];
  m_effect_time = 0;
  m_effect_time = m_effect_time | data[inx++] << 8;
  m_effect_time = m_effect_time | data[inx++];

  memcpy(colorBorder, &data[inx], sizeof(colorBorder));
  inx = inx + sizeof(colorBorder);
  m_color_border = matrix->Color(color_div(colorBorder[0]), color_div(colorBorder[1]), color_div(colorBorder[2]));

  m_effect_color_change_value = data[inx++];
  // m_effect_color_change_value *= 2;  //  color change value 2 ~ 12;

  m_effect_rain_star_time = 0;
  m_effect_rain_star_time = m_effect_rain_star_time | data[inx++] << 8;
  m_effect_rain_star_time = m_effect_rain_star_time | data[inx++];

   _picture_width = _picture_width | data[inx++] << 8;
  _picture_width = _picture_width | data[inx++];

  _picture_height = _picture_height | data[inx++] << 8; 
  _picture_height = _picture_height | data[inx++];

  canvasMemoryAllocate(_picture_width, _picture_height);

  if (sendCount == 0) {
    transmitCount = 0;
    buffer[0] = TS_STOP;
    pBufferSerial->transmit_data(SET_PICTURE_START, buffer, 1);

    if (saveFlag) { //  Save Cmd 일 경우 페이지 삭제
      char pageStr[15];
      sprintf(pageStr, "/page%d.txt", g_pageNum); 
      writeFile(LittleFS, pageStr, NULL, 0);  //  해당 페이지 삭제 

      pageEnableList[g_pageNum - START_ALIVE] = FLASH_PAGE_DISENABLE_VALUE;  //  페이지 Enable List 저장
      writeFile(LittleFS, FLASH_PAGE_ENABLE_LIST, pageEnableList, PAGE_TOTAL_COUNT);
    }
    return;
  } else {  //  페이지 지우기가 아닌 경우 transmitCount 계산 
    transmitCount = (sendCount / TRANSMIT_COUNT);   
  }

  int _rotation_w =  g_canvasWidth, _rotation_h = g_canvasHeight; //  로테이션 된 상태의 width, height
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) {
    _rotation_w = g_canvasHeight;
    _rotation_h = g_canvasWidth;
  }

  if (g_picture_matrix != NULL ) {
    for (int i = 0; i < _rotation_w; i++) free(g_picture_matrix[i]);
    free(g_picture_matrix);
  }

  g_picture_matrix = (struct Picture_Matrix_RGB **) calloc(_rotation_w, sizeof(struct Picture_Matrix_RGB *)); //  2D BitMap Dynamic Arrayallocation bitmap to Row
  for (int i = 0; i < _rotation_w; i++) g_picture_matrix[i] = (struct Picture_Matrix_RGB*) calloc (_rotation_h, sizeof(struct Picture_Matrix_RGB)); //  2D BitMap Dynamic Arrayallocation bitmap to Col

  if (sendCount % TRANSMIT_COUNT != 0)  transmitCount += 1; //  나머지 값이 있다면 + 1

  if (packet_index_check != NULL) {
    free(packet_index_check);
  }
  packet_index_check = (uint8_t*) calloc(transmitCount, sizeof(uint8_t));
  
  buffer[0] = TS_CONTINUE;
  pBufferSerial->transmit_data(SET_PICTURE_START, buffer, 1);

}

void LedPannel::save_picture(char* data) { 
  saveFlag = true;
  set_picture_start(data);
}

/**
 * @brief picture drawing packet
 * @apiNote - x, y, r, g, b 5Bytes 순차적 패킷
 * @param data 
 */
void LedPannel::set_picture(char* data) 
{
  int inx = 0;
  uint8_t transmitNum = 0;
  byte pointX;
  byte pointY;
  byte colorDot[3];
  uint16_t dotLen = 0;

  transmitNum = data[inx++];
  
  packet_index_check[transmitNum] = RECIVE_ACK;

  dotLen = dotLen | (data[inx++] << 8);
  dotLen = dotLen | data[inx++];

  for(int i=0; i<dotLen; i++)
  {
    pointX = data[inx++];
    pointY = data[inx++];
    colorDot[0] = data[inx++];
    colorDot[1] = data[inx++];
    colorDot[2] = data[inx++];
    // set_draw_pixel(matrix, pointX, pointY, colorDot[0], colorDot[1], colorDot[2]);
    g_picture_matrix[pointX][pointY].red = colorDot[0];
    g_picture_matrix[pointX][pointY].green = colorDot[1];
    g_picture_matrix[pointX][pointY].blue = colorDot[2];
  }  
}

void LedPannel::set_picture_end()
{ 
  char nackList[transmitCount] = {0};
  int nackCount = 0;

  for(uint8_t i = 0; i < transmitCount; i++) {
    if(packet_index_check[i] != RECIVE_ACK ) {
      nackList[nackCount++] = i; 
      // Serial.print("nack num : ");
      // PRINTLN(nackList[nackCount - 1 ], DEC);
    }
  }
  
  if(nackCount != 0) {  //못받은 패킷이 있다면 PICTURE_RECIVE_NACK
    pBufferSerial->transmit_data(PICTURE_RECIVE_NACK, nackList, nackCount);
    return;    
  }
  pBufferSerial->transmit_data(PICTURE_RECIVE_ACK);

  int _rotation_w =  g_canvasWidth, _rotation_h = g_canvasHeight; //  로테이션 된 상태의 width, height
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) {
    _rotation_w = g_canvasHeight;
    _rotation_h = g_canvasWidth;
  }

  // 받은 Picture 데이터를 캔버스에서 저장했던 부분
  // 이제는 받았던 자료를 rgb buffer에 넣는 구조로 변경 필요
  for(int x=0; x < _rotation_w; x++)
  {  
    for(int y=0; y < _rotation_h; y++){
      rgbBuf[x][y].red = g_picture_matrix[x][y].red;
      rgbBuf[x][y].green = g_picture_matrix[x][y].green;
      rgbBuf[x][y].blue = g_picture_matrix[x][y].blue;
    }
  }
  
  //memcpy(rgbBuf, g_picture_matrix, sizeof(Picture_Matrix_RGB));


  //matrix->show();
  matrix->drawRGBBitmap(0 , 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
  Serial.printf("%s, %d\n", __FUNCTION__, __LINE__);
  if (saveFlag == true ) {  //SaveFlag 검사
    save_picture_excute();
    saveFlag = false; 
  }
  set_effect(m_effectCmd);

  if (m_actionCmd == CMD_ACT_DEFALUT) {
    categoryActionFlag = false;
  } else {
    set_screen_color(matrix, 0, 0, 0); 
    set_action();
  }

  if (g_picture_matrix != NULL ) {
    for (int i = 0; i < _rotation_w; i++) {
       free(g_picture_matrix[i]);
    }
    free(g_picture_matrix);
  }

  delay(300); //  BT 전송 연속 전송에 대한 안정성 딜레이
  if ((m_optionCmd & OPTION_GIF_BACKGROUND) != 0) { //  option 값에 gif background 비트 마스크 있는 경우 다음 패킷 요청
    PRINTF("gif background!!! \n");
    pBufferSerial->transmit_data(SET_NEXT_PACKET);
  }
  
  g_picture_matrix = NULL;
  matrix->show();
  Serial.printf("%s, %d\n", __FUNCTION__, __LINE__);

}
// CMD : SET_PICTURE_ONCE_TIME  한번에 bitmap packet 모두 수신
void LedPannel::set_picture_once_time(char* data, int len)
{
  PRINTLN("set_picture_once_time");
  char buffer[1] = {0};
  char colorBorder[3]; 
  uint16_t saveFlag;
  int packet_idx = 0;
  
  canvas16->fillScreen(0);
  set_action_off(); /* scrolling text off */

  m_actionCmd = data[packet_idx++];  //  1

  m_actionTime = 0;
  m_actionTime = m_actionTime | (data[packet_idx++] << 8);
  m_actionTime = m_actionTime | data[packet_idx++];
  g_pageNum = data[packet_idx++] + 1;

  m_effectCmd = data[packet_idx++];
  m_effect_time = 0;
  m_effect_time = m_effect_time | data[packet_idx++] << 8;
  m_effect_time = m_effect_time | data[packet_idx++];

  memcpy(colorBorder, &data[packet_idx], sizeof(colorBorder));
  packet_idx = packet_idx + sizeof(colorBorder);
  m_color_border = matrix->Color(color_div(colorBorder[0]), color_div(colorBorder[1]), color_div(colorBorder[2]));


  m_effect_color_change_value = data[packet_idx++];
  m_effect_color_change_value *= 2;  //  color change value 2 ~ 12;

  m_effect_rain_star_time = 0;
  m_effect_rain_star_time = m_effect_rain_star_time | data[packet_idx++] << 8;
  m_effect_rain_star_time = m_effect_rain_star_time | data[packet_idx++];
  saveFlag = data[packet_idx++];

  byte pointX;
  byte pointY;
  byte colorDot[3];

  while(packet_idx < len)
  {
    pointX = data[packet_idx++];
    pointY = data[packet_idx++];
    colorDot[0] = data[packet_idx++];
    colorDot[1] = data[packet_idx++];
    colorDot[2] = data[packet_idx++];
    set_draw_pixel(matrix, pointX, pointY, colorDot[0], colorDot[1], colorDot[2]);
  }  

  matrix->drawRGBBitmap(0 , 0, canvas16->getBuffer(), g_board_config.matrix_width, g_board_config.matrix_height);
}

void LedPannel::save_picture_excute()
{
  // PRINTLN("save_picture_excute");
  int bufferSize = (g_canvasWidth * g_canvasHeight * 3) + SIZE_CMD + SIZE_PAGE + SIZE_ACTION + SIZE_EFFECT + SIZE_OPTION + SIZE_PICTURE_SIZE;  // 64 x 32 x 3(r,g,b) + Packet Info Data
  uint8_t* picture_buffer = (uint8_t*) calloc (bufferSize, sizeof(uint8_t));
  int inx = 0;
  char pageStr[15];
  uint8_t pageNum;
  
  memset(picture_buffer, 0, bufferSize);  // set_draw_pixel[32][64].red {0, };
  PRINTLN(bufferSize);

  pageNum = g_pageNum;
  picture_buffer[inx++] = SET_PICTURE;
  picture_buffer[inx++] = m_optionCmd;
  picture_buffer[inx++] = pageNum;
  picture_buffer[inx++] = m_actionCmd;
  picture_buffer[inx++] = (char)(m_actionTime >>  8);
  picture_buffer[inx++] = (char)(m_actionTime & 0xff);

  picture_buffer[inx++] = m_effectCmd;
  picture_buffer[inx++] = (char)((m_effect_time & 0x0000ff00) >> 8);
  picture_buffer[inx++] = (char)((m_effect_time & 0x000000ff) >> 0);
  picture_buffer[inx++] = (char)((m_color_border & 0x00ff0000) >> 16);
  picture_buffer[inx++] = (char)((m_color_border & 0x0000ff00) >> 8);
  picture_buffer[inx++] = (char)((m_color_border & 0x000000ff) >> 0);
  picture_buffer[inx++] = m_effect_color_change_value;
  picture_buffer[inx++] = (char)((m_effect_rain_star_time & 0x0000ff00) >> 8);
  picture_buffer[inx++] = (char)((m_effect_rain_star_time & 0x000000ff) >> 0);
  picture_buffer[inx++] = g_canvasWidth >> 8;
  picture_buffer[inx++] = g_canvasWidth;
  picture_buffer[inx++] = g_canvasHeight >> 8;
  picture_buffer[inx++] = g_canvasHeight;

  int _rotation_w =  g_canvasWidth, _rotation_h = g_canvasHeight; //  로테이션 된 상태의 width, height
  if (g_rotationValue == ROTATION_VALUE_PORTRAIT) {
    _rotation_w = g_canvasHeight;
    _rotation_h = g_canvasWidth;
  }

  for (int x=0; x < _rotation_w; x++)
  {  
    for (int y=0; y < _rotation_h; y++){
      picture_buffer[inx++] = g_picture_matrix[x][y].red;
      picture_buffer[inx++] = g_picture_matrix[x][y].green;
      picture_buffer[inx++] = g_picture_matrix[x][y].blue;
    }
  }

  // memcpy(&picture_buffer[inx], canvas16->getBuffer(),  (g_canvasWidth * g_canvasHeight * 2));
  
  if (transmitCount == 0) {
    bufferSize = 0;
  }

  sprintf(pageStr, "/page%d.txt", pageNum); 
  if (pageNum == LOGO_PAGE_IDX + START_ALIVE) {
    prefs.begin(PREFS_NAME);
    prefs.putBytes(PREFS_KEY_LOGO_PAGE, picture_buffer, bufferSize);
    prefs.end();
  }
  else {
    writeFile(LittleFS, pageStr, picture_buffer, bufferSize);  //  페이지 Data Buffer 저장
    
    pageEnableList[pageNum - START_ALIVE] = FLASH_PAGE_ENABLE_VALUE;  //  페이지 Enable List 저장
    writeFile(LittleFS, FLASH_PAGE_ENABLE_LIST, pageEnableList, PAGE_TOTAL_COUNT);  
  }
  if (m_actionCmd == CMD_ACT_DEFALUT)  matrix->drawRGBBitmap(0 , 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
  free(picture_buffer);
}



void LedPannel::set_text(char* data)
{
  int inx = 0;
  m_optionCmd = data[inx++];
  uint8_t pageNum = data[inx++];
  int strLen = 0;
  strLen = strLen | data[inx++] << 8;
  strLen = strLen | data[inx++];
  char* textData = (char*)malloc(strLen+1); /* For strings, +1 it must insert null at the end of the string. */
  String textBoxData;
  char colorFont[3]; 
  char colorScr[3]; 
  int colorRgbScr;
  char effectCmd; 
  char colorBorder[3]; 

  

  set_action_off(); /* previous action text off */
  if (strLen == 0) {  // Text 없을 시 화면 지우고 return
    matrixClear();  
    return;
  };
  memcpy(textData, &data[inx], strLen);

  inx = inx + strLen;
  textData[strLen] = 0;
  textBoxData = textData;

  g_textSize = (uint8_t)data[inx++];
  memcpy(colorFont, &data[inx], sizeof(colorFont));
  inx = inx + sizeof(colorFont);
  memcpy(colorScr, &data[inx], sizeof(colorScr));
  inx = inx + sizeof(colorScr);
  g_fontColor = matrix->Color(color_div(colorFont[0]), color_div(colorFont[1]), color_div(colorFont[2]));
  colorRgbScr = matrix->Color(color_div(colorScr[0]), color_div(colorScr[1]), color_div(colorScr[2]));

  m_actionCmd = data[inx++];

  m_actionTime = 0;
  m_actionTime = m_actionTime | data[inx++] << 8;
  m_actionTime = m_actionTime | data[inx++];

  effectCmd = data[inx++];
  m_effect_time = 0;
  m_effect_time = m_effect_time | data[inx++] << 8;
  m_effect_time = m_effect_time | data[inx++];

  memcpy(colorBorder, &data[inx], sizeof(colorBorder));
  inx = inx + sizeof(colorBorder);
  m_color_border = matrix->Color(color_div(colorBorder[0]), color_div(colorBorder[1]), color_div(colorBorder[2]));

  m_effect_color_change_value = data[inx++];
  // m_effect_color_change_value *=2;  //  color change value 2 ~ 12;

  m_effect_rain_star_time = 0;
  m_effect_rain_star_time = m_effect_rain_star_time | data[inx++] << 8;
  m_effect_rain_star_time = m_effect_rain_star_time | data[inx++];

  if ((m_optionCmd & OPTION_GIF_BACKGROUND) != 0) { //  option 값에 gif background 비트 마스크 있는 경우 다음 패킷 요청
    PRINTF("gif background!!! \n");
    pBufferSerial->transmit_data(SET_NEXT_PACKET);
  }

  setupFont(g_textSize);
  init_strArr(textBoxData);

  if (m_actionCmd == CMD_ACT_DEFALUT) {
    print_center_align_text(lineCnt, matrix, true, colorRgbScr);
    categoryActionFlag = false;
  }
  else {
    print_center_align_text(lineCnt, matrix, false, colorRgbScr);
    set_action(); 
  }
  
  set_effect(effectCmd);  
  free(textData);
}

void LedPannel::save_text(char* data, int len)
{
  int idx = 0;
  uint8_t pageNum;
  int strLen;
  char pageStr[15];
  size_t bufferSize = len + SIZE_CMD;
  uint8_t* buffer = (uint8_t*)malloc(bufferSize); 
  uint8_t _saveEnableValue = FLASH_PAGE_ENABLE_VALUE;  //  페이지 Enable 값
  idx++;  //  idx 0 is option value! 
  pageNum = data[idx++]; 
  strLen = strLen | data[idx++] << 8;
  strLen = strLen | data[idx++];

  memset(buffer, 0, bufferSize);  // Buffer Init (0);
  
  buffer[DISPLAY_CMD_IDX] = SET_TEXT;
  memcpy(&buffer[DISPLAY_OPTION_IDX], data, len);
  if (strLen == 0) {  // Flash Delete Action (어플리케이션에서 빈 페이지로 재생 버튼 클릭)
    bufferSize = 0;
    _saveEnableValue = FLASH_PAGE_DISENABLE_VALUE;
  }

  //flash memory write logic
  
  sprintf(pageStr, "/page%d.txt", pageNum + START_ALIVE); //현재 0페이지로 들어오게 되어있어서 0 + 1 연산

  if (pageNum == LOGO_PAGE_IDX) {
    prefs.begin(PREFS_NAME);
    prefs.putBytes(PREFS_KEY_LOGO_PAGE, buffer, bufferSize);
    prefs.end();
  } else {
    writeFile(LittleFS, pageStr, buffer, bufferSize); //  페이지 Data Buffer 저장

    pageEnableList[pageNum] = _saveEnableValue;  //  페이지 Enable List 저장
    writeFile(LittleFS, FLASH_PAGE_ENABLE_LIST, pageEnableList, PAGE_TOTAL_COUNT);  
  }
  free(buffer);

  if (strLen != 0) set_text(data);  //  플래시 지우기 동작이 아닐 시 set_text
}

/**************************************************************************/
/**
    @brief    - Color Text Funtion - Protrocol type1 (SET_COLOR_TEXT)
    @details  Packet Protrocol : PageNum (1Byte), Screen Color(3Bytes), Font Size(1Byte), Text Group Count(1Byte),
                                  String(N Bytes), Font Color(3Byte * Group Count)
    @param    data  블루투스 통신 데이터
    @param    len   블루투스 패킷 데이터 길이

*/
/**************************************************************************/

void LedPannel::set_color_text(char* data)
{
  int inx = 0;
  uint8_t pageNum;
  char colorFont[3]; 
  char colorScr[3];
  uint8_t textGroupCount = 0; 
  uint8_t textSize = 0;
  int colorRgbScr; 
  String textBoxData;
  String str = "";
  char effectCmd; 
  char colorBorder[3]; 

  m_optionCmd = data[inx++];
  pageNum = data[inx++];
  memcpy(colorScr, &data[inx], sizeof(colorScr));
  colorRgbScr = matrix->Color(color_div(colorScr[0]), color_div(colorScr[1]), color_div(colorScr[2]));

  inx = inx + sizeof(colorScr);
  g_textSize = data[inx++];
  textGroupCount = data[inx++];

  if (textGroupCount == 0) {   //  텍스트 없을 시 matrix 지우고 return
    matrix->clear();
    matrix->show();
    return;
  };
  
  if (color_text_group_info != NULL){
      free((void*)color_text_group_info);
  }
  color_text_group_info = (Color_text_group_info*) malloc (sizeof(struct Color_text_group_info) * textGroupCount);

  setupFont(g_textSize);
  set_action_off(); /* scrolling text off */
  
  for (int i = 0; i < textGroupCount; i++) {
    textSize = data[inx++];

    if (i == 0) {
      color_text_group_info[i].textSize = textSize;
    } else {
      color_text_group_info[i].textSize = color_text_group_info[i - 1].textSize + textSize;
    }

    if (textSize > 0) {
      for (int j=0; j<textSize; j++) {
        str.concat(data[inx++]);
      }
      memcpy(colorFont, &data[inx], sizeof(colorFont));
      inx = inx + sizeof(colorFont);

      color_text_group_info[i].color_rgb.red = colorFont[0];
      color_text_group_info[i].color_rgb.green = colorFont[1];
      color_text_group_info[i].color_rgb.blue = colorFont[2];
    }
    else PRINTLN("[Error] Text Size");
  }
  init_strArr(str);
  
  m_actionCmd = data[inx++];
  m_actionTime = 0;
  m_actionTime = m_actionTime | data[inx++] << 8;
  m_actionTime = m_actionTime | data[inx++];

  effectCmd = data[inx++];
  m_effect_time = 0;
  m_effect_time = m_effect_time | data[inx++] << 8;
  m_effect_time = m_effect_time | data[inx++];

  memcpy(colorBorder, &data[inx], sizeof(colorBorder));
  inx = inx + sizeof(colorBorder);
  m_color_border = matrix->Color(color_div(colorBorder[0]), color_div(colorBorder[1]), color_div(colorBorder[2]));

  m_effect_color_change_value = data[inx++];
  // m_effect_color_change_value *=2;  //  color change value 2 ~ 12;
  
  m_effect_rain_star_time = 0;
  m_effect_rain_star_time = m_effect_rain_star_time | data[inx++] << 8;
  m_effect_rain_star_time = m_effect_rain_star_time | data[inx++];

  if ((m_optionCmd & OPTION_GIF_BACKGROUND) != 0) { //  option 값에 gif background 비트 마스크 있는 경우 다음 패킷 요청
    PRINTF("gif background!!! \n");
    pBufferSerial->transmit_data(SET_NEXT_PACKET);
  }
  Serial.printf("%s, %d\n", __FUNCTION__, __LINE__);
  if (m_actionCmd == CMD_ACT_DEFALUT)
    print_center_align_color_text(lineCnt, matrix, true, colorRgbScr, color_text_group_info);
  else {
    print_center_align_color_text(lineCnt, matrix, false, colorRgbScr, color_text_group_info);
    set_action();   
  }
  set_effect(effectCmd);
}

void LedPannel::save_color_text(char* data, int len)
{
  int idx = 0;
  uint8_t pageNum;
  uint8_t textGroupCount;
  char pageStr[15];

  size_t bufferSize = len + SIZE_CMD;
  uint8_t* buffer = (uint8_t*)malloc(bufferSize); 
  
  uint8_t _saveEnableValue = FLASH_PAGE_ENABLE_VALUE;  //  페이지 Enable 값

  idx++;  //  idx 0 is option value! 
  pageNum = data[idx++]; 
  memset(buffer, 0, bufferSize);  // Buffer Init (0);
  
  buffer[DISPLAY_CMD_IDX] = SET_COLOR_TEXT;
  memcpy(&buffer[DISPLAY_OPTION_IDX], data, len);
  textGroupCount = data[5];

  if (textGroupCount == 0) {  // Flash Delete Action (어플리케이션에서 빈 페이지로 재생 버튼 클릭)
    bufferSize = 0;
    _saveEnableValue = FLASH_PAGE_DISENABLE_VALUE;
  }

  //flash memory write logic

  sprintf(pageStr, "/page%d.txt", pageNum + START_ALIVE); //현재 0페이지로 들어오게 되어있어서 0 + 1 연산

  if (pageNum == LOGO_PAGE_IDX) {
    prefs.begin(PREFS_NAME);
    prefs.putBytes(PREFS_KEY_LOGO_PAGE, buffer, bufferSize);
    prefs.end();
  } else {
    writeFile(LittleFS, pageStr, buffer, bufferSize);
    pageEnableList[pageNum] = _saveEnableValue;  //  페이지 Enable List 저장
    writeFile(LittleFS, FLASH_PAGE_ENABLE_LIST, pageEnableList, PAGE_TOTAL_COUNT);  
  }
  free(buffer);  

  if (textGroupCount != 0) set_color_text(data);  //  플래시 지우기 동작이 아닐 시 set_color_text
}

void LedPannel::set_scr_clear(char* data)
{
  // PRINTLN("screen clear");
  matrixClear();
}

void LedPannel::set_scr_color(char* data)
{
  set_action_off(); /* previous action text off */
  // PRINTLN("screen clolor");
  byte ScrColor[3];
  memcpy(ScrColor, data, sizeof(ScrColor));      
  fillScreenEx(ScrColor[0], ScrColor[1], ScrColor[2], matrix); 

}

void LedPannel::set_perform_mode(char* data)
{
  byte lora_id = data[0];
  byte lora_channel = data[1];
  char txt[] = "안녕하세요";

  // Serial.print("PERFORM lora_id is : "); PRINTLN(lora_id);
  // Serial.print("PERFORM lora_channel is : "); PRINTLN(lora_channel);

  pBufferSerial->transmit_data(SET_PERFORM_MODE, (char*)txt, sizeof(txt));
}

void LedPannel::setStartMode(char* data) {
  uint8_t buffer[SETTING_DATA_COUNT];
  int i = 0;
  int readBufferIndex = 0;

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer); 
  startModeNum = data[0];
  buffer[SETTING_IDX_MODE] = startModeNum;
  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
}

void LedPannel::saveLedLevel(char* data) {
  uint8_t buffer[SETTING_DATA_COUNT];
  int i = 0;
  int readBufferIndex = 0;

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  ledDivLevel = getLedDivValue(data[0]);

  String str = "밝기 : ";
  int numLedLevel = (int)data[0];
  str.concat(numLedLevel);

  buffer[SETTING_IDX_BRIGHT] = data[0];
  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);

  print_str_matrix_basic(FONT_TWO_SIZE, str, CMD_ACT_DEFALUT, matrix->Color((uint8_t)ceil(((float)255 * ledDivLevel)), (uint8_t)ceil(((float)0 * ledDivLevel)), (uint8_t)ceil(((float)0* ledDivLevel))));
}

void LedPannel::setLedLevel(char* data) {
  uint16_t fontColor;
  float divLev;
  float ledLevelTemp = ledDivLevel;
  set_action_off();

  divLev = getLedDivValue(data[0]);
  
  String str = "밝기 : ";
  int numLedLevel = (int)data[0];
  str.concat(numLedLevel);

  ledDivLevel = getLedDivValue(data[0]);
  print_str_matrix_basic(FONT_TWO_SIZE, str, CMD_ACT_DEFALUT, matrix->Color((uint8_t)ceil(((float)255 * ledDivLevel)), (uint8_t)ceil(((float)0 * ledDivLevel)), (uint8_t)ceil(((float)0* ledDivLevel))));
  ledDivLevel = ledLevelTemp;
}

void LedPannel::setUseMode(char* data) {
  uint8_t buffer[SETTING_DATA_COUNT];
  int i = 0;
  int readBufferIndex = 0;

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  i = SETTING_IDX_DP_CHECK;   
  for (int cnt = 0; cnt <4; cnt++) {  //사용 모드 선택 여부를 버퍼에 저장 (선택 시 1, 선택 되지 않았을 시 0)
    buffer[i++] = data[cnt];
  }

  i = SETTING_IDX_DP_CHECK;
  memset(useModeList, 0, sizeof(useModeList));
  modeCount = 1;
  for (int cnt = 1; cnt < sizeof(useModeList); cnt++) { // useModeList에 선택된 리스트들의 ModeNum을 저장
    if (buffer[i]) {
      useModeList[modeCount] = cnt; 
      modeCount += 1;
      ++i;
    }
  } 

  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
}

void LedPannel::setLogoTime(char* data) {
  uint8_t buffer[SETTING_DATA_COUNT];

  logoTime = 0;
  logoTime = logoTime | (data[0] << 24);
  logoTime = logoTime | (data[1] << 16);
  logoTime = logoTime | (data[2] << 8);
  logoTime = logoTime | data[3];

  int readBufferIndex = 0;

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  buffer[SETTING_IDX_LOGO_TIME_FIRST_DIGIT] = ((logoTime & 0xff000000) >> 24);
  buffer[SETTING_IDX_LOGO_TIME_SECOND_DIGIT] = ((logoTime & 0x00ff0000) >> 16);
  buffer[SETTING_IDX_LOGO_TIME_THIRD_DIGIT] = ((logoTime & 0x0000ff00) >> 8);
  buffer[SETTING_IDX_LOGO_TIME_FOUTH_DIGIT] = (logoTime & 0x000000ff);

  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
}

void LedPannel::setLogoEnable(char* data) {
  uint8_t buffer[SETTING_DATA_COUNT];

  logoEnable = data[0];
  int readBufferIndex = 0;

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  buffer[SETTING_IDX_LOGO_ENABLE_BIT] = logoEnable;

  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
}

void LedPannel::setPageTime(char* data) {
  // PRINTLN("setPageTime");

  uint8_t buffer[SETTING_DATA_COUNT];

  displayPageTime = 0;
  displayPageTime = displayPageTime | (data[0] << 24);
  displayPageTime = displayPageTime | (data[1] << 16);
  displayPageTime = displayPageTime | (data[2] << 8);
  displayPageTime = displayPageTime | data[3];

  int readBufferIndex = 0;

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  buffer[SETTING_IDX_PAGE_TIME_FIRST_DIGIT] = ((displayPageTime & 0xff000000) >> 24);
  buffer[SETTING_IDX_PAGE_TIME_SECOND_DIGIT] = ((displayPageTime & 0x00ff0000) >> 16);
  buffer[SETTING_IDX_PAGE_TIME_THIRD_DIGIT] = ((displayPageTime & 0x0000ff00) >> 8);
  buffer[SETTING_IDX_PAGE_TIME_FOUTH_DIGIT] = (displayPageTime & 0x000000ff);
  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
}

void LedPannel::setPageChaneMode(char* data) {
      PRINTLN("setPageChaneMode");
  uint8_t buffer[SETTING_DATA_COUNT];
  uint8_t nextPageModeValue = DISPLAY_AUTO_PAGE_CHANGE;
  nextPageModeValue = data[0];

  if (nextPageModeValue == DISPLAY_AUTO_PAGE_CHANGE) autoNextPageMode = true;
  else if (nextPageModeValue == DISPLAY_MENUAL_PAGE_CHANGE) autoNextPageMode = false;

  int readBufferIndex = 0;

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  buffer[SETTING_IDX_PAGE_CHANGE_WAY] = nextPageModeValue;
  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
}

void LedPannel::setLogoRepeat(char* data) {
  prefsBoardInfoArray[BOARD_INFO_IDX_LOGO_REPEAT_ON_OFF] = data[0];
  prefs.begin(PREFS_NAME);
  prefs.putBytes(PREFS_KEY_BOARD_INFO_PAGE, prefsBoardInfoArray, BOARD_INFO_PREFS_IDX_TOTAL_COUNT);
  prefs.end();
}

void LedPannel::setBtName(char* data, int len) {
  char ch_str[len + 1];

  for (int i = 0; i<len; i++) ch_str[i] = data[i];
  ch_str[len] = '\0';
  
  bt_name.clear();
  bt_name = ch_str;
  
  writeFile(LittleFS, "/bt_name.txt",(uint8_t*) ch_str, len + 1);  //  해당 페이지 삭제 
  ESP.restart();
}

// BT Cmd : SET_PAGE_TIMER_SWITCH
void LedPannel::setPageTimerSwitch(char* data) {
  uint8_t pageTimerSwitchBuffer[PAGE_TOTAL_COUNT * 5] = {0};
  int readBufferIndex = 0;

  int pageNum = data[0];
  timeSwitchStartHour[pageNum] = data[1];
  timeSwitchStartMinute[pageNum] = data[2];
  timeSwitchFromHour[pageNum] = data[3];
  timeSwitchFromMinute[pageNum] = data[4];

  
  readBufferIndex = readFile(LittleFS, "/pageTimer.txt", pageTimerSwitchBuffer); 
  pageTimerSwitchBuffer[pageNum * 5 + 1] = timeSwitchStartHour[pageNum];
  pageTimerSwitchBuffer[pageNum * 5 + 2 ] = timeSwitchStartMinute[pageNum];
  pageTimerSwitchBuffer[pageNum * 5 + 3 ] = timeSwitchFromHour[pageNum];
  pageTimerSwitchBuffer[pageNum * 5 + 4 ] = timeSwitchFromMinute[pageNum];

  writeFile(LittleFS, "/pageTimer.txt", pageTimerSwitchBuffer, PAGE_TOTAL_COUNT * 5); 
}

// BT Cmd : SET_PAGE_TIMER_SWITCH_ENABLE
void LedPannel::setPageTimerSwitchEnable(char* data) {
  uint8_t pageTimerSwitchBuffer[PAGE_TOTAL_COUNT * 5] = {0};
  int readBufferIndex = 0;

  int pageNum = data[0];
  timeSwitchIsEnable[pageNum] = data[1];
    
  readBufferIndex = readFile(LittleFS, "/pageTimer.txt", pageTimerSwitchBuffer); 
  pageTimerSwitchBuffer[pageNum * 5 + 0] = timeSwitchIsEnable[pageNum];

  writeFile(LittleFS, "/pageTimer.txt", pageTimerSwitchBuffer, PAGE_TOTAL_COUNT * 5); 

  byte buffer[PAGE_TOTAL_COUNT] = {0};
  for (int i = 0; i < PAGE_TOTAL_COUNT; i++) buffer[i] = (byte)timeSwitchIsEnable[i];

  pBufferSerial->transmit_data(SET_PAGE_TIMER_SWITCH_ENABLE, (char*) buffer, PAGE_TOTAL_COUNT);
}

// BT Cmd : get_PAGE_TIMER_SWITCH_ENABLE
void LedPannel::getPageTimerSwitchEnable(char* data) {
  PRINTLN("getPageTimerSwitchEnable");
  byte buffer[PAGE_TOTAL_COUNT] = {0};
  for (int i = 0; i < PAGE_TOTAL_COUNT; i++) buffer[i] = (byte)timeSwitchIsEnable[i];

  pBufferSerial->transmit_data(GET_PAGE_TIMER_SWITCH_ENABLE, (char*) buffer, PAGE_TOTAL_COUNT);
}

/**
 * To be Set Sign Functiono (시계. . . .) Save Check
 * BT Cmd : SET_MATRIX_FUNCTION
 */
void LedPannel::setMatrixFunction(char* data, int len) {
  // PRINTLN("setMatrixFunction");
  canvasMemoryAllocate(g_board_config.matrix_width, g_board_config.matrix_height);
  char functionValue;
  char colorBorder[3]; 
  bool saveFlag;
  int packet_idx = 0;
  uint8_t pageNum = 0; 
   
  matrixClear(); 
  setupFont(4);
  last_min = -1;
  
  m_optionCmd = data[packet_idx++]; 
  m_actionCmd = data[packet_idx++];  //  1
  m_actionTime = 0;
  m_actionTime = m_actionTime | (data[packet_idx++] << 8);
  m_actionTime = m_actionTime | data[packet_idx++];
  pageNum = data[packet_idx++];

  m_effectCmd = data[packet_idx++];
  m_effect_time = 0;
  m_effect_time = m_effect_time | data[packet_idx++] << 8;
  m_effect_time = m_effect_time | data[packet_idx++];

  memcpy(colorBorder, &data[packet_idx], sizeof(colorBorder));
  packet_idx = packet_idx + sizeof(colorBorder);
  m_color_border = matrix->Color(color_div(colorBorder[0]), color_div(colorBorder[1]), color_div(colorBorder[2]));

  m_effect_color_change_value = data[packet_idx++];
  m_effect_color_change_value *= 2;  //  color change value 2 ~ 12;

  m_effect_rain_star_time = 0;
  m_effect_rain_star_time = m_effect_rain_star_time | data[packet_idx++] << 8;
  m_effect_rain_star_time = m_effect_rain_star_time | data[packet_idx++];
  
  saveFlag = data[packet_idx++];
  if (saveFlag) {
    // PRINTF("saveFlag : %d \n", saveFlag);
    data[packet_idx - 1] = false; //  Save Flag == true 일 경우 한번만 저장하고 flase로 변경 / 현재 버퍼 인덱스 - 1
    
    char pageStr[15];
    size_t bufferSize = len + SIZE_CMD;
    uint8_t* buffer = (uint8_t*)malloc(bufferSize); 

    memset(buffer, 0, bufferSize);  // Buffer Init (0);

    buffer[0] = SET_MATRIX_FUNCTION;
    memcpy(&buffer[1], data, len);
    
    sprintf(pageStr, "/page%d.txt", pageNum + 1); //현재 0페이지로 들어오게 되어있어서 0 + 1 연산

    writeFile(LittleFS, pageStr, buffer, bufferSize);
    pageEnableList[pageNum] = FLASH_PAGE_ENABLE_VALUE;  //  페이지 Enable List 저장
    writeFile(LittleFS, FLASH_PAGE_ENABLE_LIST, pageEnableList, PAGE_TOTAL_COUNT);  
    
    free(buffer);
  } 
  functionValue = data[packet_idx++]; 

  /** 
   * 통신으로 API 요청 패킷 보냈을 경우에는 경과 시간 체크하지않고 다시 API 요청
  */
  if (len != 0) {  //  통신으로 함수 실행하였을 경우 != 0, 디스플레이 모드에서 함수 실행하였을 경우 len == 0

  }

  categoryActionFlag = true;

  // PRINTF("functionValue : %d \n", functionValue);
  if ((functionValue & FUNCTION_CLOCK_BIT_MASK) != 0) {
    if (isTimeSynch == false) {
      print_str_matrix_basic(FONT_ONE_SIZE, "시간 설정 오류\n블루투스 연결 필요", CMD_ACT_RIGHT_LEFT, matrix->Color(150,0,0));
      return;
    }
    categoryActionFlag = false;
    m_matrix_function_clock = true;
  }

  if ((m_optionCmd & OPTION_GIF_BACKGROUND) != 0) { //  option 값에 gif background 비트 마스크 있는 경우 다음 패킷 요청
    PRINTF("gif background!!! \n");
    pBufferSerial->transmit_data(SET_NEXT_PACKET);
  }
}

void LedPannel::print_function_clock() { 
  // PRINTF("print_function_clock!! \n");
    get_localtime();

    // if (isTimeSynch == false)   PRINTLN("isTimeSynch unSynch!!!");
    time_t t;
    static time_t last_t = 0;
    uint8_t x_pos_offset = (g_board_config.matrix_width - 64) / 2;

    t = time(NULL);
    if (last_t == t) return; // draw each second
    last_t = t;
    
    if (last_min != g_timeinfo.tm_min) 
    {
      matrix->clear();
      matrix->show();
      if (g_board_config.matrix_width < 64) {
        setupFont(0);
        matrix->setCursor(0, 0);
        matrix->printf("%d:%d", g_timeinfo.tm_hour, g_timeinfo.tm_min);
        matrix->show();
      } else {
        matrix->setCursor(1 + x_pos_offset, 4);
        matrix->setTextColor(matrix->Color(15, 15, 15));
        matrix->printf("%02d:%02d", g_timeinfo.tm_hour, g_timeinfo.tm_min);
        matrix->show();
      }
      last_min = g_timeinfo.tm_min;
    } 
}

/*  Rotate Save matrix Landscape */
void LedPannel::saveMatrixRotationLandscape(char* data) {
  g_rotationValue = ROTATION_VALUE_LANDSCAPE;
  
  prefsBoardInfoArray[BOARD_INFO_IDX_ROTATION] = ROTATION_VALUE_LANDSCAPE;
  // writeFile(LittleFS, "/boardInfo.txt", buffer, 1);
  prefs.begin(PREFS_NAME);
  prefs.putBytes(PREFS_KEY_BOARD_INFO_PAGE, prefsBoardInfoArray, BOARD_INFO_PREFS_IDX_TOTAL_COUNT);
  prefs.end();
  allPageDelete();

  matrixClear();
  pBufferSerial->transmit_data(SET_ROTATION_LANDSCAPE);

  g_rotation_start_pos = 0;
  canvas16->setRotation(g_rotationValue);
  matrix->canvas_rotation = g_rotationValue;
}

/*  Rotate Save matrix Portrait */
void LedPannel::saveMatrixRotationPortrait(char* data) {
  g_rotationValue = ROTATION_VALUE_PORTRAIT;

  prefsBoardInfoArray[BOARD_INFO_IDX_ROTATION] = ROTATION_VALUE_PORTRAIT;
  prefs.begin(PREFS_NAME);
  prefs.putBytes(PREFS_KEY_BOARD_INFO_PAGE, prefsBoardInfoArray, BOARD_INFO_PREFS_IDX_TOTAL_COUNT);
  prefs.end();  
  
  allPageDelete();

  matrixClear();
  pBufferSerial->transmit_data(SET_ROTATION_PORTRAIT);


  g_rotation_start_pos = g_board_config.matrix_width - g_canvasWidth;
  canvas16->setRotation(g_rotationValue);
  matrix->canvas_rotation = g_rotationValue;
}

void LedPannel::resetPage() {
  PRINTLN("resetPage");
  matrixClear();

  allPageDelete();
}

/*  Board Info Transmit (Rotation Value , Board Visrion Info  */
void LedPannel::get_board_info(char* data) {
  int _send_idx = 0;
  const int bufferSize = 18 + bt_name.length() + 1 + 1;  // 1 is bt_name length packet, 1 is gamma enable value 
  byte sendBuffer[bufferSize] = {0};
  matrix->clear(); //  배터리 검사하기 위하여 사용중인 LED Clear
  matrix->show();
  delay(100);
  int adcValue = analogRead(BATTERY_PIN) * REGISTER_DIST; 
  int batteryPercent = (adcValue - 2800) / 13;
  if (batteryPercent <= 0)  batteryPercent = 0;
  
  int sendDivLevel;
  if (ledDivLevel <= 0.10 ) sendDivLevel = 1;
  else sendDivLevel = ledDivLevel * 10;
  
  sendBuffer[0] = 1;                //  0~4 UseMode
  for (int i = 1; i <5; i++) {
    if (useModeList[i] != 0)  sendBuffer[useModeList[i]] = 1;
  }
  sendBuffer[5] = batteryPercent;   //  5 battery 
  sendBuffer[6] = startModeNum;     //  6 start mode
  sendBuffer[7] = ledDivLevel * 10; //  7 led level
  sendBuffer[8] = isBatteryModeInfo;
  sendBuffer[9] = FIRM_VERSION_MSB_DIGIT;
  sendBuffer[10] = FIRM_VERSION_MID_DIGIT;
  sendBuffer[11] = FIRM_VERSION_LSB_DIGIT;

  sendBuffer[12] = ((logoTime & 0xff000000) >> 24);
  sendBuffer[13] = ((logoTime & 0x00ff0000) >> 16);
  sendBuffer[14] = ((logoTime & 0x0000ff00) >> 8);
  sendBuffer[15] = (logoTime & 0x000000ff);
  sendBuffer[16] = logoEnable;
  sendBuffer[17] = prefsBoardInfoArray[BOARD_INFO_IDX_LOGO_REPEAT_ON_OFF];
  sendBuffer[18] = bt_name.length();
  _send_idx = 19;
  for (int i = 0; i < bt_name.length(); i++) {
    sendBuffer[_send_idx++] = bt_name.charAt(i);
  }
  //sendBuffer[_send_idx++] = matrix->gamma_enable;
  sendBuffer[_send_idx++] = 1;

  pBufferSerial->transmit_data(GET_BOARD_INFO, (char*) sendBuffer, bufferSize);

  if (m_action_flag == true || m_action_flag == true || m_action_flag == true || m_gif_action_Flag == true) return;

  matrix->drawRGBBitmap(0 , 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
}
// GET_PAGE_SETTING_INFO
void LedPannel::get_page_setting_info(char* data) {
  // PRINTLN("get_page_setting_info");

 // displayPageTime 4bytes, page change mode 1byte, pageTime (page count * 5bytes), gage count 1byte
  const int bufferSize = 4 + 1 + (PAGE_TOTAL_COUNT * 5) + 1; 
  byte sendBuffer[bufferSize] = {0};

  sendBuffer[0] = ((displayPageTime & 0xff000000) >> 24);
  sendBuffer[1] = ((displayPageTime & 0x00ff0000) >> 16);
  sendBuffer[2] = ((displayPageTime & 0x0000ff00) >> 8);
  sendBuffer[3] = (displayPageTime & 0x000000ff);
  sendBuffer[4] = autoNextPageMode;

  for (int i = 0; i < PAGE_TOTAL_COUNT; i++) {  // 5 == start idx
    sendBuffer[5 + (i * 5) + 0] = timeSwitchIsEnable[i];
    sendBuffer[5 + (i * 5) + 1] = timeSwitchStartHour[i];
    sendBuffer[5 + (i * 5) + 2] = timeSwitchStartMinute[i];
    sendBuffer[5 + (i * 5) + 3] = timeSwitchFromHour[i];
    sendBuffer[5 + (i * 5) + 4] = timeSwitchFromMinute[i];
  }
  //sendBuffer[4 + 1 + (PAGE_TOTAL_COUNT * 5)] = g_gage_count;    //  막대 Count 
  sendBuffer[4 + 1 + (PAGE_TOTAL_COUNT * 5)] = 0;    //  막대 Count 

  pBufferSerial->transmit_data(GET_PAGE_SETTING_INFO, (char*) sendBuffer, bufferSize);
}


/**************************************************************************/
/** 
 * @fn      - set_init_board(char* data)
 * @brief   - 현재 시간 앱에서 전송받고 펌웨어 데이터 정보 전달
 * @details - 로테이션 val, 타이머 스위치 on/off, 버전, 가로, 세로, 보드타입 종류, 현재 모드 값 Response
 * @param data  - 현재 시각 전송 받음.
*/
/**************************************************************************/

void LedPannel::set_init_board(char* data) {
//  Serial.printf("set_init_board \n");
  /*  send ( rotation value, time enable, ver info )  */

  /**
   * packetSize : rotation value(1byte) + page timer Enable value (page count* 1bytes) + ver (3bytes)  + Width(2byte) + hegiht(2byte) + 보드 타입 종류(1byte) + 현재 모드 Num (1byte)
   **/
  int packetSize = 1 + PAGE_TOTAL_COUNT + 3 + 2 + 2 + 1 + 1;
  int _idx = 0;
  byte sendBuffer[packetSize] = {0};
  sendBuffer[_idx++] = g_rotationValue;
  sendBuffer[_idx++] = FIRM_VERSION_MSB_DIGIT;
  sendBuffer[_idx++] = FIRM_VERSION_MID_DIGIT;
  sendBuffer[_idx++] = FIRM_VERSION_LSB_DIGIT;
  sendBuffer[_idx++] = g_board_config.matrix_width >> 8;  
  sendBuffer[_idx++] = g_board_config.matrix_width;  
  sendBuffer[_idx++] = g_board_config.matrix_height >> 8;   
  sendBuffer[_idx++] = g_board_config.matrix_height;   
  sendBuffer[_idx++] = g_board_config.model_info;
  sendBuffer[_idx++] = getModeNum(modeNum);
  for (int i = 0; i < PAGE_TOTAL_COUNT; i++) sendBuffer[_idx++] = timeSwitchIsEnable[i];

  /* cur time recive */
  uint16_t s_yy = s_yy | data[0] << 8;
  s_yy = s_yy | data[1];
  uint8_t s_MM = data[2];
  uint8_t s_dd = data[3];
  uint8_t s_hh = data[4];
  uint8_t s_mm = data[5];
  uint8_t s_ss = data[6];

  set_local_time(s_yy, s_MM, s_dd, s_hh, s_mm, s_ss);

  // PRINTF("s_yy : %d \n ", s_yy);
  // PRINTF("s_MM : %d \n ", s_MM);
  // PRINTF("s_dd : %d \n ", s_dd);
  // PRINTF("s_hh : %d \n ", s_hh);
  isTimeSynch = true;
  pBufferSerial->transmit_data(SET_INIT_BOARD, (char*) sendBuffer, packetSize);
    
  matrixClear();  //  BT 연결 시 화면 지움
}

/*  Board Battery Info Transmit   */
void LedPannel::get_battery_info(char* data) {
  byte sendBuffer[1];
  int adcValue = analogRead(BATTERY_PIN) * REGISTER_DIST; 
  byte batteryPercent = (adcValue - 2800) / 13;
  if (batteryPercent <= 0)  batteryPercent = 0;
  sendBuffer[0] = batteryPercent;

  pBufferSerial->transmit_data(GET_BATERY_INFO, (char*) sendBuffer, 1);
}

/*  Matrix Clear, Canvas16 Clear Buffer   */
void LedPannel::matrixClear() {
  if(canvas16 != NULL)  canvas16->fillScreen(0); 
  matrix->clear();
  matrix->show();
  set_action_off();
}

/*  캔버스 내용 출력 (메인 매트릭스)   */
void LedPannel::mainMatrixPrint() {
  int _temp_border_count = matrix->border_count; //  temp border count value
  if (_temp_border_count == EFFECT_BORDER_NO_LINE_COUNT) matrix->clear();  //  border 없는 경우 전체 지움
  else {  //   //  border 있는 경우 border line 제외 지움
    for (int x = _temp_border_count ; x < g_board_config.matrix_width - _temp_border_count; x++) {
      for (int y = _temp_border_count; y < g_board_config.matrix_height - _temp_border_count; y++) {  
        matrix->drawPixel(x, y, MATRIX_CLEAR_COLOR_VALUE);
      }
    }
  }
  matrix->drawRGBBitmap(m_xPos , m_yPos, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
  matrix->show();
}

float getLedDivValue(uint16_t brightLevel) {
  uint16_t value = 3; //  3 is defalut bright
  switch (brightLevel)
  {
  case 0:
    value = 0;
    break;
  case 1:
    value = 4;
    break;
  case 2:
    value = 6;
    break;
  case 3:
    value = 8;
    break;
  case 4:
    value = 10;
    break;
  default:  //  기본 밝기 10
    value = 10;
    break;
  }

  if (value == 1) return (float)value /100; //  최하 밝기일 경우 모든 RGB 값 1
  else return (float)value /10;
}

void LedPannel::getDisplayPagePixel(char* data) {
  File file;
  uint8_t pageNum = data[0];
  char pageStr[15];
  int bufIdx = 0;
  int bufSize = 0;
  int sendIdx = 0;
  bool sendEndFlag = false;

  sprintf(pageStr, "/page%d.txt", pageNum+1); //현재 0페이지로 들어오게 되어있어서 0 + 1 연산

  file = LittleFS.open(pageStr);
  bufSize = file.size();
  char * sendBuffer = NULL;
  if (bufSize != 0) {
   sendBuffer = (char*) malloc (bufSize);
    while(file.available()){
      sendBuffer[bufIdx++] = file.read();
    }
  }
  char startSendBuffer[2];
  startSendBuffer[0] = ((bufSize & 0x0000ff00) >> 8);
  startSendBuffer[1] = (bufSize & 0x000000ff);

  pBufferSerial->transmit_data(GET_DISPLAY_PAGE_PIXEL_START, (char*) startSendBuffer, 2);
  
  // 텀을 주기 위해 시간 초기 세팅
  pre_time_action = millis();

  while (1) {   // Max Count 분할해서 전송 
    if (delayExt(millis(), &pre_time_action, 1000)){ 
      if (sendEndFlag == true) break;
      if (bufIdx > BT_SEND_MAX_COUNT) {
          // PRINTF("bt _ dive send \n");
          pBufferSerial->transmit_data(GET_DISPLAY_PAGE_PIXEL, (char*) &sendBuffer[sendIdx], BT_SEND_MAX_COUNT);
          sendIdx += BT_SEND_MAX_COUNT;
          bufIdx -= BT_SEND_MAX_COUNT;
      } else {
        // PRINTF("bt _ last send \n");
        pBufferSerial->transmit_data(GET_DISPLAY_PAGE_PIXEL, (char*) &sendBuffer[sendIdx], bufIdx);
        sendEndFlag = true;
      }
    }
  }
  file.close();
  free(sendBuffer);
  pBufferSerial->transmit_data(GET_DISPLAY_PAGE_PIXEL_END, (char*) &sendBuffer[sendIdx], bufIdx);
}


/*  현재 전광판에 출력되고 있는 내용 가져오기 */
/*
void LedPannel::getMatrixPixel(char* data) {
  int bufferIdx = 0;
  uint8_t* picture_buffer = (uint8_t*) calloc (TOTAL_X_Y_R_G_B_BITMAP_SIZE, sizeof(uint8_t*));
  int sendIdx = 0;
  int16_t x = 0, y = 0;
  int16_t w = g_board_config.matrix_width;
  int16_t h = g_board_config.matrix_height;
  
  for(int16_t j=0; j<h; j++, y++) {
    for(int16_t i=0; i<w; i++ ) {
      if (matrix->matrixbuff[j * w + i] != 0) 
      {
        picture_buffer[bufferIdx++] = i;
        picture_buffer[bufferIdx++] = y;
        picture_buffer[bufferIdx++] = (uint8_t)(matrix->matrixbuff[j * w + i] & 0x1f);
        picture_buffer[bufferIdx++] = (uint8_t)((matrix->matrixbuff[j * w + i] >> 5) & 0x1f);
        picture_buffer[bufferIdx++] = (uint8_t)((matrix->matrixbuff[j * w + i] >> 10) & 0x1f);   
      }
    }
  }
  unsigned long pre_time = 0;
  while (1) {   // Max Count 분할해서 전송 
    if (delayExt(millis(), &pre_time_action, 1000)){ 
      if (bufferIdx > BT_SEND_MAX_COUNT) {
          // PRINTF("bt _ dive send \n");
          pBufferSerial->transmit_data(GET_MATRIX_PIXEL_ALL_PAGE, (char*) &picture_buffer[sendIdx], BT_SEND_MAX_COUNT);
          sendIdx += BT_SEND_MAX_COUNT;
          bufferIdx -= BT_SEND_MAX_COUNT;
      } else {
        // PRINTF("bt _ last send \n");
        pBufferSerial->transmit_data(GET_MATRIX_PIXEL_ALL_PAGE, (char*) &picture_buffer[sendIdx], bufferIdx);
        break;
      }
    }
  }
  free(picture_buffer);
}
*/

void LedPannel::effect_border_blink() {
  if (delayExt(millis(), &pre_time_effect_border, m_effect_time)) m_is_border_blink = !m_is_border_blink;
  else return;
  if (m_is_border_blink == true) {
      matrix->border_count = 0;  // border draw start
      matrix->writeFastHLine(0, 0, g_board_config.matrix_width, 0);
      matrix->writeFastHLine(0,g_board_config.matrix_height - 1, g_board_config.matrix_width, 0);
      matrix->writeFastVLine(0, 0, g_board_config.matrix_height, 0);
      matrix->writeFastVLine(g_board_config.matrix_width -1, 0, g_board_config.matrix_height, 0);   
      matrix->border_count = EFFECT_BORDER_ONE_LINE_COUNT; // border draw end
      matrix->show();
  }
  else {
      matrix->border_count = 0;   // border draw start
      matrix->writeFastHLine(0, 0, g_board_config.matrix_width, m_color_border);
      matrix->writeFastHLine(0,g_board_config.matrix_height - 1, g_board_config.matrix_width, m_color_border);
      matrix->writeFastVLine(0, 0, g_board_config.matrix_height, m_color_border);
      matrix->writeFastVLine(g_board_config.matrix_width - 1, 0, g_board_config.matrix_height, m_color_border);
      matrix->border_count = EFFECT_BORDER_ONE_LINE_COUNT; // border draw end
      matrix->show();
  }
}

void LedPannel::effect_color_wheel() {
  // PRINTLN("effect_color_wheel");
  Serial.printf("%s\n", __FUNCTION__);
  if (delayExt(millis(), &pre_time_effect_color_wheel, 80)) {
    matrix->hue_color += ((m_effect_color_change_value) % 255);

    if (m_actionCmd == CMD_ACT_DEFALUT) {
      if (g_rotationValue == ROTATION_VALUE_LANDSCAPE)  matrix->drawRGBBitmap(0, 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
      else if (g_rotationValue == ROTATION_VALUE_PORTRAIT) matrix->drawRGBBitmap(g_board_config.matrix_width - g_canvasWidth , 0, canvas16->getBuffer(), g_canvasWidth, g_canvasHeight);
      matrix->show();
    }
  } else return;
}

/**
 * @brief star effect drawing 
 * 
 */
void LedPannel::effect_rain_star() {
  bool colorRevers = true;  //  별 컬러 반전
  uint16_t starColor;
  uint16_t startCount = 0;

  if (delayExt(millis(), &pre_time_effect_rain_star, m_effect_rain_star_time)) {  //  출력 주기가 된 경우 effect draw 
    /* Main Matrix Pirnt !  */
    mainMatrixPrint();  // main Matrix 불러 온 뒤 Star Effect Draw 
    int drawingWidth = 0; 
    
    while (1) { //  연동된 패널 갯수 만큼 Star 출력
      for(int i= 0; i < STAR_COUNT; i++) {
        // Draw 'ball'  
        // Update X, Y position
        starColor = (colorRevers) ? matrix->Color(255,255,0) : matrix->Color(255,255,255);
        // clearStar(ledEffect->m_star[i][0], ledEffect->m_star[i][1], starColor);

        // Bounce off edges
        if((ledEffect->m_star[i][STAR_Y_POS_INDEX] >= (matrix->height())))  //  star y pos 최하로 내려가면 위로 이동
          ledEffect->m_star[i][STAR_Y_POS_INDEX] = 0;

        printStar(ledEffect->m_star[i][STAR_X_POS_INDEX] + drawingWidth, ledEffect->m_star[i][STAR_Y_POS_INDEX], starColor); 
        colorRevers = !colorRevers; //  2가지 color revers
      }
    
      //  panel 길이까지 모두 그렸다면 반복문 탈출
      if ((drawingWidth + 18) > g_board_config.matrix_width) break; //  18 == defalut width
      drawingWidth += 30; //  30 == 한 star effect 사이클 당 width 30
    }
    
    /*  X Pos 변경 += -1 ~ +1 */
    ledEffect->m_star[0][STAR_X_POS_INDEX] += ledEffect->m_star_x_sign;
    ledEffect->m_star[1][STAR_X_POS_INDEX] += ledEffect->m_star_x_sign;
    ledEffect->m_star[2][STAR_X_POS_INDEX] += ledEffect->m_star_x_sign;
    ledEffect->m_star[3][STAR_X_POS_INDEX] += ledEffect->m_star_x_sign;
    /*  Y pos 변경 += 1 */
    ledEffect->m_star[0][STAR_Y_POS_INDEX] += 1;
    ledEffect->m_star[1][STAR_Y_POS_INDEX] += 1;
    ledEffect->m_star[2][STAR_Y_POS_INDEX] += 1;
    ledEffect->m_star[3][STAR_Y_POS_INDEX] += 1;

    if (ledEffect->m_star_x_move >= 2) ledEffect->m_star_x_sign = -1; //  star x pos 해당 위치에서 -1~ 1 이동
    else if (ledEffect->m_star_x_move <= 0) ledEffect->m_star_x_sign = 1;
  
    ledEffect->m_star_x_move += ledEffect->m_star_x_sign;
    matrix->show();
  } 
}

/**
 * @brief setting effect - effect value에 따라 사용 할 이펙트 setup
 * 
 * @param _effect_value - effect bitmask value 
 */
void LedPannel::set_effect(uint8_t _effect_value) {
  // PRINTF("(_effect_value & EFFECT_BORDER_BLINK_BIT_MASK) : %d \n", (_effect_value & EFFECT_BORDER_BLINK_BIT_MASK));
  // PRINTF("(_effect_value & EFFECT_COLOR_WHEEL_BIT_MASK) : %d \n", (_effect_value & EFFECT_COLOR_WHEEL_BIT_MASK));
  init_effect_flag();

  if (!(_effect_value & EFFECT_BORDER_BLINK_BIT_MASK) == 0) {
    // PRINTF("border true \n");
    m_effect_border_blink = true;
    matrix->border_count = EFFECT_BORDER_ONE_LINE_COUNT;
  } 
  if (!(_effect_value & EFFECT_COLOR_WHEEL_BIT_MASK) == 0) {
    // PRINTF("chlor wheel true \n");
    m_effect_color_wheel = true;
    matrix->is_color_wheel = true;
  } 
  if (!(_effect_value & EFFECT_RAIN_STAR_BIT_MASK) == 0) {
    // PRINTF("chlor wheel true \n");
    m_effect_rain_star = true;
  }
  if (!(_effect_value & EFFECT_RAINBOW_BACKGROUND) == 0) {
    // PRINTF("chlor wheel true \n");
    m_effect_bg_rainbow = true;
  } 
  if (!(_effect_value & EFFECT_BORDER_RGB) == 0) {
    // PRINTF("chlor wheel true \n");
    m_effect_border_rgb = true;
    matrix->border_count = EFFECT_BORDER_TWO_LINE_COUNT;
  } 
  if (!(_effect_value & EFFECT_CLEAR_CRAB) == 0) {
    // PRINTF("chlor wheel true \n");
    m_effect_clear_crab = true;
  } 
}

void LedPannel::init_effect_flag() {
  m_effect_border_blink = false;
  m_effect_color_wheel = false;
  matrix->is_color_wheel = false;
  m_effect_rain_star = false;
  m_effect_bg_rainbow = false;
  m_effect_border_rgb = false;
  m_effect_clear_crab = false;
  matrix->border_count = EFFECT_BORDER_NO_LINE_COUNT;
}

void LedPannel::init_function_flag() {
  m_matrix_function_clock = false;
}

void LedPannel::printStar(uint16_t x, uint16_t y, uint16_t color) { //  임시__ drawpixel color check -> drawpixel (덮어쓰지 않는 drawpixel 개발 필요)
  drawPixelColorCheck(x, y, color, OVER_WRITE_DRAW_FLAG); //매트릭스로 바로 출력
  drawPixelColorCheck(x -1, y -1, color, OVER_WRITE_DRAW_FLAG); //매트릭스로 바로 출력
  drawPixelColorCheck(x +1, y -1, color, OVER_WRITE_DRAW_FLAG); //매트릭스로 바로 출력
  drawPixelColorCheck(x -1, y +1, color, OVER_WRITE_DRAW_FLAG); //매트릭스로 바로 출력
  drawPixelColorCheck(x +1, y +1, color, OVER_WRITE_DRAW_FLAG); //매트릭스로 바로 출력
}

void LedPannel::clearStar(uint16_t x, uint16_t y, uint16_t color) {
  drawPixelColorCheck(x, y, color, OVER_WRITE_CLEAR_FLAG); //매트릭스로 바로 출력
  drawPixelColorCheck(x -1, y -1, color, OVER_WRITE_CLEAR_FLAG); //매트릭스로 바로 출력
  drawPixelColorCheck(x +1, y -1, color, OVER_WRITE_CLEAR_FLAG); //매트릭스로 바로 출력
  drawPixelColorCheck(x -1, y +1, color, OVER_WRITE_CLEAR_FLAG); //매트릭스로 바로 출력
  drawPixelColorCheck(x +1, y +1, color, OVER_WRITE_CLEAR_FLAG); //매트릭스로 바로 출력
}

void LedPannel::printBGRainbowEffect() {
  /* Main Matrix Pirnt !  */
  if (delayExt(millis(), &pre_time_effect_border, 70)) {
    mainMatrixPrint();

    int           x1, x2, x3, x4, y1, y2, y3, y4, sx1, sx2, sx3, sx4;
    unsigned char x, y;
    long          value;

    sx1 = (int)(cos(angle1) * radius1 + centerx1);
    sx2 = (int)(cos(angle2) * radius2 + centerx2);
    sx3 = (int)(cos(angle3) * radius3 + centerx3);
    sx4 = (int)(cos(angle4) * radius4 + centerx4);
    y1  = (int)(sin(angle1) * radius1 + centery1);
    y2  = (int)(sin(angle2) * radius2 + centery2);
    y3  = (int)(sin(angle3) * radius3 + centery3);
    y4  = (int)(sin(angle4) * radius4 + centery4);
    int yLineCnt = 0;
    for(y=3; y < g_board_config.matrix_height; y++, yLineCnt++) {
      if (yLineCnt >= 2) {
        y+=6;
        yLineCnt = 0;
      }     
      x1 = sx1; x2 = sx2; x3 = sx3; x4 = sx4;
      for(x=0; x < g_board_config.matrix_width; x++) {
        value = hueShift
          + (int8_t)pgm_read_byte(sinetab + (uint8_t)((x1 * x1 + y1 * y1) >> 4))
          + (int8_t)pgm_read_byte(sinetab + (uint8_t)((x2 * x2 + y2 * y2) >> 4))
          + (int8_t)pgm_read_byte(sinetab + (uint8_t)((x3 * x3 + y3 * y3) >> 5))
          + (int8_t)pgm_read_byte(sinetab + (uint8_t)((x4 * x4 + y4 * y4) >> 5));
          drawPixelColorCheck(x, y, matrix->ColorHSV(value * 3, 255, 255),OVER_WRITE_DRAW_FLAG);  //  임시 drawcolorcheck
        x1--; x2--; x3--; x4--;
      }
      y1--; y2--; y3--; y4--;
    }
    angle1   += 0.03;
    angle2   -= 0.07;
    angle3   += 0.13;
    angle4   -= 0.15;
    hueShift += 2;
    
    uint32_t t;
    while(((t = millis()) - prevTime) < (1000 / FPS));
    prevTime = t;
  }
}

void LedPannel::effect_border_rgb() {
  uint16_t _color = 0;
  // start Point + x (규칙에 따른 계산)
  // 해당 좌표 지우기
  if (delayExt(millis(), &pre_time_effect_border, 150)) {
    for (int _x = borderAddPos * 2; _x < g_board_config.matrix_width; _x+=6) { //  상단 가로 
      matrix->drawPixel(_x, 0, _color);
      matrix->drawPixel(_x + 1, 0, _color);
      matrix->drawPixel(_x, 1, _color);
      matrix->drawPixel(_x + 1, 1, _color);
    }
    for (int _x = borderAddPos * 2 + 2 ; _x < g_board_config.matrix_width + 2; _x+=6) {  //  하단 가로
      matrix->drawPixel(g_board_config.matrix_width - (_x), g_board_config.matrix_height - 1, _color);
      matrix->drawPixel(g_board_config.matrix_width - (_x- 1), g_board_config.matrix_height - 1, _color);
      matrix->drawPixel(g_board_config.matrix_width - (_x ), g_board_config.matrix_height - 2, _color);
      matrix->drawPixel(g_board_config.matrix_width - (_x - 1), g_board_config.matrix_height - 2, _color);
    }
    for (int _y = (borderAddPos * 2) + 2; _y < g_board_config.matrix_height ; _y+=6) { //  좌측 세로
      matrix->drawPixel(0, g_board_config.matrix_height  - _y - 1  , _color);
      matrix->drawPixel(0, g_board_config.matrix_height  - _y - 2, _color);
      matrix->drawPixel(1, g_board_config.matrix_height  - _y - 1, _color);
      matrix->drawPixel(1, g_board_config.matrix_height  - _y - 2, _color);
    }
    for (int _y = (borderAddPos * 2) + 2; _y < g_board_config.matrix_height ; _y+=6) { //  끝 세로
      matrix->drawPixel(g_board_config.matrix_width - 1, _y , _color);
      matrix->drawPixel(g_board_config.matrix_width - 1, _y + 1, _color);
      matrix->drawPixel(g_board_config.matrix_width - 2, _y , _color);
      matrix->drawPixel(g_board_config.matrix_width - 2, _y + 1, _color);
    }
    
    borderAddPos = ++borderAddPos  % 3;
    switch (borderAddPos)
    {
    case 0:
        _color = matrix->Color(255,0,0);
      break;
    case 1:
        _color = matrix->Color(0,255,0);
      break;
    case 2:
        _color = matrix->Color(0,0,255);
      break;  
    // case 3:
    //     _color = matrix->Color(15,15,15);
    //   break;  
    // case 4:
    //     _color = matrix->Color(15,15,0);
    //   break;
    // case 5:
    //     _color = matrix->Color(0,15,15);
    //   break;
    // case 6:
    //     _color = matrix->Color(15,0,15);
    //   break;  
    // case 7:
    //     _color = matrix->Color(15,15,15);
    //   break;  
    }

  // 해당 좌표 그리기
    for (int _x = borderAddPos * 2; _x < g_board_config.matrix_width; _x+=6) {//  상단 가로 
      matrix->drawPixel(_x, 0, _color);
      matrix->drawPixel(_x + 1, 0, _color);
      matrix->drawPixel(_x, 1, _color);
      matrix->drawPixel(_x + 1, 1, _color);
    }
    for (int _x = borderAddPos * 2 + 2 ; _x < g_board_config.matrix_width + 2; _x+=6) { //  하단 가로
      matrix->drawPixel(g_board_config.matrix_width - (_x), g_board_config.matrix_height - 1, _color);
      matrix->drawPixel(g_board_config.matrix_width - (_x- 1), g_board_config.matrix_height - 1, _color);
      matrix->drawPixel(g_board_config.matrix_width - (_x ), g_board_config.matrix_height - 2, _color);
      matrix->drawPixel(g_board_config.matrix_width - (_x - 1), g_board_config.matrix_height - 2, _color);
    }
    for (int _y = (borderAddPos * 2) + 2; _y < g_board_config.matrix_height ; _y+=6) { //  좌측 세로
      matrix->drawPixel(0, g_board_config.matrix_height  - _y - 1  , _color);
      matrix->drawPixel(0, g_board_config.matrix_height  - _y - 2, _color);
      matrix->drawPixel(1, g_board_config.matrix_height  - _y - 1, _color);
      matrix->drawPixel(1, g_board_config.matrix_height  - _y - 2, _color);
    }
    for (int _y = (borderAddPos * 2) + 2; _y < g_board_config.matrix_height ; _y+=6) { //  끝 세로
      matrix->drawPixel(g_board_config.matrix_width - 1, _y , _color);
      matrix->drawPixel(g_board_config.matrix_width - 1, _y + 1, _color);
      matrix->drawPixel(g_board_config.matrix_width - 2, _y , _color);
      matrix->drawPixel(g_board_config.matrix_width - 2, _y + 1, _color);
    }
    matrix->show();
  }
}

void LedPannel::effect_clear_crab() { // 추후에 구현
  // matrix->drawRGBBitmapClearEx(m_xPos, m_yPos, (const uint16_t *)bitmap_crab32x32, 32, 32);
  // matrix->drawRGBBitmap(m_xPos, m_yPos, (const uint16_t *)bitmap_crab32x32, 32, 32);
}


/**
 * @brief 사용중인 LED에 덮어쓰지 않기 위한 draw pixel
 * 
 * @param x x pos
 * @param y y pos
 * @param color color 565 value 
 * @param flag  flag 0 = draw, flag 1 = clear
 */

// 수정 필요
void LedPannel::drawPixelColorCheck(int16_t x, int16_t y, uint16_t color, uint8_t flag) {

  if (x < 0 || x >= g_board_config.matrix_width || y < 0  || y >= g_board_config.matrix_height ) return;
  int16_t idx = x + y * g_board_config.matrix_width;

  if (matrix->getPixelColor(idx) == 0 && flag == 0) { //  draw  --> 해당 좌표 pixel color value 가 0(검은색) 인 경우에만 표시
    matrix->drawPixel(x, y, color);
  } else if (matrix->getPixelColor(idx) == color && flag == 1) {  //  clear -- > 해당 좌표 와 값 같은 경우 clear
    matrix->drawPixel(x, y, 0);
  }
}



/**************************************************************************/
/** 
 * @fn      - canvasMemoryAllocate()
 * @brief   - 캔버스 크기 재할당 함수
 * @details - 캔버스 크기가 다르면 크기에 맞게 재할당
*/
/**************************************************************************/
void canvasMemoryAllocate(uint16_t _width, uint16_t _height) {
  if (g_canvasWidth != _width || g_canvasHeight != _height) {
    if (canvas16 != NULL) {
      delete canvas16;
      canvas16 = NULL;
    }
    canvas16 = new GFXcanvas16(_width, _height); 
    
    g_canvasWidth = _width;
    g_canvasHeight = _height;
  }
  
  /*  로테이션 세팅 */
  if (g_rotationValue == ROTATION_VALUE_LANDSCAPE) {
    g_rotation_start_pos = 0;
  } else if (g_rotationValue == ROTATION_VALUE_PORTRAIT) {
    g_rotation_start_pos = (g_board_config.matrix_width - g_canvasWidth);
  }
  canvas16->setRotation(g_rotationValue);
}
