#include <FS.h>
#include "../../popsign.h"
#include "../led_matrix/ledpannel.h"
#include "../led_matrix/display_page.h"
#include "../utils/etc.h"
#include "../communication/command.h"
#include "../flash/flash.h"
#include "../img/bitmap_mode_logo.h"
#include "../board/board.h"

//  Board Config Info 
extern Board_Config g_board_config;

/*  디스플레이 모드 관련 변수  */
extern char* pageBuffer[PAGE_TOTAL_COUNT];
extern size_t pageSize[PAGE_TOTAL_COUNT];
extern int8_t g_pageCount;
extern bool nextPageToken;
extern bool autoNextPageMode;
extern bool categoryActionFlag;
extern bool timeSwitchIsEnable[PAGE_TOTAL_COUNT];
extern int displayPageTime;
extern int displayPageIntervalTime;
extern bool isSinglePagePrint;  // 단일 페이지일 경우 깜빡거리지 않게 하기 위해 설정
extern uint8_t display_step;  //  디스플레이 모드 출력 스텝
extern int int_flg;
extern uint8_t pageEnableList[PAGE_TOTAL_COUNT];  // 페이지 Enable List --> 페이지 사용가능하는지 가지고 있는 버퍼 (모든 페이지 접근 하는데에 시간이 오래걸려 해당 버퍼 사용)
unsigned long page_interval_pre_time; //  페이지 전환 시간 간격 저장 값




/*  Mode 관련 변수 */
extern int logoTime;


/* Wifi 모드 관련 변수 */
extern bool isStreamCallback;

//  Gage Mode 관련 변수 - 게이지 갯수
extern int g_gage_count;

/**
 * 디스플레이 모드 다음 페이지 검사 후 출력하는 함수
 * 센서 페이지 검사하여 표시
 * 경우 1 : 액션인 경우 -> 액션의 루틴의 1회가 끝난 후 nextPageToken 값에 따른 페이지 전환
 * 경우 2 : 디폴트 인경우 -> 지정된 시간에 따라 페이지 전환
 * 경우 3 : 수동 모드 인경우 -> 모드 스위치 전환으로 페이지 전환  
 * 
 **/
bool nextPagePrint(void* matrix, LedPannel* ledpannel) {
    for (int i = 0; i < (PAGE_TOTAL_COUNT); i++) {  //  -1 == 현재 페이지 제외한 페이지까지 반복 
      // 다음 페이지가 있을 때 까지 페이지를 넘김.
      g_pageCount++;
      // PRINTF("g_pageCount : %d \n", g_pageCount);
      if (g_pageCount >= PAGE_TOTAL_COUNT) {
        g_pageCount = 0; 
      }
      // if 페이지가 있다면
      if (pageSize[(g_pageCount)]) {    
          Serial.printf("%s, %d\n", __FUNCTION__, __LINE__);
          ledpannel->multiPagePrint(g_pageCount); 
          page_interval_pre_time = millis();
          return true;
      } 
    }

    return false;
}

/**
 * @fn checkTimerPage
 * @brief Timer 설정 된 페이지 Time 검사하여 페이지 출력
 */ 
bool checkTimerPagePrint(void* matrix, LedPannel* ledpannel) {
  int timer_switch_result;
  timer_switch_result = timer_switch_print_page();

  if (timer_switch_result == PAGE_TIME_ASYNC) {
    // if timer_switch_print_page() 반환 값이 시간 동기화가 되어있지 않는 경우 아래 메시지 출력
    ledpannel->print_str_matrix_basic(FONT_ONE_SIZE, "시간 설정 오류\n블루투스 연결 필요", 0x02, ((MATRIX*)matrix)->Color(150,150,150));

    page_interval_pre_time = millis();
    return true;
  } else if (timer_switch_result == PAGE_TIME_OK) {
    // if timer_switch_print_page() 반환 값에 현재 시간이 페이지 출력 시간인 경우 정상 페이지 출력
    ledpannel->multiPagePrint(g_pageCount); 
    page_interval_pre_time = millis();
    return true;
  } else // if (timer_switch_result == PAGE_TIME_NO) 
  {
    return false;
  }
}

/**
 * 다음페이지까지 넘기기 위한 조건 검사
 * 경우 1 : 액션인 경우 -> 액션의 루틴의 1회가 끝난 후 nextPageToken 값에 따른 페이지 전환
 * 경우 2 : 디폴트 인경우 -> 지정된 시간에 따라 페이지 전환
 * 경우 3 : 수동 모드 인경우 -> 모드 스위치 전환으로 페이지 전환  
 * 
 * autoNextPageMode : 디스플레이 페이지 수동 변환 모드
 * nextPageToken : 액션 루틴 끝난 후 페이지 토큰 true 반환
 **/
bool checkNextPageCondition(int _displayPageTime, LedPannel* ledpannel) {
  bool _next_page_ok = false;

  if (autoNextPageMode) { // 자동 페이지 전환 모드
    if (categoryActionFlag == true ) {  //  페이지 내용이 액션이라면 nextPageToken == true 다음 페이지 전환(한 번의 액션이 끝났을 때 true 반환하는 함수)
      if (nextPageToken == true) {
        _next_page_ok = true;
      } else {
        _next_page_ok = false;
      }
    } else {  //  페이지 내용이 액션이 아니라면 시간에 따라 페이지 전환
      if (delayExt(millis(), &page_interval_pre_time, _displayPageTime)) {
        _next_page_ok = true;
      } else _next_page_ok = false;
    }
  } else {  //  수동 페이지 전환 모드
    if (int_flg == PAGE_CHANGE) { 
      int_flg = 0;
      _next_page_ok = true;
    } else _next_page_ok = false;
  }

  return _next_page_ok; //  다음 페이지 전환 여부 return;
}
/**************************************************************************/
/** 
 * @brief   - 페이지 로드 함수 (플래시 -> SRAM)
 * @details - page1~5 로드 -> pageBuffer[0~4]
*/
/**************************************************************************/
int init_multiPage() {
  File pageFile;    //  페이지 파일
  int pageTotalCount = 0;
  char pageStr[30];

  for(int i = 0; i < PAGE_TOTAL_COUNT; i++) {
    if (pageEnableList[i] == FLASH_PAGE_ENABLE_VALUE) {

      sprintf(pageStr, "/page%d.txt", i + 1); //현재 0페이지로 들어오게 되어있어서 0 + 1 연산
      pageFile = LittleFS.open(pageStr);
      pageSize[i] = pageFile.size();

      //  Page1~20 Flash Byte Read
      if(pageSize[i]){ 
        pageTotalCount++;
      }
      pageFile.close();
    } else {
      pageSize[i] = 0;
    }
  }
  return pageTotalCount;
}

/**************************************************************************/
/** 
 * @brief   - 페이지 버퍼 배열 메모리 초기화 함수
 * @details - pageBuffer[0~4] = null
*/
/**************************************************************************/
void free_multiPage()
{
  for (int i = 0; i < PAGE_TOTAL_COUNT; i++) {
    pageSize[i] = 0;
    if (pageBuffer[i] != NULL) { 
      free(pageBuffer[i]);
      pageBuffer[i] = NULL;
    }

  }
}

/**************************************************************************/
/** 
 * @fn  - play_display()
 * @brief   - 디스플레이 모드 출력
 * @details - display_step == 0 -> 페이지 초기화 및 첫 페이지출력
 *            display_step == 1 -> 다음 페이지 조건 검사 및 페이지 출력
 *            display_step == 2 -> 단일 페이지 조건 검사 및 페이지 출력
*/
/**************************************************************************/
void play_display(void* matrix, LedPannel* ledpannel) {
  // PRINTF("display_step : %d \n", display_step);
  if (display_step == 0) 
  {
    
    char pageStr[10];
    int pageTotalCount = 0;
    g_pageCount = -1; //  센서 감지 시 첫 페이지부터 실행하기 위하여 -1으로 이동

    // init_modeChange_flag();
    pageTotalCount = init_multiPage();  // 
    // PRINTF("pageTotalCount : %d \n", pageTotalCount);
    if (pageTotalCount == 0) {  //  페이지 없는 경우 페이지 없음 이모지 출력
      uint8_t x_pos_offset = (g_board_config.matrix_width - 64) / 2;
      display_step = 2;
      ((MATRIX*)matrix)->drawRGBBitmap(x_pos_offset, 0, (uint16_t*) non_page_64_32, 64, 32);
      return;
    } else if (pageTotalCount == 1) { //  페이지가 하나인 경우 모드스탭 3으로 이동
      nextPagePrint(((MATRIX*)matrix), ledpannel);
      display_step = 2;
      return;
    }
    display_step = 1;

    /*  첫 페이지 출력 후 검사  */
    nextPagePrint(((MATRIX*)matrix), ledpannel);
  }
  else if (display_step == 1) {
  // 페이지 조건에 따라 전환되며 출력하는 영역 
    ledpannel->command_excute(); 
    Serial.printf("%s, %d\n", __FUNCTION__, __LINE__);
    if (checkNextPageCondition(displayPageTime, ledpannel)) {
        categoryActionFlag = false;
        ledpannel->set_action_off(false);
        nextPageToken = false;
        page_interval_pre_time = millis();
        if (pageBuffer[g_pageCount] != NULL) {  //  다음 페이지로 전환 시 Page Data 해제
          free(pageBuffer[g_pageCount]);
          pageBuffer[g_pageCount] = NULL;
        }
    } else return;
  
    if (!nextPagePrint(((MATRIX*)matrix), ledpannel)) {
      // 마지막 페이지까지 page 출력이 없었다면 다음 페이지 조건 검사하지 않고 다음 바로 페이지 실행
      page_interval_pre_time = 0;
    }
  }
  else if (display_step == 2) {    
    
    // print pov display

    
    ledpannel->command_excute(); //  단일 페이지 or 페이지 없음
  }
}
