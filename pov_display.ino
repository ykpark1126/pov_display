/**
 * @file floorledsign.ino
 * @author jongju park(jongju0920@kakao.com)
 * @brief IDE Setting Read Me 참조 
 * @version 0.3.1
 * @date 2022-06-22
 * @copyright frontier (c) 2022
 * 
 */

#include <Adafruit_GFX.h>   // Core graphics library
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include <BluetoothSerial.h>
#include <FS.h>
#include <LittleFS.h>
#include <Preferences.h>
#include <BLEDevice.h>
#include <SPI.h>
#include "popsign.h"
#include "src/communication/bluetooth.h"
#include "src/communication/buffer_serial.h"
#include "src/board/board.h"
#include "src/board/config.h"
#include "src/flash/flash.h"
#include "src/led_matrix/ledpannel.h"
#include "src/led_matrix/display_page.h"
#include "src/utils/etc.h"
#include "src/utils/localtime.h"
#include "src/img/bitmap_mode_logo.h"


#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

/**
 * @brief Board Config Info 
 * @details member - model_info (보드 모델), matrix_width (매트릭스 넓이),
 *                   matrix_height(매트릭스 높이), flash_size (플래시 크기);
 */
Board_Config g_board_config(MODEL_INFO, IDE_MATRIX_WIDTH, IDE_MATRIX_HEIGHT, FLASH_MOMORY_SIZE);  //  Arudino IDE에서 설정한 내용 Defalut로 삽입.

/** 
 * @brief   Matrix - LED Pin Draw Matrix 
 *          * 모델별로 객체 달리 가져갈 수 있는 typedef 변수
 *          * P3RGB64x32MatrixPanel or GlassLed 객체
*/
//MATRIX *matrix;
//Adafruit_NeoMatrix *matrix;
#define PIN 5

MATRIX *matrix = new Adafruit_NeoMatrix(IDE_MATRIX_WIDTH, IDE_MATRIX_HEIGHT, 3, 1, PIN,
NEO_TILE_TOP   + NEO_TILE_LEFT   + NEO_TILE_ROWS   + NEO_TILE_PROGRESSIVE +
NEO_MATRIX_TOP + NEO_MATRIX_LEFT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG,
NEO_GRB + NEO_KHZ800);

#define NUMPIXELS 40 // Popular NeoPixel ring size
// When setting up the NeoPixel library, we tell it how many pixels1,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels1(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


Picture_Matrix_RGB rgbBuf[IDE_MATRIX_WIDTH][IDE_MATRIX_HEIGHT];
bool pov_en = 0;

/** 
 * @brief   - 매트릭스 , 비트맵 관련 출력 처리 객체
*/
LedPannel ledpannel;
/** 
 * @brief   - 블루투스 통신 객체
*/
BluetoothSerial SerialBT;
/** 
 * @brief   - 블루투스 통신 패킷 프로토콜 처리 객체
*/
BufferSerial bufferSerial = BufferSerial(&SerialBT);

/** 
 * @brief   - 블루투스 함수 차일드 객체
*/
Bluetooth bluetooth(&SerialBT);

/* delay 값 관련 변수 */
unsigned long pre_time = 0; 

/* 폰트 관련 변수 */
char **strArr = NULL;
int lineCnt = 1;
int16_t font_total_width = 0, font_total_height = 0;

/*  Mode 관련 변수 */
int modeStep = 0;
int int_flg = 0;                //  Mode 변경 Flag


uint8_t modeNum = MODE_BLUETOOTH;
uint8_t startModeNum = MODE_BLUETOOTH;
float ledDivLevel = 1;
uint8_t useModeList[MODE_COUNT] = {MODE_BLUETOOTH}; 
uint8_t modeCount = 1;
// 로고 시간 
int logoTime = 2000;
// 로고 이용 
uint8_t logoEnable = LOGO_ENABLE_BIT_MASK_DEFAULT;

/*  현재 로테이션 값  */
uint8_t g_rotationValue = ROTATION_VALUE_LANDSCAPE;
int g_rotation_start_pos = 0; //  로테이션에 따른 x 좌표 위치 (세로 모드 일 경우 시작 좌표 변경 됨)

/*  디스플레이 모드 관련 변수  */
size_t pageSize[PAGE_TOTAL_COUNT] = {0};

int8_t g_pageCount = 0;
bool nextPageToken = false;
bool categoryActionFlag = false;
int displayPageTime = 5000;
// 페이지 전환 모드 (수동 or 자동) 
bool autoNextPageMode = true;
// 페이지 버퍼
char* pageBuffer[PAGE_TOTAL_COUNT] = {NULL, };
// 페이지 Enable List --> 페이지 사용가능하는지 가지고 있는 버퍼 (모든 페이지 접근 하는데에 시간이 오래걸려 해당 버퍼 사용)
uint8_t pageEnableList[PAGE_TOTAL_COUNT] = {0};
bool isSinglePagePrint = true;  // 단일 페이지일 경우 깜빡거리지 않게 하기 위해 설정
uint8_t display_step = 0; //  디스플레이 모드 출력 스텝
//  Board 정보 배터리 Or 상시 전원
int8_t isBatteryModeInfo = POPSIGN_NON_BATTERY_MODE;

#define MOTOR_ENABLE_CNT 3
int hall_flg = 0;
int hall_cnt = 0;

// motor 동작 변수 1 : On, 0 : Off
uint8_t motor_en = 0;
uint8_t motor_step = 0;


/* Preferences Flash */
Preferences prefs;  
uint8_t prefsBoardInfoArray[BOARD_INFO_PREFS_IDX_TOTAL_COUNT] = {0};

// 블루투스 이름 
String bt_name = "SHOU_POV";

/*  PAGE TIME SWICH INFO   */
struct tm g_timeinfo;

/*  구동 시간대 설정 관련 변수 */
bool timeSwitchIsEnable[PAGE_TOTAL_COUNT] = {false};
int8_t timeSwitchStartHour[PAGE_TOTAL_COUNT] = {0}, timeSwitchStartMinute[PAGE_TOTAL_COUNT] = {0};
int8_t timeSwitchFromHour[PAGE_TOTAL_COUNT] = {0}, timeSwitchFromMinute[PAGE_TOTAL_COUNT] = {0};
bool isTimeSynch = false;


/*  Bitmap Image 관련 변수 */
bool g_logo_blink_flag = true;
uint16_t g_copy_bitmap[7*11] = {1};

/*  초기화 및 버전 정보 Flag */
int8_t g_initModeStep = LOOP_MODE_EXCUTE;
bool preventContinuousClicks = false; //  모드 연속 변경 막기위해 Flag 생성 
bool isContinueClickedBtn = false;  //  모드 전환 버튼 지속해서 클릭되고 있는지 체크 변수
unsigned long pre_time_reset = 0; 

/* led pixels1 index*/
int led_inx = 0;

//  기본 페이지 출력 중인 경우 
bool isDefalutIagePrint = true;  
//  페이지 출력 간격 유지 시간
int displayPageIntervalTime = DEFALUT_PAGE_INTERVAL_TIME; 


/** 
 * @brief - 홀센서 이벤트
**/
void IRAM_ATTR hall_sensor_event() 
{

  if (digitalRead(HALL_SENSOR_PIN) == 0) {    
    hall_flg = LOW;
    ++hall_cnt;
    //Serial.printf("1\n");
  }

  led_inx = 0; // LED 인덱스를 초기화 해서 자석이 인식되었을 때마다 초기화 되게하기 위함
  //Serial.printf("hall\n");
}


/** 
 * @brief - 모드 변경 이벤트
**/
void IRAM_ATTR mode_change_event() 
{
  int_flg = MODE_CHANGE;
}


/*  Multi Core Task_0 */
TaskHandle_t Task_0;

void setup() {
  //Serial Begin
  Serial.begin(115200);

  //Flash Begin
  if(!LittleFS.begin(true)){
      Serial.println("LittleFS Mount Failed");
  }

  pixels1.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  Serial.printf("Model : %lld \n", SHOU_0100_0040_16M);


  // MATRIX DECLARATION:
  // Parameter 1 = width of EACH NEOPIXEL MATRIX (not total display)
  // Parameter 2 = height of each matrix
  // Parameter 3 = number of matrices arranged horizontally
  // Parameter 4 = number of matrices arranged vertically
  // Parameter 5 = pin number (most are valid)
  // Parameter 6 = matrix layout flags, add together as needed:
  
  matrix->begin();
  matrix->setTextWrap(false);
  matrix->setBrightness(40);
  matrix->clear();
  matrix->show();  
  
  ledpannel.setup(matrix, &bufferSerial);
  device_init_start();


  int8_t modePin = MODE_PIN;
  
  bool checkUpdate = false;
  pinMode(modePin, INPUT_PULLUP);       //  모드 전환 핀 
  
  /*  배터리 전원 모델 or 상시 전원 모델 체크 */
  isBatteryModeInfo = POPSIGN_NON_BATTERY_MODE;

  Serial.printf("Shou POV Display : %s \n", FIRM_VERSION);
  //  플래시에 저장 되어있는 세팅 정보 리드 (모드 정보, LED 밝기, 업데이트 유무, 매트릭스 크기, 로고 시간, 디스플레이 설정 시간 등)
  getSettingInfo(&modeNum, &ledDivLevel, &modeCount, useModeList, sizeof(useModeList), &checkUpdate, &startModeNum, &logoTime, &displayPageTime);
  //  매트릭스 크기에 따른 Rotation 값 설정
  ledpannel.setupRotation();
  ledpannel.setupFont(1);

  if (checkUpdate == true)  ledpannel.print_str_matrix(1, "업데이트\n성공"); //  Config 버전 정보 달라졌을 경우

  Serial.printf("modeCount : %d \n", modeCount);

  /*  보드 타입에 따른 사용 모드 설정 (BT MODEL or UART MODEL) */
  if (g_board_config.model_info == YOUFUNI || g_board_config.model_info == ENT_12832) {
    Serial.printf("uart!! \n");
    Serial2.begin(115200, SERIAL_8N1, RX, TX);
    useModeList[0] = MODE_UART;
    useModeList[1] = MODE_BLUETOOTH;
    useModeList[2] = MODE_DISPLAY;
    modeCount = 3;
    modeNum = 0;
  }
  esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_DEFAULT, ESP_PWR_LVL_P9);
  esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_ADV, ESP_PWR_LVL_P9);
  esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_SCAN ,ESP_PWR_LVL_P9);
  disableCore0WDT();

  xTaskCreatePinnedToCore (
    sensor_check,                 // 센서 처리용 task
    "sensor_check",    // 태스크 이름
    // configMINIMAL_STACK_SIZE, // 스택 할당 크기 (number of bytes)
    10000, // 스택 할당 크기 (number of bytes)
    NULL,                     // 태스크 인수
    1,                        // 태스크 우선 순위 
    &Task_0,                  // 태스크 핸들
    0                         // 태스크가 실행될 코어 
  );
  
  // matrix->stopDMAoutput();
  log_ps_memory();
  attachInterrupt(digitalPinToInterrupt(modePin), mode_change_event, HIGH);  // ModeChange를 위한 이벤트 인터럽트 설정 (Mode Pin - GPIO13)
  esp_bredr_tx_power_set(ESP_PWR_LVL_P9,ESP_PWR_LVL_P9);  // Bluetooth Tx Power Level 설정 }

  //all_prox_sens_init();
  ir_init();
  hall_sensor_init();
  attachInterrupt(digitalPinToInterrupt(HALL_SENSOR_PIN), hall_sensor_event, CHANGE);  // ModeChange를 위한 이벤트 인터럽트 설정 (Mode Pin - GPIO13)
  
}

int pin_hal = 0;
void sensor_check(void *param)
{

  while(true) {

    if (motor_en == 1)
    {
      if (motor_step == 0) 
      {
        hall_cnt = 0;
        ir_motor_control(true);
        motor_step = 1;
      }
      else if (motor_step == 1)
      {
        if (hall_cnt >= MOTOR_ENABLE_CNT)
          motor_step = 2;
        else ir_motor_control(true);
      }
      else if (motor_step == 2)
      {
        
      }

    }
    vTaskDelay(100);  
    
  }
}


void loop()
{
    check_flash_reset();  //  리셋, 버전 출력, 수동 모드 탈출 Check  
    mode_change_check();  //  모드 전환 Check
    
    if (g_initModeStep != LOOP_VERSION_PRINT) {     //  버전 정보 출력 모드일 경우 하위 모드 출력하지 않음
      if (getModeNum(modeNum) == MODE_BLUETOOTH) 
      {
        pov_en = false;
        bluetoothMode();
      }
      else if (getModeNum(modeNum) == MODE_DISPLAY) 
      {
        displayMode();
      }
      else delay(10);
    }
    else delay(10);
}

/**************************************************************************/
/** 
 * @fn    - bluetoothMode()
 * @brief - 블루투스 통신 및 수신 받은 패킷에 따른 LED 점멸
*/
/**************************************************************************/
void bluetoothMode() // 0 ~ 1 Logo Setion, 2 ~ 3 bluetooth init, 4 is bt ongoing
{
  if (modeStep == 0) {
    Serial.println("BLUETOOTH MODE!");
    bluetooth.setup();
    bufferSerial.set_serial_type(TRANSMIT_BLUETOOTH);
    uint8_t x_pos_offset = (g_board_config.matrix_width - 64) / 2;
    
    pre_time = millis();
    modeStep = 1;
    //matrix->copyRGBBitmapRect(45 + x_pos_offset, 17, 7, 11, g_copy_bitmap);
  } 
  else if (modeStep == 1) 
  {
    ledpannel.command_excute();

    if(bluetooth.check())
    {
      // Serial.println("bluetooth OK!");
      modeStep = 2;
      pre_time = millis();
      led_inx = 0;
      motor_en = 0;
      motor_step = 0;

    }  
  } 
  else if (modeStep == 2) {
    if (!SerialBT.hasClient()) {
      modeStep = 1;
      bluetooth.disconnect();
      delay(200);
      bluetooth.setup();
    }
    bufferSerial.process(matrix, &ledpannel);
    // 홀 센서 flag가 Low일 때 회전하면서 표시한다.
	  if (hall_flg == LOW) {
		  ledpannel.pov_play(&led_inx, g_board_config.matrix_width, g_board_config.matrix_height, &pre_time, &hall_flg);
	  }
  } 
}



/**************************************************************************/
/** 
 * @fn    - displayMode()
 * @brief - 플래시에 저장된 페이지 출력
*/
/**************************************************************************/
void displayMode()
{
  if(bluetooth.check())
  {
    PRINTLN("블루투스 연결 시도 들어옴");
    bufferSerial.process(matrix, &ledpannel);
    return;
  } 
  /**
   * modeStep 0 : 디스플레이 모드 로고 출력
   * modeStep 1 : 디스플레이 모드 로고 시간 대기
   * modeStep 2 : 디스플레이 모드 페이지 출력
  */
  if (modeStep == 0)
  {
    Serial.println("DISPLAY MODE!");
    bluetooth.setup();
    bufferSerial.set_serial_type(TRANSMIT_BLUETOOTH);
    pov_en = false;
    modeStep = 1;
    Serial.printf("%s, %d", __FUNCTION__, __LINE__);
  }
  else if (modeStep == 1) 
  {
    modeStep = 2;
    display_step = 0;
    Serial.printf("%s, %d", __FUNCTION__, __LINE__);
    // load image from flash and output to rgb bufer
    // ledpannel.loadPage(0); 
    ledpannel.multiPagePrint(0);
    pre_time = millis();
    led_inx = 0;
    

  }
  else if (modeStep == 2) 
  {
	  if (hall_flg == LOW) {
      ledpannel.pov_play(&led_inx, g_board_config.matrix_width, g_board_config.matrix_height, &pre_time, &hall_flg);
    }
  }
}


/** 
 * @fn    - mode_change_check()
 * @brief - 모드 변경 이벤트 처리
**/
void mode_change_check() 
{
  
  if ((!autoNextPageMode)) { //  디스플레이 수동 페이지 전환 모드 일 경우
    if (getModeNum(modeNum) == MODE_DISPLAY)  return;
    else if (get_critical_status() == BUSY_WAITS && digitalRead(MODE_PIN) == HIGH) {
      critical_section_end();
      int_flg = 0;
    }
  }
  if (preventContinuousClicks == true) {
    if (digitalRead(MODE_PIN) == HIGH) {
      int_flg = 0;
      preventContinuousClicks = false;
    }
    return;
  }
  if (get_critical_status() == NON_BUSY) 
  {
    if (int_flg == MODE_CHANGE)
    {
      mode_entry_init(getModeNum(modeNum));
      modeNum = ++modeNum % modeCount;
      modeStep = 0;
      int_flg = 0;
      g_initModeStep = LOOP_MODE_EXCUTE;
      categoryActionFlag = false;
      ledpannel.set_action_off();
    }
  }
}

/** 
 * @fn    - getModeNum()
 * @brief - 사용되는 모드 리스트안의 모드 Get
 * @param modeNum - 검색 할 Mode Number  
**/
int getModeNum(uint8_t modeNum) 
{
  return useModeList[modeNum];
}

/** 
 * @fn    - getCurModeNum()
 * @brief - 현재 Mode Number 반환하는 함수
 * @return - 현재 Mode Number
**/
int getCurModeNum() 
{
  return useModeList[modeNum];
}

/** 
 * @fn    - mode_entry_init()
 * @brief - 모드 변경 시 해당 모드의 자원 해제
 * @param ModeNum - 변경 되기전의 모드 Num
 * @TCH : 모드 변경 자원 해제 
**/
void mode_entry_init (uint8_t modeNum) {
  matrix->fillScreen(0);
  switch (modeNum)
  {
  case MODE_BLUETOOTH :
    bluetooth.disconnect();
    break;
  case MODE_DISPLAY :
    bluetooth.disconnect();
    free_multiPage();
    break;
  case MODE_UART :
    break;
  default:
    break;
  }
}


/** 
 * @fn    - init_modeChange_flag()
 * @brief - 모드 변경 값 변수 0 초기화
**/
void init_modeChange_flag() {
  int_flg = 0;
}

/** 
 * @fn    - set_transmit_mode_change()
 * @brief - uart <-> bluetooth 모드 변경
**/
void set_transmit_mode_change(char* data) {  //  Switch Mode to Communication
  uint8_t mode_info = data[0];
  if (mode_info == TRANSMIT_MODE_BLUETOOTH) {
    bool checkUpdate = false;
    mode_entry_init(getModeNum(modeNum));
    getSettingInfo(&modeNum, &ledDivLevel, &modeCount, useModeList, sizeof(useModeList), &checkUpdate, &startModeNum, &logoTime, &displayPageTime);
    useModeList[0] = MODE_BLUETOOTH;
    useModeList[1] = MODE_DISPLAY;
    modeCount = 2;
    int_flg = MODE_CHANGE;
    modeNum = -1;
  } else if (mode_info == TRANSMIT_MODE_UART) {
    mode_entry_init(getModeNum(modeNum));
    useModeList[0] = MODE_UART;
    useModeList[1] = MODE_BLUETOOTH;
    useModeList[2] = MODE_DISPLAY;
    modeCount = 3;
    int_flg = MODE_CHANGE;
    modeNum = -1;
  }
}
/**************************************************************************/
/** 
 * @fn      - set_mode_change(char* data)
 * @brief   - 모드 변경 함수
 * @details - 기존에 모드가 없다면 useModeList에 저장 후 플래시 삽입
 * @param data - 변경할 모드 정보
*/
/**************************************************************************/
void set_mode_change(char* data) {
  Serial.println("set_mode_change");
  mode_entry_init(getModeNum(modeNum));

  uint8_t _mode_num = data[0];
  int_flg = 0;
  modeStep = 0;
  categoryActionFlag = false;
  ledpannel.set_action_off();
  
  bool check_mode_already_exist = false;
  int mode_idx = 0;

  uint8_t buffer[SETTING_DATA_COUNT];
  int i = 0;
  int readBufferIndex = 0;

  ledpannel.set_matrix_end();

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  i = SETTING_IDX_DP_CHECK; //  플래시 모드 정보 시작 인덱스

  for (int i = 0; i < MODE_COUNT; i++) {  //  payload로 넘어온 모드가 기존에 있는지 Check
    if (useModeList[i] == _mode_num) {
      check_mode_already_exist = true;
      mode_idx = i;
      break;
    }
  }

  if (!check_mode_already_exist) {  //  기존에 모드가 없다면 useModeList에 모드 추가
    // PRINTF("기존에 모드 없음");
    useModeList[modeCount] = _mode_num;
    modeNum = modeCount;
    modeCount++;
    buffer[i + (_mode_num - 1)] = 1;  //  Setting Idx 계산
  } else {
    modeNum = mode_idx;
  }
  
  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
  ledpannel.set_matrix_begin();
}

/**************************************************************************/
/** 
 * @fn      - set_mode_change_bt_non_disconnect()
 * @brief   - 블루투스 연결 끊지 않고 블루투스 모드로 변환하는 함수 
 * @details - 블루투스 모드 진입 시 연결 끊기므로 모드 스탭2로 이동하여 모드 변경 후 끊는 로직 넘김
*/
/**************************************************************************/
void set_mode_change_bt_non_disconnect() {
  // Serial.println("set_mode_change_bt_non_disconnect");
  switch (getModeNum(modeNum))
  {
  case  MODE_DISPLAY: 
    // Serial.println("displaymode");
    free_multiPage();
    break;
  default:
    break;
  } 

  modeNum = MODE_BLUETOOTH;
  modeStep = 2;

  ledpannel.matrixClear();   //  BT 모드 변환 시 화면 지움
}


/** 
 * @fn    - check_flash_reset()
 * @brief - 버전 표시, 플래시 리셋 처리 함수
 * @details - 3초 누를 시 버전 표시, 10초 누를 시 플래시 초기화 및 재부팅
**/
void check_flash_reset() {  //  모드 스위치 3초 이상 누르고 있을 시 플래시 포멧 및 리셋  
  if (get_critical_status() == BUSY_WAITS) return;
  if (digitalRead(MODE_PIN) == LOW || isContinueClickedBtn)
  {
    if (!isContinueClickedBtn) {  //  Rest Loop 최초 진인지 체크 변수 -> 최초 진입이라면 시간 체크하고 탈출
      isContinueClickedBtn = true;
      pre_time_reset = millis();
      return;
    }

    if (digitalRead(MODE_PIN) == HIGH) {  //  최초 진입이 아닐 시 버튼클릭이 떼어져있다면 자원 초기화 하고 탈출
      isContinueClickedBtn = false;
      g_initModeStep = LOOP_MODE_EXCUTE;
      return;
    }

    int nextTime;
    if (g_initModeStep == LOOP_MODE_EXCUTE) nextTime = 3000; //  3000 == 버전 표시 버튼 클릭 시간 
    else if (g_initModeStep == LOOP_VERSION_PRINT) nextTime = 7000;  //  7000 == 초기화 버튼 클릭 시간 
    if (delayExt(millis(), &pre_time_reset, nextTime)) {
      if ((!autoNextPageMode) && getModeNum(modeNum) == MODE_DISPLAY) { //  디스플레이 수동모드인 경우 모드 전환
        mode_entry_init(getModeNum(modeNum));
        modeNum = ++modeNum % modeCount;
        modeStep = 0;
        categoryActionFlag = false;
        ledpannel.set_action_off();
        int_flg = 0;
        critical_section_begin();
      } else {
        if (g_initModeStep == LOOP_MODE_EXCUTE) { // 처음 3초 버전 출력
          ledpannel.print_str_matrix_basic(FONT_ONE_SIZE, FIRM_VERSION, CMD_ACT_DEFALUT, matrix->Color(0,255,0));
          g_initModeStep = LOOP_VERSION_PRINT;
          int_flg = 0;
          preventContinuousClicks = true;
          // while (digitalRead(MODE_PIN) == LOW); //  Mode Pin Status Low case is Infinite loop . . .
        } else if (g_initModeStep == LOOP_VERSION_PRINT) {
          Serial.println("Flash Format & Reset");
          matrix->fillScreen(0);
          // String _str =  "";
          String _str = "초기화";
          ledpannel.print_str_matrix_basic(FONT_ONE_SIZE, _str, CMD_ACT_DEFALUT, matrix->Color(255,0,0));
          delay(3000);
          matrix->fillScreen(0);
          prefs.begin(PREFS_NAME);
          prefs.clear();
          // prefs.freeEntries();
          // prefs.remove();
          flashFormat();
          ESP.restart();
        }
      }
    } 
  } else return;
}


/** 
 * @fn    - timer_switch_print_page()
 * @brief - 디스플레이 페이지 시간 조건 체크 함수
 * @details - 3초 누를 시 버전 표시, 10초 누를 시 플래시 초기화 및 재부팅
 * @return PAGE_TIME_OK : 페이지 출력 (시간 조건 true)
 * @return PAGE_TIME_NO : 페이지 출력 안함(시간 조건 false)
**/
int timer_switch_print_page() {
  if (isTimeSynch == false) return PAGE_TIME_ASYNC;

  getLocalTime(&g_timeinfo);
  int curHour = g_timeinfo.tm_hour;
  int curMinute = g_timeinfo.tm_min;
  int curTotalMin = (curHour * 60) + curMinute;
  int startTotalMin = (timeSwitchStartHour[g_pageCount] * 60) + timeSwitchStartMinute[g_pageCount];
  int fromTotalMin = (timeSwitchFromHour[g_pageCount] * 60) + timeSwitchFromMinute[g_pageCount];
  // Serial.printf("startTotalMin <= fromTotalMin \n");
  // Serial.printf("curTotalMin : %d \n", curTotalMin);
  // Serial.printf("startTotalMin : %d \n", startTotalMin);
  // Serial.printf("fromTotalMin : %d \n", fromTotalMin);
  if (startTotalMin <= fromTotalMin) {
    if (startTotalMin <= curTotalMin && fromTotalMin >= curTotalMin) {
      return PAGE_TIME_OK;
    }
  } else {
    if (startTotalMin <= curTotalMin || fromTotalMin >= curTotalMin ) {
      return PAGE_TIME_OK;
    }
  }
  return PAGE_TIME_NO;
}


/** 
 * @fn    - device_init_start()
 * @brief - 상단 모서리 삼각표시 출력
**/
void device_init_start() {
  
  Serial.printf("device init \n");
  for (int16_t j = 0; j < 40; j++) {
    if (j < 15)
      pixels1.setPixelColor(j, 20, 0, 0);
    else if (j >= 15 && j < 25)
      pixels1.setPixelColor(j, 0, 20, 0);
    else if (j >= 25 && j < 40)
      pixels1.setPixelColor(j, 20, 0, 0);
  }
  pixels1.show();
}

/** 
 * @fn    - log_ps_memory()
 * @brief - PSRAM Memory 출력
**/
void log_ps_memory() {
  log_d("Used PSRAM: %d", ESP.getPsramSize() - ESP.getFreePsram());
  log_d("ESP.getPsramSize() : %d", ESP.getPsramSize());
  log_d("ESP.getFreePsram() : %d", ESP.getFreePsram());
}

/** 
 * @fn    - log_heap_memory()
 * @brief - heap Memory 출력
**/
void log_heap_memory() {
  log_d("Total heap: %d", ESP.getHeapSize());
  log_d("Free heap: %d", ESP.getFreeHeap());
  log_d("Total PSRAM: %d", ESP.getPsramSize());
  log_d("Free PSRAM: %d", ESP.getFreePsram());
}
