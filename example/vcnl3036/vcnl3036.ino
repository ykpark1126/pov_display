#include <VCNL3036.h>
#include <Wire.h>




// 채널 1
/* 근접 센서 I2C 채널 관련 */
#define SENS1_I2C_SDA 25
#define SENS1_I2C_SCL 33


/* 근접 센서 인터럽트 핀 */
#define PROXI_SENS1 39



// 채널 2
/* 근접 센서 I2C 채널 관련 */
// #define SENS1_I2C_SDA 27
// #define SENS1_I2C_SCL 26


// /* 근접 센서 인터럽트 핀 */
// #define PROXI_SENS1 34


// 채널 3
// /* 근접 센서 I2C 채널 관련 */
// #define SENS1_I2C_SDA 14
// #define SENS1_I2C_SCL 12


// /* 근접 센서 인터럽트 핀 */
// #define PROXI_SENS1 35



/** 
 * @brief - 근접 센서 체크
**/
void IRAM_ATTR check_proxi_sensor() 
{
  // stop motor
  // ir_motor_control(false);
  Serial.printf("*************************OK**************************\n");
}

VCNL3036 sens;

void setup(){  
  Serial.begin(115200);
  //all_prox_sens_init();

  pinMode(PROXI_SENS1, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PROXI_SENS1), check_proxi_sensor, CHANGE); 
 
  Wire.begin(SENS1_I2C_SDA, SENS1_I2C_SCL);
  if (sens.begin(true) == false) {
    Serial.printf("1 Sensor Fail\n");
    
  }
  else 
  {
    sens.setPSResolution(RESOL_12BIT);
    sens.setPSDuty(PS_DUTY_40);// 4.85ms
    sens.setPSIT(PS_IT_1T0);  // 8T
    sens.setINTMode(PS_INT_CLOSING); // interrupt by closing
    //sens.enableINTProxMode();
    sens.enableINTNormalMode();
    sens.setPSTHDL(400);
    sens.setPSTHDH(800); // 위에 선택한 12bit(4096)의 값으로 센서의 감도를 나타내는 것으로 판단됨
    sens.setPSPers(PS_PERS_1); // 2번 이상 발생해야 인터럽트 발생
    sens.setLedCurrent(LED_CURRENT_200mA);
    sens.startReading();
    //Wire.end();
    Serial.printf("1 Sensor ok\n");
    
  }

}  
uint16_t val = 0;
void loop()
{



  delay(2000);  
  //sens.begin(false);
  // if (sens.begin(true) == false)
  //   Serial.printf("2 Sensor Fail\n");
  //val = sens.readPSData();
  //Serial.printf("val : %04x\n", val);

  // val = sens.readCommand(VCNL3036_DEV_ADDRESS, 0x03);
  // Serial.printf("val2 : %04x\n", val);

  // val = sens.readCommand(VCNL3036_DEV_ADDRESS, 0x04);
  // Serial.printf("val3 : %04x\n", val);

  val = sens.readINTFlag();
   Serial.printf("val4 : %04x\n", val);


  
  
}

void all_prox_sens_init(void)
{  
  bool ret;
  ret = prox_init(1);
  if (ret == false)
    Serial.printf("1 Sensor Fail\n");
//  ret = prox_init(2);
//  if (ret == false)
//    Serial.printf("2 Sensor Fail\n");
//  ret = prox_init(3);
//  if (ret == false)
//    Serial.printf("3 Sensor Fail\n");
//  ret = prox_init(4);
//  if (ret == false)
//    Serial.printf("4 Sensor Fail\n");

  pinMode(PROXI_SENS1, INPUT_PULLUP);
//  pinMode(PROXI_SENS2, INPUT_PULLUP);
//  pinMode(PROXI_SENS3, INPUT_PULLUP);
//  pinMode(PROXI_SENS4, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(PROXI_SENS1), check_proxi_sensor, RISING);  
//  attachInterrupt(digitalPinToInterrupt(PROXI_SENS2), check_proxi_sensor, RISING); 
//  attachInterrupt(digitalPinToInterrupt(PROXI_SENS3), check_proxi_sensor, RISING); 
//  attachInterrupt(digitalPinToInterrupt(PROXI_SENS4), check_proxi_sensor, RISING); 
}

bool prox_init(uint8_t ch)
{
  int sda, scl;
  if (ch == 1) {
    sda = SENS1_I2C_SDA;
    scl = SENS1_I2C_SCL;
  }
//  else if (ch == 2) {
//    sda = SENS2_I2C_SDA;
//    scl = SENS2_I2C_SCL;
//  }
//  else if (ch == 3) {
//    sda = SENS3_I2C_SDA;
//    scl = SENS3_I2C_SCL;
//  }
//  else if (ch == 4) {
//    sda = SENS4_I2C_SDA;
//    scl = SENS4_I2C_SCL;
//  }
  
  VCNL3036 sens;
  Wire.begin(sda, scl);
  if (sens.begin(true) == false)
    return false;
  else 
  {
    sens.setPSResolution(RESOL_12BIT);
    sens.setPSDuty(PS_DUTY_40);// 4.85ms
    sens.setPSIT(PS_IT_8T0);  // 8T
    sens.setINTMode(PS_INT_CLOSING); // interrupt by closing
    sens.enableINTProxMode();
    sens.setPSTHDL(500);
    sens.setPSTHDH(4000); // 위에 선택한 12bit(4096)의 값으로 센서의 감도를 나타내는 것으로 판단됨
    sens.setPSPers(PS_PERS_2); // 2번 이상 발생해야 인터럽트 발생
    Wire.end();
    return true;
  }
}
