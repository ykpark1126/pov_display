#ifndef _POPSIGN_
#define _POPSIGN_

#include <AnimatedGIF.h>


#define MODE_CHANGE 1   //모드 전환 값

// 통신 ACK, NACK Value
#define RECIVE_ACK  1
#define RECIVE_NACK 0

#define WIFI_RESOURCE_RELEASE 1 //  와이파이 모드 해제되는 경우 기록되는 값
#define PAGE_CHANGE 1   // 페이지 전환 값

#define PAGE_TIME_ASYNC 0   // 현재 시간 동기화 않음
#define PAGE_TIME_OK    1   // 페이지 출력 조건 True
#define PAGE_TIME_NO    2   // 페이지 출력 조건 False

#define RETURN_EXCEPTION_VALUE    -99   // Return 예외 값

#if ARDUHAL_LOG_LEVEL >= ARDUHAL_LOG_LEVEL_DEBUG    // Log Level 디버그 레벨보다 높은 경우 Serial 출력
    #define PRINT_LINE Serial.println(__LINE__);
    #define PRINTF Serial.printf
    #define PRINTLN Serial.println
#else                                               //  Log Level 디버그 레벨 낮은 경우 Serial 출력하지 않음
    #define PRINT_LINE
    #define PRINTF
    #define PRINTLN
#endif


#define DEVICE_INIT_VALUE 255

/*  Loop - 모드 처리 Step 0  */
#define LOOP_MODE_EXCUTE 0
/*  Loop - 버전 출력 Step 1  */
#define LOOP_VERSION_PRINT 1


/*  모드 변경 초기화  */
void init_modeChange_flag();
/*  모드 관련 설정 함수  */
void IRAM_ATTR mode_change_event(); 
void mode_change_check();
int getModeNum(uint8_t modeNum);
int getCurModeNum();
void mode_entry_init (uint8_t modeNum);


/*  모드 별 기능 수행 함수 */
void bluetoothMode();
void displayMode();
void init_mode_change_flag();  // 모드 스위치 연속 클릭 처리 방지  
void set_transmit_mode_change(char* data);  //  uart 전용 모드 변경
void set_mode_change(char* data);
void set_mode_change_bt_non_disconnect();

/* 초기화 */
void check_flash_reset();
/* 디스플레이 페이지 시간 조건 체크 함수 */
int timer_switch_print_page();
/*  상단 모서리 삼각표시 출력 */
void device_init_start();

/* core1 모드 관련 처리 Task */
void IRAM_ATTR play_pov(void *param); // Task 0 : Running on CPU #0
/* memory 출력 관련 함수 */
void log_ps_memory();
void log_heap_memory();


#endif
